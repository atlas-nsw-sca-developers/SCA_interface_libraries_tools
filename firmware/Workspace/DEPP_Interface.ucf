
#general signals
NET "clk_i" LOC = AG18;
NET "reset" LOC = G6;

#PCB control signals
NET "i2c_scl" LOC = AH34;
NET "i2c_scl" IOSTANDARD = LVCMOS25;
NET "i2c_sda" LOC = AJ34;
NET "i2c_sda" IOSTANDARD = LVCMOS25;

#GBT-SCA control signals
NET "supply_en"       LOC = AH33;
NET "supply_en"       IOSTANDARD = LVCMOS25;
NET "sca_auxport_scl" LOC = AD32;
NET "sca_auxport_scl" IOSTANDARD = LVCMOS25;
NET "sca_auxport_sda" LOC = AE32;
NET "sca_auxport_sda" IOSTANDARD = LVCMOS25;
NET "sca_elink_tx_pri_p" LOC = V32;
NET "sca_elink_ck_pri_p" LOC = AA34;
NET "sca_elink_rx_pri_p" LOC = W34;
NET "sca_elink_tx_Aux_p" LOC = C32;
NET "sca_elink_ck_Aux_p" LOC = B33;
NET "sca_elink_rx_Aux_p" LOC = B32;
NET "sca_fuseprog"       LOC = AC34;
NET "sca_reset_b"        LOC = AD34;
NET "sca_testenable"     LOC = W32;
NET "sca_axudisable"     LOC = Y32;

#USB interface	
NET "pdb[0]" LOC = G12;
NET "pdb[0]" IOSTANDARD = LVCMOS25;
NET "pdb[1]" LOC = G11;
NET "pdb[1]" IOSTANDARD = LVCMOS25;
NET "pdb[2]" LOC = G13;
NET "pdb[2]" IOSTANDARD = LVCMOS25;
NET "pdb[3]" LOC = L10;
NET "pdb[3]" IOSTANDARD = LVCMOS25;
NET "pdb[4]" LOC = K9;
NET "pdb[4]" IOSTANDARD = LVCMOS25;
NET "pdb[5]" LOC = L9;
NET "pdb[5]" IOSTANDARD = LVCMOS25;
NET "pdb[6]" LOC = C13;
NET "pdb[6]" IOSTANDARD = LVCMOS25;
NET "pdb[7]" LOC = E13;
NET "pdb[7]" IOSTANDARD = LVCMOS25;
NET "astb" LOC = B13;
NET "astb" IOSTANDARD = LVCMOS25;
NET "dstb" LOC = A13;
NET "dstb" IOSTANDARD = LVCMOS25;
NET "pwr" LOC = K8;
NET "pwr" IOSTANDARD = LVCMOS25;
NET "pwait" LOC = N10;
NET "pwait" IOSTANDARD = LVCMOS25;
NET "clk_usb" LOC = J10;
NET "clk_usb" CLOCK_DEDICATED_ROUTE = FALSE;

# LEDS
NET "LEDs[0]" LOC = AG8;
NET "LEDs[1]" LOC = AH8;
NET "LEDs[2]" LOC = AH9;
NET "LEDs[3]" LOC = AG10;
NET "LEDs[4]" LOC = AH10;
NET "LEDs[5]" LOC = AG11;
NET "LEDs[6]" LOC = AF11;
NET "LEDs[7]" LOC = AE11;

# SWITCHES
NET "SWITCH[0]" LOC = J19;
NET "SWITCH[1]" LOC = L18;
NET "SWITCH[2]" LOC = K18;
NET "SWITCH[3]" LOC = H18;
NET "SWITCH[4]" LOC = H17;
NET "SWITCH[5]" LOC = K17;
NET "SWITCH[6]" LOC = G16;
NET "SWITCH[7]" LOC = G15;
# SCA GPIO		 
NET "sca_gpio[3]" LOC = R33;
NET "sca_gpio[0]" LOC = R34;
NET "sca_gpio[1]" LOC = T33;
NET "sca_gpio[2]" LOC = R32;
NET "sca_gpio[4]" LOC = P34;
NET "sca_gpio[5]" LOC = N34;
NET "sca_gpio[7]" LOC = P32;
NET "sca_gpio[6]" LOC = N32;
# SCA I2C
NET "sca_i2c_scl[0]" LOC = AH32;
NET "sca_i2c_scl[1]" LOC = D34;
NET "sca_i2c_scl[2]" LOC = H32;
NET "sca_i2c_scl[3]" LOC = AM32;
NET "sca_i2c_scl[4]" LOC = E34;
NET "sca_i2c_scl[5]" LOC = H33;
NET "sca_i2c_scl[6]" LOC = AK32;
NET "sca_i2c_scl[7]" LOC = F34;
NET "sca_i2c_scl[8]" LOC = K32;
NET "sca_i2c_scl[9]" LOC = AN33;
NET "sca_i2c_scl[10]" LOC = J34;
NET "sca_i2c_scl[11]" LOC = K34;
NET "sca_i2c_scl[13]" LOC = M32;
NET "sca_i2c_scl[14]" LOC = M33;
NET "sca_i2c_scl[12]" LOC = AL33;
NET "sca_i2c_scl[15]" LOC = AP32;
NET "sca_i2c_sda[0]" LOC = AG32;
NET "sca_i2c_sda[1]" LOC = C34;
NET "sca_i2c_sda[2]" LOC = G32;
NET "sca_i2c_sda[3]" LOC = AM33;
NET "sca_i2c_sda[4]" LOC = F33;
NET "sca_i2c_sda[5]" LOC = J32;
NET "sca_i2c_sda[6]" LOC = AJ32;
NET "sca_i2c_sda[7]" LOC = G33;
NET "sca_i2c_sda[8]" LOC = K33;
NET "sca_i2c_sda[9]" LOC = AN34;
NET "sca_i2c_sda[10]" LOC = H34;
NET "sca_i2c_sda[11]" LOC = L34;
NET "sca_i2c_sda[12]" LOC = AL34;
NET "sca_i2c_sda[13]" LOC = L33;
NET "sca_i2c_sda[14]" LOC = N33;
NET "sca_i2c_sda[15]" LOC = AN32;

# SCA SPI
NET "sca_spi_miso"    LOC = AB33;
NET "sca_spi_mosi"    LOC = AG33;
NET "sca_spi_clk"     LOC = AE33;
NET "sca_spi_ss<0>"   LOC = U32;
NET "sca_spi_ss<1>"   LOC = U31;
NET "sca_spi_ss<2>"   LOC = U33;
NET "sca_spi_ss<3>"   LOC = T34;
NET "sca_spi_ss<4>"   LOC = AA33;
NET "sca_spi_ss<5>"   LOC = Y33;
NET "sca_spi_ss<6>"   LOC = AC33;

# sca jtag
NET "sca_jtag_tck"    LOC = AF34;
NET "sca_jtag_tdi"    LOC = AB32;
NET "sca_jtag_tdo"    LOC = AC32;
NET "sca_jtac_tms"    LOC = AF33;
NET "sca_jtag_res"    LOC = AE34;

# PMOD external connector
NET "extJA<0>"  LOC = "AD11";   # JA1
NET "extJA<1>"  LOC = "AD9";    # JA2
NET "extJA<2>"  LOC = "AM13";   # JA3
NET "extJA<3>"  LOC = "AM12";   # JA4
NET "extJA<4>"  LOC = "AD10";   # JA7
NET "extJA<5>"  LOC = "AE8";    # JA8
NET "extJA<6>"  LOC = "AF10";   # JA9
NET "extJA<7>"  LOC = "AJ11";   # JA10
NET "extJB<3>"  LOC = "AB10";   # JB3
NET "extJB<4>"  LOC = "AC9";   # JB4