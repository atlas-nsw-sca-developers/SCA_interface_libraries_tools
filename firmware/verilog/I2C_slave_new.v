// Copyright (c) 2015 
// Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
// All rights reserved.
// You may redistribute and modify this project under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// Please see the CERN OHL v.1.2 for applicable conditions

`timescale 1ns / 1ps
`define leds

module I2C_slave_new #(
	parameter [23:0] timeout = 24'hffffff,
	parameter [7:0]  nreg    = 16
	)(
	input                clock,
	input                reset,
	input                SCL,
	inout                SDA,
	output  [nreg*8-1:0] rec,
	input   [nreg*8-1:0] send,
	input   [9:0]        I2C_10b_ADR,
   input   [6:0]        I2C_7b_ADR
	`ifdef leds
	,output         [7:0] led  
	`endif
   );
	
	reg  [2:0] state;
	reg        start, stop, SDA_del,SCL_del;
	reg        SDAo, SCLc, SDAc, rw, ack;
	reg  [3:0] k, point;
	reg  [3:0] i;
	reg  [7:0] buffer;
	reg  [7:0] mem_rec[0:nreg-1];
	wire [7:0] mem_send[0:nreg-1];
	reg [23:0] timeoutcnt;
	
	wire   SDAi = SDA;
	assign SDA = (~SDAo) ? 1'b0 : 1'bZ; 
	PULLUP pullupS15(SDA);
		
	for(genvar v=0; v<nreg; v=v+1) begin:outassignament 
		assign rec[v*8 +: 8] = mem_rec[v][7:0];
		assign mem_send[nreg-v-1][7:0] = send[v*8 +: 8];
	end
	
	localparam [2:0] IDLE=3'd1, ADR=3'd2, RX=3'd4, TX=3'd3;
	
	always @(posedge clock) SCLc    <=#1 SCL;	
	always @(posedge clock) SCL_del <=#1 SCLc;
	always @(posedge clock) SDAc    <=#1 SDAi;	
	always @(posedge clock) SDA_del <=#1 SDAc;	
	
	always @(posedge clock or posedge reset) begin
		if(reset) start <=#1 1'b0;
		else if(SCLc & ~SDAc & SDA_del) start <=#1 1'b1;
		else start <=#1 1'b0;
	end
	
	always @(posedge clock or posedge reset) begin
		if(reset) stop <=#1 1'b0;
		else if(SCLc & SDAc & ~SDA_del) stop <=#1 1'b1;
		else stop <=#1 1'b0;
	end
	
	wire close = (timeoutcnt==0) ? 1'b1 : 1'b0;
	
	always @(posedge clock or posedge reset) begin 
		if(reset) timeoutcnt <=#1 timeout;
		else begin
			if ((SCLc==0)||(SDAc==0)) timeoutcnt <=#1 timeout;
			else if(timeoutcnt>0) timeoutcnt <=#1 timeoutcnt-24'b1;
		end
	end
		
	always @(posedge clock or posedge reset) begin
		if(reset) begin
				state  <=#1 IDLE;
				point  <=#1 0;
				buffer <=#1 0;
				SDAo   <=#1 'b1;
				i      <=#1 0;
				ack    <=#1 1'b0;
				for (k=0;k<nreg-1;k=k+1) mem_rec[k] <=#1 8'h0;
		end
		else if (start)        begin
			state  <=#1 ADR;
			point  <=#1 4'h0;
			buffer <=#1 7'h0;
			SDAo   <=#1 1'b1;
			i      <=#1 4'b0;
			ack    <=#1 1'b0;
		end
		else if (stop | close) state <=#1 IDLE;	
		else begin
			case (state)
				IDLE: SDAo <=#1 1'b1;
				ADR: begin
					i <=#1 (~SCL_del & SCLc) ? (i+4'b1) : i; 
					if (i<8) begin
						SDAo <=#1 1'b1;
						if (~SCL_del & SCLc) 
							buffer[7:0] <=#1 {buffer[6:0], SDAc};
					end
					else if(i==8 && SCL_del && !SCLc) begin
						if(buffer[7:1] == I2C_7b_ADR) SDAo <=#1 1'b0;//ack
						else state <=#1 IDLE;
					end
					else if(i==9 && SCL_del && !SCLc) begin 
							i  <=#1 0;
							if(buffer[0]) begin
								state <=#1 TX;
								SDAo  <=#1 mem_send[0][7];
							end
							else          state <=#1 RX;
					end	
				end
				
				RX: begin
					i <=#1 (~SCL_del & SCLc) ? (i+4'b1) : i; 
					if(i<8) begin
						SDAo <=#1 1'b1;
						if (~SCL_del & SCLc) 
							buffer[7:0] <=#1 {buffer[6:0], SDAc};
					end
					else if(i==8 && SCL_del && !SCLc) 
						SDAo <=#1 1'b0;
					else if(i==9 && SCL_del && !SCLc) begin
						SDAo       <=#1 1'b1;
						i          <=#1 0;
						mem_rec[point] <=#1 buffer;
						point      <=#1 point + 6'b1;
					end
				end
				
				TX: begin
					i <=#1 (~SCL_del & SCLc) ? (i+4'b1) : i; 
					if(i<8 && !SCLc && SCL_del) 
						SDAo <=#1 mem_send[point][7-i];
					else if(i==8 && !SCL_del && SCLc)
						ack <= ~SDAc;
					else if(i==8 && SCL_del && !SCLc) 
						SDAo <=#1 1'b1;
					else if(i==9 && SCL_del && !SCLc) begin
						if (ack) begin
							SDAo       <=#1 (point<15) ? mem_send[point+1][7] : 1'b1;
							i          <=#1 0;
							point      <=#1 point + 6'b1;
						end
						else
							state <=#1 IDLE;
					end
				end
			endcase
		end
	end
	
	`ifdef leds
	wire notif_start, notif_stop;
	
	LED_notifier #(
		.LEN(10000000)
	)start_notif(
		.clk_i(clock),
		.res_i(reset),
		.event_i(start),
		.notif_o(notif_start)
	);
	LED_notifier #(
		.LEN(10000000)
	)stop_notif(
		.clk_i(clock),
		.res_i(reset),
		.event_i(stop),
		.notif_o(notif_stop)
	);
	assign led[7:0] = {notif_start, notif_stop, close, 2'b0, state[2:0]};
	`endif
endmodule
