// Copyright (c) 2015 
// Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
// All rights reserved.
// You may redistribute and modify this project under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// Please see the CERN OHL v.1.2 for applicable conditions

//This module is triplicated for SEU tolerance
                        
`define REG_LEN        32   
`define SPI_OFS_BITS   4:2
`define GENERIC_R0     0   // wb_adr: 0x00 
`define STATUS_REG     1   // wb_adr: 0x04
`define I2C_MSK        2   // wb_adr: 0x08
`define I2C_CTRL       3   // wb_adr: 0x0c   
`define DATA_REG0      4   // wb_adr: 0x10
`define DATA_REG1      5   // wb_adr: 0x14
`define DATA_REG2      6   // wb_adr: 0x18
`define DATA_REG3      7   // wb_adr: 0x1c 
`define I2C_CTRL_GO    8   // wb_adr: 0x20 
`define GENERIC_R0_LEN 32
`define GENERIC_R1_LEN 32
`define GENERIC_R2_LEN 32

`timescale 1ns / 1ps

module i2c_master_top( 
  output scl,
  output wb_int_o,
  output sda_out,
  output wb_ack_o,
  output wb_err_o,
  output [31:0] wb_dat_o, 
  input sda_in,
  input [31:0] wb_dat_i, 
  input [4:0] wb_adr_i,
  input wb_rst_i,
  input [3:0] wb_sel_i, 
  input wb_we_i,
  input wb_stb_i,
  input wb_cyc_i,
  input wb_clk_i
  );
          
  wire [31:0] i2c_msk_reg;
  wire [7:0] SRA_reg;
  wire reponse;
  wire read_nc;
  wire [7:0] i2c_dat_out;
  wire Start_tr;
  wire [7:0] Rx_Byte;
  wire MultiByte;
  wire [127:0] rx;
  wire [3:0] data_reg_sel;
  wire busy;
  wire rx_1byte;
  wire [31:0] i2c_ctrl_reg;
  wire [3:0] blktr_idx;
  wire [31:0] r0_out;
  wire [3:0] O;
  wire S133;
  wire le_ec;
  
  i2c_multi_reg  I2C_multi (
     .clk(wb_clk_i),
     .rst(wb_rst_i),
     .wb_dat_in(wb_dat_i[31:0]),
     .i2c_dat_out(i2c_dat_out[7:0]),
     .latch(O[3:0]),
     .i2c_latch(blktr_idx[3:0]),
     .Rx_1byte(rx_1byte),
     .rx(rx[127:0]),
     .byte_sel(wb_sel_i[3:0]),
     .tip(busy),
     .le_el(le_ec),
     .multi_byte(MultiByte),
     .Rx_Byte_in(Rx_Byte[7:0])
     );
 
  wb_adapt_i2c  WB2I2c (
     .clk(wb_clk_i),
     .rst(wb_rst_i),
     .Start_tr(Start_tr),
     .wb_ack(wb_ack_o),
     .reponse_n(reponse),
     .read_nc(read_nc),
     .wb_we(wb_we_i),
     .wb_addr(wb_adr_i[4:0])
     );
 
  wb_generic  wb_gen_1 (
     .wb_clk_i(wb_clk_i),
     .wb_rst_i(wb_rst_i),
     .wb_adr_i(wb_adr_i[4:0]),
     .wb_dat_i(wb_dat_i[31:0]),
     .wb_dat_o(wb_dat_o[31:0]),
     .wb_sel_i(wb_sel_i[3:0]),
     .wb_we_i(wb_we_i),
     .wb_stb_i(wb_stb_i),
     .wb_cyc_i(wb_cyc_i),
     .wb_ack_o(wb_ack_o),
     .wb_err_o(wb_err_o),
     .wb_int_o(wb_int_o),
     .registre0(r0_out[31:0]),
     .i2c_msk_reg(i2c_msk_reg[31:0]),
     .tip(busy),
     .last_bit(read_nc),
     .status_i2c(SRA_reg[7:0]),
     .i2c_ctrl_reg(i2c_ctrl_reg[31:0]),
     .rx_byte_in(Rx_Byte[7:0]),
     .data_reg_sel(data_reg_sel[3:0]),
     .rx(rx[127:0])
     );
 
  i2c_blk  I2C_ccu (
     .scl(scl),
     .sda_out(sda_out),
     .reponse(reponse),
     .clk(wb_clk_i),
     .read_nc(read_nc),
     .busy(busy),
     .sda_in(sda_in),
     .i2c_br(S133),
     .reset(wb_rst_i),
     .SRA_out(SRA_reg[7:0]),
     .MultiByte(MultiByte),
     .rx_1byte(rx_1byte),
     .msk(i2c_msk_reg[31:24]),
     .blktr_idx(blktr_idx[3:0]),
     .Start_tr_n(Start_tr),
     .le_ec(le_ec),
     .Rx_Byte_o(Rx_Byte[7:0]),
     .multi_byte_in(i2c_dat_out[7:0]),
     .cra(i2c_ctrl_reg[31:24]),
     .byte_out_cmd(r0_out[31:24]),
     .byte_out3(r0_out[23:16]),
     .byte_out4(r0_out[15:8]),
     .byte_out5(r0_out[7:0])
     );
 
  assign S133 = 1'b0;
  assign O[3] = ( data_reg_sel[3]) & ( wb_we_i);
  assign O[2] = ( data_reg_sel[2]) & ( wb_we_i);
  assign O[1] = ( data_reg_sel[1]) & ( wb_we_i);
  assign O[0] = ( data_reg_sel[0]) & ( wb_we_i);
  
endmodule





module i2c_blk (scl, sda_out, reponse, clk, read_nc, busy, sda_in, i2c_br,
                reset, SRA_out, MultiByte, rx_1byte, msk, blktr_idx,
                Start_tr_n, le_ec, Rx_Byte_o, multi_byte_in, cra, byte_out_cmd,
                byte_out3, byte_out4, byte_out5
                );
 
  output scl;
  output sda_out;
  output reponse;
  wire reponse;
  input clk;
  wire clk;
  input read_nc;
  wire read_nc;
  output busy;
  wire busy;
  input sda_in;
  wire sda_in;
  input i2c_br;
  wire i2c_br;
  input reset;
  wire reset;
  output [7:0] SRA_out;
  wire [7:0] SRA_out;
  output MultiByte;
  wire MultiByte;
  output rx_1byte;
  wire rx_1byte;
  input [7:0] msk;
  wire [7:0] msk;
  output [3:0] blktr_idx;
  wire [3:0] blktr_idx;
  input Start_tr_n;
  wire Start_tr_n;
  output le_ec;
  wire le_ec;
  output [7:0] Rx_Byte_o;
  wire [7:0] Rx_Byte_o;
  input [7:0] multi_byte_in;
  wire [7:0] multi_byte_in;
  input [7:0] cra;
  wire [7:0] cra;
  input [7:0] byte_out_cmd;
  wire [7:0] byte_out_cmd;
  input [7:0] byte_out3;
  wire [7:0] byte_out3;
  input [7:0] byte_out4;
  wire [7:0] byte_out4;
  input [7:0] byte_out5;
  wire [7:0] byte_out5;
  wire sti2c_out;
  wire ext_mode;
  wire start_i2c;
  wire [4:0] nbyte;
  wire [1:0] msk_op;
  wire stop_cond;
  wire ext_multi_mode;
  wire sda_gnd;
  wire [7:0] data_outY;
  wire rw_b_ext;
  wire ack_ok;
  wire busy_i2c;
  wire hor;
  wire [7:0] out_i2c;
 
  div_prog_sm  CLK_DIV
    (
     .clk(clk),
     .clk_out(hor),
     .reset(reset),
     .div(cra[1:0])
     );
 
  master_cmd_sm  Exec_cmd
    (
     .clock2(hor),
     .cmd(out_i2c[7:0]),
     .rx_byte_o(Rx_Byte_o[7:0]),
     .busy(busy_i2c),
     .sda(sda_out),
     .scl(scl),
     .sda_bi(sda_in),
     .reset(reset),
     .start_i2c(sti2c_out),
     .ack_ok(ack_ok),
     .data_in(data_outY[7:0]),
     .data_in2(byte_out5[7:0]),
     .nbyte(nbyte[4:0]),
     .sda_gnd(sda_gnd),
     .ext_mode(ext_mode),
     .MultiByte(MultiByte),
     .multi_byte_in(multi_byte_in[7:0]),
     .le_ec(le_ec),
     .rx_1byte(rx_1byte),
     .ext_multi_mode(ext_multi_mode),
     .rw_b_ext(rw_b_ext),
     .stop_cond(stop_cond),
     .blktr_idx(blktr_idx[3:0])
     );
 
  i_mask_op_fc  CMSK_op
    (
     .mask_in(msk[7:0]),
     .data_in(Rx_Byte_o[7:0]),
     .data_pass(byte_out4[7:0]),
     .data_out(data_outY[7:0]),
     .op(msk_op[1:0]),
     .clk(clk),
     .reset(reset)
     );
 
  sync_start  sync_clk
    (
     .reset(reset),
     .clk(clk),
     .sti2c_in(start_i2c),
     .sti2c_out(sti2c_out)
     );
 
  decode_sm_back  CMD_decod
    (
     .reset(reset),
     .ack_ok_in(ack_ok),
     .clk(clk),
     .cmd_in(byte_out_cmd[7:0]),
     .nbyte(nbyte[4:0]),
     .out_i2c(out_i2c[7:0]),
     .i2c_busy(busy_i2c),
     .start_i2c(start_i2c),
     .crc_ok(Start_tr_n),
     .occup(busy),
     .addr_i2c(byte_out3[7:0]),
     .read_nc(read_nc),
     .rep_out(reponse),
     .msk_op(msk_op[1:0]),
     .SRA(SRA_out[7:0]),
     .SDA_LOW(sda_gnd),
     .ext_mode(ext_mode),
     .i2c_br(i2c_br),
     .EBRDCST(cra[6]),
     .MultiByte(MultiByte),
     .nbyte_in(cra[5:2]),
     .ext_multi_mode(ext_multi_mode),
     .rw_b_ext(rw_b_ext),
     .stop_cond_in(stop_cond)
     );

endmodule





module i2c_multi_reg(clk,rst,wb_dat_in,i2c_dat_out,latch,i2c_latch,
                     Rx_1byte,rx,byte_sel,tip,le_el,multi_byte,Rx_Byte_in);
  
 input clk;
 input rst;
 input [3:0] latch;          // num. du registre 32 bit a ecrire depuis wb
 output [127:0]  rx;
 input [3:0]  byte_sel;     // vient de wb_sel wishb.
 input tip;                 // 1 quand ic2 actif
 input le_el;               // ecriture i2c = 0 lec = 1
 input multi_byte;          // =1 si mode multibyte
 input Rx_1byte;            // signal a 1 quand des data reception sont dispo.
 input  [31:0]   wb_dat_in;  // data pour ecriture dans registre depuis wb
 //input  [7:0]    i2c_dat_in; //  data  lue de i2c pour ecriture reg
 input  [3:0]    i2c_latch;  //  num. du byte a ecrire ou lire dans reg
 input  [7:0]    Rx_Byte_in;  // data reception de i2c
 output [7:0]    i2c_dat_out; // data a ecrite dans i2c
 reg    [7:0]    i2c_dat_out;
  
 reg [127:0]  data;   // registre in ou data multibyte i2c
 
  
 assign rx = data;
 
  always @(posedge clk or posedge rst)
  begin
    if(rst)
     // cnt <= #Tp {`SPI_CHAR_LEN_BITS+1{1'b0}};
       data[127:0] <= 128'h89;
    else if (latch[0] && !tip)   // mettre l'equivalent de tip dans les conditions pour empecher l'ecriture depuis wb. pendant i2cactif
        begin
         if(byte_sel[3])   // R0
             data[31:24] <= wb_dat_in[31:24];              
        if(byte_sel[2])
             data[23:16] <= wb_dat_in[23:16];             
        if(byte_sel[1])
             data[15:8] <= wb_dat_in[15:8];                     
         if(byte_sel[0])
             data[7:0] <= wb_dat_in[7:0];                          
      end
     else if (latch[1]&& !tip)   // R1
       begin
         if(byte_sel[3])
             data[63:56] <= wb_dat_in[31:24];               
        if(byte_sel[2])
             data[55:48] <= wb_dat_in[23:16];               
        if(byte_sel[1])
             data[47:40] <= wb_dat_in[15:8];                     
         if(byte_sel[0])
             data[39:32] <= wb_dat_in[7:0];                              
       end  
       
     else if (latch[2]&& !tip)   // R2
       begin
         if(byte_sel[3])
             data[95:88] <= wb_dat_in[31:24];              
        if(byte_sel[2])
             data[87:80] <= wb_dat_in[23:16];              
        if(byte_sel[1])
             data[79:72] <= wb_dat_in[15:8];                     
         if(byte_sel[0])
             data[71:64] <= wb_dat_in[7:0];                              
       end        
  
     else if (latch[3]&& !tip)  //R3
       begin
         if(byte_sel[3])
             data[127:120] <= wb_dat_in[31:24];              
        if(byte_sel[2])
             data[119:112] <= wb_dat_in[23:16];               
        if(byte_sel[1])
             data[111:104] <= wb_dat_in[15:8];                     
         if(byte_sel[0])
             data[103:96] <= wb_dat_in[7:0];                              
       end         
     else    // transfer i2c actif  mais en i2c read !!! lec_ec == 1
     begin
        if (tip && le_el  && multi_byte && Rx_1byte)    // lecture i2c
           begin
                if(i2c_latch == 4'h0)
                  data[7:0] <= Rx_Byte_in[7:0]; 
           else if(i2c_latch == 4'h1)
                  data[15:8]<= Rx_Byte_in[7:0]; 
           else if(i2c_latch == 4'h2)
                  data[23:16]<= Rx_Byte_in[7:0]; 
           else if(i2c_latch == 4'h3)
                  data[31:24]<= Rx_Byte_in[7:0];    
           else if(i2c_latch == 4'h4)
                  data[39:32]<= Rx_Byte_in[7:0];                 
           else if(i2c_latch == 4'h5)
                  data[47:40]<= Rx_Byte_in[7:0];              
           else if(i2c_latch == 4'h6)
                  data[55:48]<= Rx_Byte_in[7:0];                       
           else if(i2c_latch == 4'h7)
                  data[63:56]<= Rx_Byte_in[7:0];      
           else if(i2c_latch == 4'h8)
                  data[71:64]<= Rx_Byte_in[7:0];                 
           else if(i2c_latch == 4'h9)
                  data[79:72]<= Rx_Byte_in[7:0];
           else if(i2c_latch == 4'ha)
                  data[87:80]<= Rx_Byte_in[7:0];               
           else if(i2c_latch == 4'hb)
                  data[95:88]<= Rx_Byte_in[7:0];                                   
           else if(i2c_latch == 4'hc)
                  data[103:96]<= Rx_Byte_in[7:0];
           else if(i2c_latch == 4'hd)
                  data[111:104]<= Rx_Byte_in[7:0];               
           else if(i2c_latch == 4'he)
                  data[119:112]<= Rx_Byte_in[7:0];                      
           else if(i2c_latch == 4'hf)
                  data[127:120]<= Rx_Byte_in[7:0];                                                     
         end
     end     
  end
  
always @(posedge clk )
 begin
   if(i2c_latch == 4'h0)                            
      i2c_dat_out <= data[31:24];   // ------modif 7.3.13--------------R0 
  else if(i2c_latch == 4'h1)
      i2c_dat_out <= data[23:16];  
  else if(i2c_latch == 4'h2)
      i2c_dat_out <= data[15:8];  
  else if(i2c_latch == 4'h3)
      i2c_dat_out <= data[7:0];           
  else if(i2c_latch == 4'h4)   //------------------------------R1
      i2c_dat_out <= data[63:56];
  else if(i2c_latch == 4'h5)
      i2c_dat_out <= data[55:48];
  else if(i2c_latch == 4'h6)
      i2c_dat_out <= data[47:40];      
  else if(i2c_latch == 4'h7)
      i2c_dat_out <= data[39:32]; 
  else if(i2c_latch == 4'h8)   //------------------------------R2
      i2c_dat_out <= data[95:88];
  else if(i2c_latch == 4'h9)
      i2c_dat_out <= data[87:80];
  else if(i2c_latch == 4'ha)
      i2c_dat_out <= data[79:72];      
  else if(i2c_latch == 4'hb)
      i2c_dat_out <= data[71:64]; 
  else if(i2c_latch == 4'hc)   //------------------------------R3
      i2c_dat_out <= data[127:120];
  else if(i2c_latch == 4'hd)
      i2c_dat_out <= data[119:112];
  else if(i2c_latch == 4'he)
      i2c_dat_out <= data[111:104];      
  else if(i2c_latch == 4'hf)
      i2c_dat_out <= data[103:96];                                 
 end  
endmodule







//-----------------------------------------
//`include "spi_open_core/spi_defines.v"
// ChP    `include "timescale.v"

module wb_generic
(
  // Wishbone signals
  wb_clk_i, wb_rst_i, wb_adr_i, wb_dat_i, wb_dat_o, wb_sel_i,
  wb_we_i, wb_stb_i, wb_cyc_i, wb_ack_o, wb_err_o, wb_int_o,
  
  // sortie des registre I2C
  registre0,i2c_msk_reg,tip,last_bit,status_i2c,i2c_ctrl_reg,
  //input 
  rx_byte_in,data_reg_sel,rx

);

  parameter Tp = 1;

  // Wishbone signals
  input                            wb_clk_i;         // master clock input
  input                            wb_rst_i;         // synchronous active high reset
  input                      [4:0] wb_adr_i;         // lower address bits
  input                   [32-1:0] wb_dat_i;         // databus input
  output                  [32-1:0] wb_dat_o;         // databus output
  input                      [3:0] wb_sel_i;         // byte select inputs
  input                            wb_we_i;          // write enable input
  input                            wb_stb_i;         // stobe/core select signal
  input                            wb_cyc_i;         // valid bus cycle input
  output                           wb_ack_o;         // bus cycle acknowledge output
  output                           wb_err_o;         // termination w/ error
  output                           wb_int_o;         // interrupt request signal output
  output                    [31:0] i2c_ctrl_reg; 
  reg                       [31:0] i2c_ctrl_reg;        // registre de control I2C CRA
  output                     [3:0] data_reg_sel;     // selection dur reg de 32 bit sur 128 a ecrire
  input                  [128-1:0] rx;               // Rx register
 
  // interface avec i2c ccu
  input                            tip;              // transfer in progress    busy
  input                            last_bit;         // marks last character bit    
  input [7:0]                      status_i2c;       // registre de status du bloc i2c ccu                                          
  input [7:0]                      rx_byte_in;      // resultat lecture single byte a6 et a10
  // sortie des registres
                                                     
  reg                     [32-1:0] wb_dat_o;
  reg                              wb_ack_o;
  reg                              wb_int_o;
  
                                               
   
  output       [`REG_LEN-1:0]   registre0;               // Divider register
  output       [`REG_LEN-1:0]   i2c_msk_reg;         // registre mask
  
  
  reg       [`REG_LEN-1:0]   registre0;               // Divider register
  reg       [`REG_LEN-1:0]   i2c_msk_reg;               // mask
 
    
  reg                     [32-1:0] wb_dat;           // wb data out
  
 
   wire             registre0_sel ;// decodage registre 0 
   wire             i2c_ctrl_sel  ;// decodage registre de control 
   wire             i2c_msk_sel   ;// decodage pour reg mask
   wire     [3:0]   data_reg_sel ;// decodage registre data multibyte i2c
 
    
  // decodage des 7 registres du slave
  assign registre0_sel = wb_cyc_i & wb_stb_i & (wb_adr_i[`SPI_OFS_BITS] == `GENERIC_R0);
  assign i2c_ctrl_sel  = wb_cyc_i & wb_stb_i & (wb_adr_i[`SPI_OFS_BITS] == `I2C_CTRL); 
  assign i2c_msk_sel   = wb_cyc_i & wb_stb_i & (wb_adr_i[`SPI_OFS_BITS] == `I2C_MSK);
  assign data_reg_sel[0] = wb_cyc_i & wb_stb_i & (wb_adr_i[`SPI_OFS_BITS] == `DATA_REG0); 
  assign data_reg_sel[1] = wb_cyc_i & wb_stb_i & (wb_adr_i[`SPI_OFS_BITS] == `DATA_REG1); 
  assign data_reg_sel[2] = wb_cyc_i & wb_stb_i & (wb_adr_i[`SPI_OFS_BITS] == `DATA_REG2); 
  assign data_reg_sel[3] = wb_cyc_i & wb_stb_i & (wb_adr_i[`SPI_OFS_BITS] == `DATA_REG3);  
  
    
  // Read from registers
  always @(wb_adr_i or registre0  or status_i2c or i2c_msk_reg or i2c_ctrl_reg or rx or rx_byte_in )    //  zarbi
  begin
    case (wb_adr_i[`SPI_OFS_BITS])
      `DATA_REG0:    wb_dat = rx[31:0];  // non-bloc  -> bloc 7.3.13
      `DATA_REG1:    wb_dat = rx[63:32];
      `DATA_REG2:    wb_dat = rx[95:64];
      `DATA_REG3:    wb_dat = rx[127:96];
      `STATUS_REG:   begin
                       wb_dat[31:24] = status_i2c[7:0];    // lecture du status ccu i2c
                       wb_dat[23:16] = rx_byte_in[7:0];   // lecture resultat i2c read
                       wb_dat[15:0] = 16'hbb;
                    end
      `GENERIC_R0:    wb_dat = {{32-`GENERIC_R0_LEN{1'b0}}, registre0};
//      `GENERIC_R1:    wb_dat <= {{32-`GENERIC_R1_LEN{1'b0}}, registre1};
      `I2C_CTRL:     wb_dat =  i2c_ctrl_reg;
      `I2C_MSK:      wb_dat =   i2c_msk_reg;
      
      default:      wb_dat = 128'h0;
    endcase
  end
  
  // Wb data out
  always @(posedge wb_clk_i or posedge wb_rst_i)
  begin
    if (wb_rst_i)
      wb_dat_o <= #Tp 32'b0;
    else
      wb_dat_o <= #Tp wb_dat;
  end
  
  // Wb acknowledge
  always @(posedge wb_clk_i or posedge wb_rst_i)
  begin
    if (wb_rst_i)
      wb_ack_o <= #Tp 1'b0;
    else
      wb_ack_o <= #Tp wb_cyc_i & wb_stb_i & ~wb_ack_o;
  end
  
  // Wb error
  assign wb_err_o = 1'b0;
  
  // Interrupt
  always @(posedge wb_clk_i or posedge wb_rst_i)
  begin
    if (wb_rst_i)
      wb_int_o <= #Tp 1'b0;      // reset de l'interrupt
    else if (tip && last_bit  )
      wb_int_o <= #Tp 1'b1;      // interrupt actif
    else if (wb_ack_o)
      wb_int_o <=  1'b0;   // reset interrupt apres la lecture ( ou ecriture) de n'importe quel registre 
	// else if((wb_adr_i[`SPI_OFS_BITS]==`STATUS_REG) && wb_cyc_i && wb_stb_i && (~wb_we_i))
	//	wb_int_o <=  1'b0;
  end
  
  // write registre 0
  always @(posedge wb_clk_i or posedge wb_rst_i)
  begin
    if (wb_rst_i)
     begin
       registre0 <= #Tp {32{1'b0}};
//       registre1 <= #Tp {32{1'b0}};       
//       registre2 <= #Tp {32{1'b0}};  
       i2c_ctrl_reg <= #Tp 32'h8;   // provisoire   bit [5:2] = 2
       i2c_msk_reg <= #Tp {32{1'b0}}; 
     //  rx <= 128'h0;    //registre pour transfert multiple
  
      end
    else if (registre0_sel && wb_we_i && !tip)
      begin
        if (wb_sel_i[0])
          registre0[7:0]  <= #Tp wb_dat_i[7:0];
        if (wb_sel_i[1])
          registre0[15:8] <= #Tp wb_dat_i[15:8];
        if (wb_sel_i[2])
          registre0[23:16] <= #Tp wb_dat_i[23:16];
        if (wb_sel_i[3])
          registre0[31:24] <= #Tp wb_dat_i[ 31:24];
      end           
    else if (i2c_msk_sel && wb_we_i && !tip)
      begin
        if (wb_sel_i[0])
          begin
          i2c_msk_reg[7:0]   <= #Tp wb_dat_i[7:0];
       //   i2c_msk_reg[31:8]  <= #Tp 24'h0;   // clear les bit du haut
          end 
        if (wb_sel_i[1])
          i2c_msk_reg[15:8] <= #Tp wb_dat_i[15:8];
        if (wb_sel_i[2])
          i2c_msk_reg[23:16] <= #Tp wb_dat_i[23:16];
        if (wb_sel_i[3])
          i2c_msk_reg[31:24] <= #Tp wb_dat_i[ 31:24];
      end
      
      
    else if (i2c_ctrl_sel && wb_we_i && !tip)
      begin
        if (wb_sel_i[0])
          i2c_ctrl_reg[7:0]  <= #Tp wb_dat_i[7:0];
        if (wb_sel_i[1])
          i2c_ctrl_reg[15:8] <= #Tp wb_dat_i[15:8];
        if (wb_sel_i[2])
          i2c_ctrl_reg[23:16] <= #Tp wb_dat_i[23:16];
        if (wb_sel_i[3])
          i2c_ctrl_reg[31:24] <= #Tp wb_dat_i[ 31:24];
      end           
  end  
  
endmodule
  




module master_cmd_sm (clock2, cmd, rx_byte_o, busy, sda, scl, sda_bi, reset,
                      start_i2c, ack_ok, data_in, data_in2, nbyte, sda_gnd,
                      ext_mode, MultiByte, multi_byte_in, le_ec, rx_1byte,
                      ext_multi_mode, rw_b_ext, stop_cond, blktr_idx
                      );
 
  input clock2;
  input [7:0] cmd;
  output [7:0] rx_byte_o;
  reg [7:0] rx_byte_o;
  output busy;
  reg busy;
  output sda;
  reg sda;
  output scl;
  reg scl;
  input sda_bi;
  wire sda_bi;
  input reset;
  input start_i2c;
  output ack_ok;
  reg ack_ok;
  input [7:0] data_in;
  input [7:0] data_in2;
  input [4:0] nbyte;
  output sda_gnd;
  reg sda_gnd;
  input ext_mode;
  wire ext_mode;
  input MultiByte;        
  wire MultiByte;
  input [7:0] multi_byte_in; 
  wire [7:0] multi_byte_in;
  output le_ec;
  reg le_ec;
  output rx_1byte;         
  reg rx_1byte;
  input ext_multi_mode;
  wire ext_multi_mode;
  input rw_b_ext;
  wire rw_b_ext;
  output stop_cond;
  reg stop_cond;
  output [3:0] blktr_idx;  
  reg [3:0] blktr_idx;
  reg [3:0] nb_bit;
  reg [7:0] address;
  reg [7:0] rx_byte;
  reg status;
  reg [7:0] registre;
  reg ack_multi;           
  reg [4:0] nb_byte;       
  reg [3:0] i2c_mode;
 
  parameter rst_state = 6'b000000,
            Ack_read  = 6'b000001,
            READ      = 6'b000010,
            S30       = 6'b000011,
            S31       = 6'b000100,
            S32       = 6'b000101,
            S44       = 6'b000110,
            S46busy_o = 6'b000111,
            main      = 6'b001000,
            repos     = 6'b001001,
            s10       = 6'b001010,
            s17Rd     = 6'b001011,
            s22       = 6'b001100,
            s4Rd      = 6'b001101,
            s5Rd      = 6'b001110,
            s_dat_rdy = 6'b001111,
            thd_sda   = 6'b010000,
            s7_stop   = 6'b010001,
            s6_stop   = 6'b010010,
            s8_stop   = 6'b010011,
            tsu_sto   = 6'b010100,
            S0        = 6'b010101,
            S1        = 6'b010110,
            S11       = 6'b010111,
            S9        = 6'b011000,
            S3_scl1   = 6'b011001,
            S18delai  = 6'b011010,
            S19       = 6'b011011,
            S4        = 6'b011100,
            S5        = 6'b011101,
            S6_scl0   = 6'b011110,
            S8_sda_1  = 6'b011111,
            TST_ACK   = 6'b100000;
 
 
  reg [5:0] visual_rst_state_current;

  // Synchronous process
  always  @(posedge clock2 or posedge reset)
  begin : master_cmd_sm_rst_state
 
    if (reset === 1'b1)
    begin
      status <= 1'b0;
      rx_byte_o <= 8'h0;
      stop_cond <= 1'b0;
      blktr_idx <= 4'h0;
      visual_rst_state_current <= rst_state;
    end
    else
    begin
 	status    <= status;
      rx_byte_o <= rx_byte_o;
      stop_cond <= stop_cond;
      blktr_idx <= blktr_idx;
      visual_rst_state_current <= visual_rst_state_current;
      busy <= busy;
      scl <= scl;
      sda <= sda;
      nb_byte <= nb_byte;
      le_ec <= le_ec;
      address <= address;
      ack_multi <= ack_multi;
      rx_1byte <= rx_1byte;
      nb_bit <= nb_bit;
      ack_ok <= ack_ok;
      registre <= registre;
      sda_gnd <= sda_gnd;  
	i2c_mode <= i2c_mode;
	
      case (visual_rst_state_current)
        rst_state:
          begin
            busy <= 1'b0;
            scl <= 1'b1;
            sda <= 1'b1;
            nb_byte <= 5'h0;
            le_ec <= 1'b0;
            address <= 8'h0;
            ack_multi <= 1'b0;
            rx_1byte <= 1'b0;
            nb_bit <= 4'b1000;
            ack_ok <= 1'b0;
            registre <= 8'h0;
            sda_gnd <= 1'b0;  
            blktr_idx <= 4'h0;
            visual_rst_state_current <= repos;
          end
 
        Ack_read:
          begin
            if (nb_byte == nbyte)
            begin
              sda <= 1'b0;
              visual_rst_state_current <= s7_stop;
            end
            else if ((nb_byte != nbyte) && MultiByte)
            begin
              ack_multi <= 1'b0;
              sda <= 1'b1;
              rx_1byte <= 1'b0;
              visual_rst_state_current <= READ;
            end
            else
              visual_rst_state_current <= Ack_read;
          end
 
        READ:
          begin
            scl <= 1'b1;
            visual_rst_state_current <= s4Rd;
          end
 
        S30:
          begin
            scl <= 1'b1;
            visual_rst_state_current <= S31;
          end
 
        S31:
          begin
            visual_rst_state_current <= S32;
          end
 
        S32:
          begin
            scl <= 1'b0;
            nb_bit <= 4'b0111;
            visual_rst_state_current <= Ack_read;
          end
 
        S44:
          begin
            sda <= address[7];
            visual_rst_state_current <= s_dat_rdy;
          end
 
        S46busy_o:
          begin
            busy <= 1'b0;
            scl <= 1'b1;
            sda <= 1'b1;
            nb_byte <= 5'h0;
            le_ec <= 1'b0;
            address <= 8'h0;
            ack_multi <= 1'b0;
            rx_1byte <= 1'b0;
            nb_bit <= 4'b1000;
            ack_ok <= 1'b0;
            registre <= 8'h0;
            sda_gnd <= 1'b0;  
            blktr_idx <= 4'h0;
            visual_rst_state_current <= repos;
          end
 
        main:
          begin
            if (nb_byte == 5'h0)
            begin
              sda <= 1'b0;
              ack_ok <= 1'b0;
              visual_rst_state_current <= s10;
            end
            else if ((nb_byte > 5'h0) && (cmd[0]))
            begin
              nb_bit <= (nb_bit - 4'b1);
              blktr_idx <= 4'h0;
              ack_multi <= 1'b0;
              sda <= 1'b1;
              rx_1byte <= 1'b0;
              visual_rst_state_current <= READ;
            end
            else if (nb_byte > 5'b00000)
            begin
              scl <= 1'b1;
              visual_rst_state_current <= S3_scl1;
            end
            else
              visual_rst_state_current <= main;
          end
 
        repos:
          begin
            if (start_i2c)
            begin
              nb_byte <= 5'h0;
              le_ec <= cmd[0];
              address <= cmd;  
              busy <= 1'b1;
              sda <= 1'b1;
              status <= 1'b0; 
              stop_cond <= 1'b0;
              rx_1byte <= 1'b0;
              i2c_mode <= {ext_multi_mode,rw_b_ext,MultiByte,cmd[0]};
 
              if((nb_byte != 5'b00000) && (nb_byte != nbyte))
              begin
              case  ({ext_multi_mode,rw_b_ext,MultiByte,cmd[0]})
                     4'b0000,4'b0100,4'b1000,4'b1100:
                    begin
                      if (nb_byte == 5'b00001)
                         begin
                          address <= data_in;
                          sda <=     data_in[7];
                         end
                      if (nb_byte == 5'b00010)
                         begin
                          address <= data_in2;
                          sda <=     data_in2[7];
                         end
                    end
                   4'b1110:
                     begin
                      if(nb_byte == 5'b00010)
                        begin
                         address <= {cmd[7:1],1'b1};
                         sda <= cmd[7];
                        end
                      if(nb_byte == 5'b00001)
                         begin
                          address <= data_in;
                          sda <=     data_in[7];
                      end
                      if(nb_byte >= 5'b00011)
                         begin
                           address <= multi_byte_in;
                           sda <=    multi_byte_in[7];
                         end
                      end
                   4'b0110,4'b0010:  
                        begin
                         address <= multi_byte_in;
                         sda <= multi_byte_in[7];
                        end
                   4'b1010:   
                       begin
                          if(nb_byte == 5'b00001)
                             begin
                                address <= data_in;  
                                sda <= data_in[7];
                             end
                           else
                              begin
                                address <= multi_byte_in;
                                sda <= multi_byte_in[7];
                              end
                       end
 
 
                   default:
                       address <= 1;
              endcase
              end 
              visual_rst_state_current <= main;
            end
            else if ((sda_bi == 1'b1) && (!(start_i2c)))
            begin
              sda_gnd <= 1'b0;
              visual_rst_state_current <= repos;
            end
            else if ((sda_bi == 1'b0) && (!(start_i2c)))
            begin
              sda_gnd <= 1'b1;
              visual_rst_state_current <= repos;
            end
            else
              visual_rst_state_current <= repos;
          end
 
        s10:
          begin
            visual_rst_state_current <= thd_sda;
          end
 
        s17Rd:
          begin
            if (nb_bit == 4'b0000)
            begin
              rx_byte_o <= registre;
              scl <= 1'b0;
              nb_byte <= nb_byte + 5'h1; 
              blktr_idx <= blktr_idx + 4'h1;
              visual_rst_state_current <= s22;
            end
            else
            begin
              scl <= 1'b0;
              registre <= (registre << 1);
              nb_bit <= (nb_bit-4'b1);
              visual_rst_state_current <= s5Rd;
            end
          end
 
        s22:
          begin
            rx_1byte <= 1'b1;
            if (MultiByte && (nb_byte != nbyte))
              begin
                ack_multi <= 1'b1;
                sda <= 1'b0;
               end
            else
              sda <= 1'b1;
            visual_rst_state_current <= S30;
          end
 
        s4Rd:
          begin
              registre[0] <= sda_bi;
            visual_rst_state_current <= s17Rd;
          end
 
        s5Rd:
          begin
            ack_multi <= 1'b0;
            sda <= 1'b1;
            rx_1byte <= 1'b0;
            visual_rst_state_current <= READ;
          end
 
        s_dat_rdy:
          begin
            scl <= 1'b1;
            visual_rst_state_current <= S3_scl1;
          end
 
        thd_sda:
          begin
            scl <= 1'b0;
            visual_rst_state_current <= S44;
          end
 
        s7_stop:
          begin
            scl <= 1'b1;
            stop_cond <= 1'b1;
            visual_rst_state_current <= s8_stop;
          end
 
        s6_stop:
          begin
            busy<= 1'b0;
            visual_rst_state_current <= S46busy_o;
          end
 
        s8_stop:
          begin
            visual_rst_state_current <= tsu_sto;
          end
 
        tsu_sto:
          begin
            sda <= 1'b1;
            visual_rst_state_current <= s6_stop;
          end
 
        S0:
          begin
            scl <= 1'b1;
            visual_rst_state_current <= S1;
          end
 
        S1:
          begin
            visual_rst_state_current <= S11;
          end
 
        S11:
          begin
            sda <= 1'b1;
            visual_rst_state_current <= S9;
          end
 
        S9:
          begin
            busy<= 1'b0;
            visual_rst_state_current <= S46busy_o;
          end
 
        S3_scl1:
          begin
            nb_bit <= (nb_bit - 4'b1);
            address <= (address << 1);
            visual_rst_state_current <= S4;
          end
 
        S18delai:
          begin
            if ((!(status)) && (nb_byte == nbyte && ext_mode == 1'b0))
            begin
              sda <= 1'b0;
              visual_rst_state_current <= s7_stop;
            end
            else if ((!(status)) && ((nb_byte == nbyte) && (ext_mode == 1'b1)))
            begin
              sda <= 1'b1;
              visual_rst_state_current <= S0;
            end
            else if ((!(status)) && (nb_byte != nbyte))
            begin
              rx_1byte <= 1'b0;
 
              i2c_mode <= {ext_multi_mode,rw_b_ext,MultiByte,cmd[0]};
 
              if((nb_byte != 5'b00000) && (nb_byte != nbyte))

              begin
              case  ({ext_multi_mode,rw_b_ext,MultiByte,cmd[0]})
                     4'b0000,4'b0100,4'b1000,4'b1100:
                    begin
                      if (nb_byte == 5'b00001)
                         begin
                          address <= data_in;
                          sda <=     data_in[7];
                         end
                      if (nb_byte == 5'b00010)
                         begin
                          address <= data_in2;
                          sda <=     data_in2[7];
                         end
                    end
                   4'b1110:
                     begin
                      if(nb_byte == 5'b00010)
                        begin
                         address <= {cmd[7:1],1'b1};
                         sda <= cmd[7];
                        end
                      if(nb_byte == 5'b00001)
                         begin
                          address <= data_in;
                          sda <=     data_in[7];
                      end
                      if(nb_byte >= 5'b00011)
                         begin
                           address <= multi_byte_in;
                           sda <=    multi_byte_in[7];
                         end
                      end
                   4'b0110,4'b0010:  
                        begin
                         address <= multi_byte_in;
                         sda <= multi_byte_in[7];
                        end
                   4'b1010:    
                       begin
                          if(nb_byte == 5'b00001)
                             begin
                                address <= data_in;  
                                sda <= data_in[7];
                             end
                           else
                              begin
                                address <= multi_byte_in;
                                sda <= multi_byte_in[7];
                              end
                       end
 
 
                   default:
                       address <= 1;
              endcase
              end 
              visual_rst_state_current <= main;
            end
            else if (status)
            begin
              sda <= 1'b0;
              visual_rst_state_current <= s7_stop;
            end
            else
            begin
              scl <= 1'b1;
              visual_rst_state_current <= S3_scl1;
            end
          end
 
        S19:
          begin
            scl <= 1'b0;
            visual_rst_state_current <= S18delai;
          end
 
        S4:
          begin
            scl <= 1'b0;
            visual_rst_state_current <= S6_scl0;
          end
 
        S5:
          begin
            scl <= 1'b1;
            visual_rst_state_current <= S3_scl1;
          end
 
        S6_scl0:
          begin
            if (nb_bit == 4'b0000)
            begin
              sda <= 1'b1;
              if(!le_ec && ext_multi_mode && (nb_byte >= 5'b00010))
                blktr_idx <= blktr_idx + 4'b0001;
              if(!le_ec && MultiByte && (!ext_multi_mode) && (nb_byte >=
               5'b00001))
                blktr_idx <= blktr_idx + 4'b0001;
              visual_rst_state_current <= S8_sda_1;
            end
            else
            begin
              sda <= address[7];
              visual_rst_state_current <= S5;
            end
          end
 
        S8_sda_1:
          begin
            scl <= 1'b1;
            if(sda_bi == 1'b1)
                begin
                 status <= 1'b1;
                 ack_ok <= 1'b0;
                 end
            else
               begin
                 status <= 1'b0;
                 ack_ok <= 1'b1;
               end
            visual_rst_state_current <= TST_ACK;
          end
 
        TST_ACK:
          begin
            nb_byte <= nb_byte + 5'b00001;
            nb_bit <= 4'b1000;
            visual_rst_state_current <= S19;
          end
 
        default:
          begin
            status <= 1'b0;
            rx_byte_o <= 8'h0;
            stop_cond <= 1'b0;
            blktr_idx <= 4'h0;
            visual_rst_state_current <= rst_state;
          end
      endcase
    end
  end

endmodule





module div_prog_sm (clk, clk_out, reset, div
                    );
 
  input clk;
  output clk_out;
  input reset;
  input [1:0] div;
  
  reg clk_out;
  reg [6:0] compt;
  reg [6:0] diviseur;
  reg clk_int;
  reg res;
  reg s1;
  reg s3;
  
   wire [2:0] visual_res_current = { s3,s1, res };

  // Synchronous process
  always  @(posedge clk or posedge reset)
  begin : div_prog_sm_res
 
    if (reset === 1'b1)
    begin
      clk_out <= 1'b0;
      diviseur <= 7'd48;
      clk_int <= 1'b0;
      compt <= 7'h0;
      res <= 1'b1;
      s1 <= 1'b0;
      s3 <= 1'b0;
    end
    else
    begin
 	clk_out <= clk_out;
      diviseur <= diviseur;
      clk_int <= clk_int;
      compt <= compt;
      res <= res;
      s1 <= s1;
      s3 <= s3;
      case (1'b1)
        res:
          begin
            clk_int <= ~clk_int;
            compt <= 7'h0;
            res <= 1'b0;
            s3 <= 1'b1;
          end
 
        s1:
          begin
            if (compt < diviseur)
            begin
              compt <= (compt + 7'h1);
              s1 <= 1'b1;
            end
            else if (compt == diviseur)
            begin
              clk_out <= clk_int;
              clk_int <= ~clk_int;
              compt <= 7'h0;
              s1 <= 1'b0;
              s3 <= 1'b1;
            end
            else
            begin
              s1 <= 1'b1;
            end
          end
 
        s3:
          begin
            if (div == 2'b11)
            begin
              diviseur <=7'd3;
              s3 <= 1'b0;
              s1 <= 1'b1;
            end
            else if (div == 2'b0)
            begin
              diviseur <= 7'd48;
              s3 <= 1'b0;
              s1 <= 1'b1;
            end
            else if (div == 2'b01)
            begin
              diviseur <= 7'd23;
              s3 <= 1'b0;
              s1 <= 1'b1;
            end
            else if (div == 2'b10)
            begin
              diviseur <= 7'd11;
              s3 <= 1'b0;
              s1 <= 1'b1;
            end
            else
            begin
              s3 <= 1'b1;
            end
          end
 
      endcase
    end
  end

endmodule





module i_mask_op_fc (mask_in, data_in, data_pass, data_out, op, clk, reset
                     );
 
  input [7:0] mask_in;
  input [7:0] data_in;
  input [7:0] data_pass;
  output [7:0] data_out;
  input [1:0] op;
  input clk;
  input reset;
  reg [7:0] visual_0_data_out;
 
 
  always @( posedge (clk) or posedge (reset) )
  begin   :mask_op

    if (reset)
    begin
        visual_0_data_out <= 8'b00000000;
    end
    else
    begin
      visual_0_data_out <= visual_0_data_out;
      if (op == 2'b00)
      begin
          visual_0_data_out <= data_pass;
      end
      else
      begin
        if (op == 2'b01)
        begin
            visual_0_data_out <= (data_in & mask_in);
        end
        else
        begin
          if (op == 2'b10)
          begin
              visual_0_data_out <= (data_in | mask_in);
          end
          else
          begin
            if (op == 2'b11)
            begin
                visual_0_data_out <= (data_in ^ mask_in);
            end
          end
        end
      end
    end
  end
 
    assign data_out = visual_0_data_out;

endmodule




 
module decode_sm_back (reset, ack_ok_in, clk, cmd_in, nbyte, out_i2c, i2c_busy,
                       start_i2c, crc_ok, occup, addr_i2c, read_nc, rep_out,
                       msk_op, SRA, SDA_LOW, ext_mode, i2c_br, EBRDCST,
                       MultiByte, nbyte_in, ext_multi_mode, rw_b_ext,
                       stop_cond_in
                       );
//`include "Table_Commande.v"
 
  input reset;
  input ack_ok_in;
  input clk;
  input [7:0] cmd_in;
  wire [7:0] cmd_in;
  output [4:0] nbyte;
  reg [4:0] nbyte;
  output [7:0] out_i2c;
  reg [7:0] out_i2c;
  input i2c_busy;
  output start_i2c;
  reg start_i2c;
  input crc_ok;
  wire crc_ok;
  output occup;
  reg occup;
  input [7:0] addr_i2c;
  wire [7:0] addr_i2c;
  input read_nc;
  wire read_nc;
  output rep_out;
  reg rep_out;
  output [1:0] msk_op;
  reg [1:0] msk_op;
  output [7:0] SRA;
  reg [7:0] SRA;
  input SDA_LOW;
  wire SDA_LOW;
  output ext_mode;
  reg ext_mode;
  input i2c_br;
  wire i2c_br;
  input EBRDCST;
  wire EBRDCST;
  output MultiByte;        
  reg MultiByte;
  input [3:0] nbyte_in;
  wire [3:0] nbyte_in;
  output ext_multi_mode;   
  reg ext_multi_mode;
  output rw_b_ext;
  reg rw_b_ext;
  input stop_cond_in;
  wire stop_cond_in;
  reg lire_i2c;
  wire [7:0] com;
 
  parameter res             = 5'b00000,
            S55             = 5'b00001,
            S56             = 5'b00010,
            S58             = 5'b00011,
            State_start_i2c = 5'b00100,
            WaitBusy_st     = 5'b00101,
            a7_out          = 5'b00110,
            attente         = 5'b00111,
            lec_cmd         = 5'b01000,
            rd_wr_err       = 5'b01001,
            re_nc           = 5'b01010,
            sen_ad_10bit_2  = 5'b01011,
            send_ad_10bit   = 5'b01100,
            tr_cor          = 5'b01101,
            wait_r_nc       = 5'b01110,
            RMW_normal      = 5'b01111,
            RMW_ext         = 5'b10000,
            att_busy0       = 5'b10001,
            s13             = 5'b10010,
            s6              = 5'b10011,
            start_i2c1      = 5'b10100;
 
 
  reg [4:0] visual_res_current;
 
  // Synchronous process
  always  @(posedge clk or posedge reset)
  begin : decode_sm_back_res
 
    if (reset === 1'b1)
    begin
      out_i2c <= 8'h0;
      start_i2c <= 1'b0;
      lire_i2c <= 1'b0;
      msk_op <= 2'b0;
      SRA <= 8'h0;
      ext_mode <= 1'b0;
      nbyte <= 5'b0;
      rep_out <= 1'b1;
      MultiByte <= 1'b0;
      occup <= 1'b0;
      ext_multi_mode <= 1'b0;
      rw_b_ext <= 1'b0;  // write
      visual_res_current <= res;
    end
    else
    begin
		out_i2c <= out_i2c;
      start_i2c <= start_i2c;
      lire_i2c <= lire_i2c;
      msk_op <= msk_op;
      SRA <= SRA;
      ext_mode <= ext_mode;
      nbyte <= nbyte;
      rep_out <= rep_out;
      MultiByte <= MultiByte;
      occup <= occup;
      ext_multi_mode <= ext_multi_mode;
      rw_b_ext <= rw_b_ext;
      
      case (visual_res_current)
        res:
          begin
            start_i2c <= 1'b0;
            occup <= 1'b0;
            rep_out <= 1'b1;
            lire_i2c <= 1'b0;
            msk_op <= 2'b0;
            ext_mode <= 1'b0;
            MultiByte <= 1'b0;
            
            ext_multi_mode <= 1'b0;
            rw_b_ext <= 1'b0;  // 28.5.13
            visual_res_current <= attente;
          end
 
        S55:
          begin
            //lire_i2c = 1'b1;
            start_i2c <= 1'b1;
            SRA[6] <= 1'b0;
            lire_i2c <= 1'b1; //19nov2012
            visual_res_current <= State_start_i2c;
          end
 
        S56:
          begin
            visual_res_current <= tr_cor;
          end
 
        S58:
          begin
            if (i2c_busy)
            begin
              start_i2c <= 1'b0;
              visual_res_current <= S58;
            end
            else if ((~(i2c_busy)) && (!(ack_ok_in)))
            begin
              SRA[2] <= 1'b0;
              SRA[6] <= 1'b1;
              visual_res_current <= rd_wr_err;
            end
            else if ((~(i2c_busy)) && (!(lire_i2c) && ack_ok_in))
            begin
              SRA[2] <= 1'b1;
              visual_res_current <= S56;
            end
            else if ((~(i2c_busy)) && (lire_i2c && ack_ok_in))
            begin
              SRA[2] <= 1'b1;
              rep_out <= 1'b0;
              visual_res_current <= wait_r_nc;
            end
            else
              visual_res_current <= S58;
          end
 
        State_start_i2c:
          begin
            start_i2c <= 1'b1;
            visual_res_current <= WaitBusy_st;
          end
 
        WaitBusy_st:
          begin
            if (i2c_busy == 1'b0)
            begin
              visual_res_current <= WaitBusy_st;
            end
            else
            begin
              visual_res_current <= S58;
            end
          end
 
        a7_out:
          begin
            if (com >= 8'h10 && 8'h15 >= com)
            begin
              if (com >= 8'h10 && 8'h12 >= com)
              begin
                nbyte <= 4'b0010;
                out_i2c[0] <= 1'b1;// addresse[0] = 1 read
                visual_res_current <= RMW_normal;
              end
              else if ((com >= 8'h13) && (8'h15 >= com ))
              begin
                nbyte <= 4'b0011;
                visual_res_current <= RMW_ext;
              end
              else
              begin
                nbyte <= 4'b0010;
                out_i2c[0] <= 1'b1;// addresse[0] = 1 read
                visual_res_current <= RMW_normal;
              end
            end
            else if (com == 8'h0)
            begin
              nbyte <= 5'b0010;
              out_i2c[0] <= 1'b0;
              visual_res_current <= S55;
            end
            else if (com == 8'h2)
            begin
              nbyte <= 5'b0011;
              rw_b_ext <= 1'b0;//write
              out_i2c[0] <= 1'b0;
              visual_res_current <= S55;
            end
            else if (com == 8'h16)
            begin
              MultiByte <= 1'b1;
              if(nbyte_in == 4'h0)
                  nbyte <= 5'h11;  // 29.3.13
              else
                  nbyte <= {1'b0,nbyte_in}+5'h1 ;
              out_i2c[0] <= 1'b0;
              visual_res_current <= S55;
            end
            else if (com == 8'h1)
            begin
              nbyte <= 5'b0010;
              out_i2c[0] <= 1'b1;
              //lecture
              start_i2c <= 1'b1;
              SRA[6] <= 1'b0;
              lire_i2c <= 1'b1; //19nov2012
              visual_res_current <= State_start_i2c;
            end
            else if (com == 8'h17)
            begin
              MultiByte <= 1'b1;
              if(nbyte_in == 4'h0)
                 nbyte <= 5'h11;
              else
                 nbyte <= {1'b0,nbyte_in} + 5'h1; 
              out_i2c[0] <= 1'b1;
              //lecture
              start_i2c <= 1'b1;
              SRA[6] <= 1'b0;
              lire_i2c <= 1'b1; //19nov2012
              visual_res_current <= State_start_i2c;
            end
//Extended I2C address mode disabled
/*          else if (com == {3'b0, I2C_write_multi_ext[6:2]})
            begin
              MultiByte <= 1'b1;
              nbyte <= {1'b0,nbyte_in} +5'h2;
              ext_multi_mode <= 1'b1;
              rw_b_ext <= 1'b0;
              out_i2c[0] <= 1'b0;
              visual_res_current <= S55;
            end
            else if (com == {3'b0, I2C_read_multi_ext[6:2]})
            begin
              MultiByte <= 1'b1;
              ext_mode <= 1'b1;
              nbyte <=   5'b00010; // pour addr multi
              ext_multi_mode <= 1'b1;
              start_i2c <= 1'b1;
              rw_b_ext <= 1'b1;  //r
              visual_res_current <= send_ad_10bit;
            end
*/            
//				else if (com == 8'h3)
//            begin
//              ext_mode <= 1'b1;
//              nbyte <= 5'b0010;
//              start_i2c <= 1'b1;
//              out_i2c[0]<= 1'b0;
//              rw_b_ext <= 1'b1; //r
//              visual_res_current <= send_ad_10bit;
//            end
            else
            begin
              SRA[5]  <= 1'b1;
              visual_res_current <= tr_cor;
            end
          end
 
        attente:
          begin
            if (crc_ok)
            begin
              visual_res_current <= attente;
            end
            else if (!(crc_ok) && ((i2c_br && EBRDCST) || !(i2c_br)))
            begin
              visual_res_current <= lec_cmd;
            end
            else
              visual_res_current <= attente;
          end
 
        lec_cmd:
          begin
            if (SDA_LOW == 1'b0)
            begin
              occup <= 1'b1; //nov 2012
              out_i2c <= (addr_i2c << 1);
              SRA[2] <= 1'b0;
              visual_res_current <= a7_out;
            end
            else if (SDA_LOW == 1'b1)
            begin
              occup <= 1'b1; //nov 2012
              
              SRA <= 8'h08;
              
              rep_out <= 1'b0;
              visual_res_current <= wait_r_nc;
            end
            else
              visual_res_current <= lec_cmd;
          end
 
        rd_wr_err:
          begin
            rep_out <= 1'b0;
            visual_res_current <= wait_r_nc;
          end
 
        re_nc:
          begin
            if (read_nc)
            begin
              visual_res_current <= re_nc;
            end
            else if (!(read_nc))
            begin
              visual_res_current <= tr_cor;
            end
            else
              visual_res_current <= re_nc;
          end
/* 
        sen_ad_10bit_2:
          begin
            if (i2c_busy)
            begin
              visual_res_current <= sen_ad_10bit_2;
            end
            else if (!(i2c_busy) && !(stop_cond_in))
            begin
              nbyte <= 5'h8;
              
              out_i2c[0] <= 1'b1;
//		Removed because of synth errors
              if(com == {3'b0,I2C_read_multi_ext[6:2]})
                  nbyte <= {1'b0,nbyte_in}+ 5'h1;
              else
                  nbyte <= 5'h2;
              start_i2c <= 1'b1;
              SRA[6] <= 1'b0;
              lire_i2c <= 1'b1; //19nov2012
              visual_res_current <= State_start_i2c;
            end
            else if (stop_cond_in)
            begin
              rep_out <= 1'b0;
              visual_res_current <= wait_r_nc;
            end
            else
              visual_res_current <= sen_ad_10bit_2;
          end
*/
/*
        send_ad_10bit:
          begin
            if (!(i2c_busy))
            begin
              visual_res_current <= send_ad_10bit;
            end
            else if (i2c_busy)
            begin
              visual_res_current <= sen_ad_10bit_2;
            end
            else
              visual_res_current <= send_ad_10bit;
          end
*/ 
        tr_cor:
          begin
            start_i2c <= 1'b0;
            occup <= 1'b0;
            rep_out <= 1'b1;
            lire_i2c <= 1'b0;
            msk_op <= 2'b0;
            ext_mode <= 1'b0;
            MultiByte <= 1'b0;
            ext_multi_mode <= 1'b0;
            rw_b_ext <= 1'b0;  // 28.5.13
            visual_res_current <= attente;
          end
 
        wait_r_nc:
          begin
            if (read_nc)
            begin
              visual_res_current <= re_nc;
            end
            else if (!(read_nc))
            begin
              visual_res_current <= wait_r_nc;
            end
            else
              visual_res_current <= wait_r_nc;
          end
 
        RMW_normal:
          begin
             start_i2c <= 1'b1;
            visual_res_current <= start_i2c1;
          end
 
        RMW_ext:
          begin
             start_i2c <= 1'b1;
            visual_res_current <= start_i2c1;
          end
 
        att_busy0:
          begin
            if (i2c_busy)
            begin
              visual_res_current <= s6;
            end
            else if (!(i2c_busy))
            begin
              visual_res_current <= att_busy0;
            end
            else
              visual_res_current <= att_busy0;
          end
 
        s13:
          begin
            if (ack_ok_in)
            begin
              
              visual_res_current <= S55;
            end
            else if (!(ack_ok_in))
            begin
              
              SRA[6] <= 1'b1;
              visual_res_current <= rd_wr_err;
            end
            else
            begin
              
              if ((com >= 8'h10) && (8'h12 >= com))
              begin
                nbyte <= 4'b0010;
                out_i2c[0] <= 1'b1;// addresse[0] = 1 read
                visual_res_current <= RMW_normal;
              end
              else if ((com >= 8'h13) && (8'h15 >= com))
              begin
                nbyte <= 4'b0011;
                visual_res_current <= RMW_ext;
              end
              else
              begin
                nbyte <= 4'b0010;
                out_i2c[0] <= 1'b1;// addresse[0] = 1 read
                visual_res_current <= RMW_normal;
              end
            end
          end
 
        s6:
          begin
            if (i2c_busy)
            begin
              visual_res_current <= s6;
            end
            else if (((ack_ok_in) && !(i2c_busy)) && ((cmd_in == 8'h10) || (
                     cmd_in == 8'h13)))
            begin
              start_i2c <= 1'b0;
              msk_op <= 2'b01;
              out_i2c[0] <= 1'b0;
              visual_res_current <= s13;
            end
            else if (((ack_ok_in) && !(i2c_busy)) && ((cmd_in == 8'h11) || (
                     cmd_in == 8'h14)))
            begin
              start_i2c <= 1'b0;
              msk_op <= 2'b10;
              out_i2c[0] <= 1'b0;
              visual_res_current <= s13;
            end
            else if (((ack_ok_in) && !(i2c_busy)) && ((cmd_in == 8'h12) || (
                     cmd_in == 8'h15)))
            begin
              start_i2c <= 1'b0;
              msk_op <= 2'b11;
              out_i2c[0] <= 1'b0;
              visual_res_current <= s13;
            end
            else if ((!(ack_ok_in)) && !(i2c_busy))
            begin
              if (ack_ok_in)
              begin
                start_i2c <= 1'b0;
                visual_res_current <= S55;
              end
              else if (!(ack_ok_in))
              begin
                start_i2c <= 1'b0;
                SRA[6] <= 1'b1;
                visual_res_current <= rd_wr_err;
              end
              else
              begin
                start_i2c <= 1'b0;
                if (com >= 8'h10 && 8'h12 >= com)
                begin
                  nbyte <= 4'b0010;
                  out_i2c[0] <= 1'b1;// addresse[0] = 1 read
                  visual_res_current <= RMW_normal;
                end
                else if ((com >= 8'h13) && (8'h15 >= com))
                begin
                  nbyte <= 4'b0011;
                  visual_res_current <= RMW_ext;
                end
                else
                begin
                  nbyte <= 4'b0010;
                  out_i2c[0] <= 1'b1;// addresse[0] = 1 read
                  visual_res_current <= RMW_normal;
                end
              end
            end
            else
              visual_res_current <= s6;
          end
 
        start_i2c1:
          begin
            visual_res_current <= att_busy0;
          end
 
        default:
          begin
            out_i2c <= 8'h0;
            start_i2c <= 1'b0;
            lire_i2c <= 1'b0;
            msk_op <= 2'b0;
            SRA <= 8'h0;
            ext_mode <= 1'b0;
            nbyte <= 5'b0;
            
            rep_out <= 1'b1;
            MultiByte <= 1'b0;
            occup <= 1'b0;
            ext_multi_mode <= 1'b0;
            rw_b_ext <= 1'b0;  // write
            visual_res_current <= res;
          end
      endcase
    end
  end
 
  assign com[7:0] = cmd_in[7:0];

endmodule





module wb_adapt_i2c (clk, rst, Start_tr, wb_ack, reponse_n, read_nc, wb_we,
                     wb_addr
                     );
 
  input clk;
  wire clk;
  input rst;
  wire rst;
  output Start_tr;
  reg Start_tr;
  input wb_ack;            //  pour demarrer le cycle type ccu node ctrl
  wire wb_ack;
  input reponse_n;         //  0 pour reponse vers node ctrl
  wire reponse_n;
  output read_nc;
  reg read_nc;
  input wb_we;
  wire wb_we;
  input [4:0] wb_addr;
  wire [4:0] wb_addr;
 
  parameter wb2ccu_nc_iddle = 3'b000,
            ReadNc          = 3'b001,
            Reponse_i2c     = 3'b010,
            S14             = 3'b011,
            out_crc_ok      = 3'b100;
 
 
  reg [2:0] visual_wb2ccu_nc_iddle_current;

  // Synchronous process
  always  @(posedge clk)
  begin : wb_adapt_i2c_wb2ccu_nc_iddle
 
    if (rst === 1'b1)
    begin
      Start_tr <= 1'b1;
      read_nc <= 1'b0;
      visual_wb2ccu_nc_iddle_current <= wb2ccu_nc_iddle;
    end
    else
    begin
 
      case (visual_wb2ccu_nc_iddle_current)
        wb2ccu_nc_iddle:
          begin
            if ((wb_ack == 1'b0) && (reponse_n == 1'b1) && (wb_addr != 5'h0))
            begin
              visual_wb2ccu_nc_iddle_current <= wb2ccu_nc_iddle;
            end
            else if (reponse_n == 1'b0)
            begin
              visual_wb2ccu_nc_iddle_current <= Reponse_i2c;
            end
            else if ((wb_ack == 1'b1) && (reponse_n == 1'b1) && (wb_we == 1'b1)
                     && (wb_addr == 5'h0))
            begin
              Start_tr <= 1'b0;
              visual_wb2ccu_nc_iddle_current <= out_crc_ok;
            end
            else
              visual_wb2ccu_nc_iddle_current <= wb2ccu_nc_iddle;
          end
 
        ReadNc:
          begin
            Start_tr <= 1'b1;
            read_nc <= 1'b0;
            visual_wb2ccu_nc_iddle_current <= wb2ccu_nc_iddle;
          end
 
        Reponse_i2c:
          begin
            read_nc <= 1'b1;  // lecture du resultat
            visual_wb2ccu_nc_iddle_current <= ReadNc;
          end
 
        S14:
          begin
            Start_tr <= 1'b1;
            read_nc <= 1'b0;
            visual_wb2ccu_nc_iddle_current <= wb2ccu_nc_iddle;
          end
 
        out_crc_ok:
          begin
            visual_wb2ccu_nc_iddle_current <= S14;
          end
 
        default:
          begin
            Start_tr <= 1'b1;
            read_nc <= 1'b0;
            visual_wb2ccu_nc_iddle_current <= wb2ccu_nc_iddle;
          end
      endcase
    end
  end

endmodule





module sync_start (reset, clk, sti2c_in, sti2c_out
                   );

  input reset;
  wire reset;
  input clk;
  wire clk;
  input sti2c_in;
  wire sti2c_in;
  output sti2c_out;
	reg sti2c_out;
 
 
  always
    @( negedge (clk) or posedge (reset) )
  begin   :Start
 
    if (reset)
    begin
      sti2c_out <= 1'b0;
    end
    else
    begin
      sti2c_out <= sti2c_in;
    end
  end

endmodule
