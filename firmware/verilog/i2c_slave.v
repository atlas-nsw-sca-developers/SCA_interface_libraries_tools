// Copyright (c) 2015 
// Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
// All rights reserved.
// You may redistribute and modify this project under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// Please see the CERN OHL v.1.2 for applicable conditions

`timescale 1ns / 1ps

module I2C_Slave_7b10b(	
	input         wb_clk_i,   
	input         wb_res_i,        
	input	  [5:0] wb_adr_i,            
	output [31:0] wb_dat_o,
	input  [31:0] wb_dat_i,	
	input         wb_wen_i, 
	input         wb_stb_i, 	  
	input         wb_cyc_i,
	output        wb_int_o,
	output        wb_ack_o,
	inout         SDA,
	input         SCL,
	output  [7:0] STATUS
	);
	
	wire          I2C_SetDataValid;
	wire          SDA_IN, SDA_OUT;
	wire          running, CTRL;
	wire          CTRL_modifEn = I2C_SetDataValid;
	wire          CTRL_int_in  = I2C_SetDataValid;
	wire          int_event    = I2C_SetDataValid;	
	wire  [127:0] DATA_rec, DATA_send;
	wire  [31:0]  CTRL2;
	wire  [6:0]   I2C_ADR_7B  = CTRL2[6:0];
	wire  [9:0]   I2C_ADR_10b = CTRL2[17:8];
	
	I2C_slave_new #(
		.nreg(16)
	)I2C_Slave(
		 .clock(wb_clk_i), 
		 .reset(wb_res_i), 
		 .SCL(SCL), 
		 .SDA(SDA),
		 .rec(DATA_rec[127:0]),
		 .send(DATA_send[127:0]),
		 .led(STATUS),
		 .I2C_10b_ADR(I2C_ADR_10b),
       .I2C_7b_ADR(I2C_ADR_7B)
	 );

	WhishboneGenericInterface #(
		.bus_width(32),               
		.ctrlReg_width(8),                  
		.statReg_width(8),                  
		.dataReg_width(8),                  
		.n_W_reg(16),                         
		.n_R_reg(16),                         
		.adr_width(6)
	)I2C_slave(	
		.wb_clk_i(wb_clk_i),                  
		.wb_res_i(wb_res_i),           
		.wb_adr_i(wb_adr_i),     
		.wb_wen_i(wb_wen_i),           
		.wb_dat_o(wb_dat_o),   
		.wb_stb_i(wb_stb_i),
		.wb_dat_i(wb_dat_i), 
		.wb_int_o(wb_int_o),  
		.wb_ack_o(wb_ack_o),
		.STATUS(STATUS),               
		.CTRL(CTRL), 
		.CTRL2(CTRL2),
		.CTRL_modifEn(CTRL_modifEn),   
		.CTRL_int_in(CTRL_int_in),     
		.DATA_miso(DATA_rec[127:0]),    
		.DATA_mosi(DATA_send[127:0]),        	
		.int_event(int_event)                
	);
	
endmodule
