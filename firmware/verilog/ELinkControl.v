// Copyright (c) 2015 
// Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
// All rights reserved.
// You may redistribute and modify this project under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// Please see the CERN OHL v.1.2 for applicable conditions

`timescale 1ns / 1ps

module ELinkControl(
	input         wb_clk_i,   
	input         wb_res_i,        
	input	  [2:0] wb_adr_i,            
	output [31:0] wb_dat_o,
	input  [31:0] wb_dat_i,	
	input         wb_wen_i, 
	input         wb_stb_i, 	  
	input         wb_cyc_i,
	output        wb_int_o,
	output        wb_ack_o,
	input         HDLC_sd_miso, 
	output        HDLC_sd_mosi, 
	output        HDLC_sd_clk,
	output        LED_tx,
	output        LED_rx  
	);
	
	wire    [7:0] CTRL, STATUS, CTRL_int_in;
	wire   [63:0] DATATX, DATARX;
	wire          tx_ena, tx_dav, rx_cmd_ua, rx_ena, sd_mosi, sd_clk;
	wire          tx_cmd_test, tx_cmd_reset, tx_cmd_sabm; 
	wire   [15:0] tx_dat, rx_dat; 	 	
	wire          int_event, ENABLE, CTRL_modifEn; 
	wire          SCA_new_pkt_ack = 1'b1;
	
	assign HDLC_sd_mosi = (ENABLE) ? sd_mosi : 1'b0;
	assign HDLC_sd_clk  = (ENABLE) ? sd_clk  : 1'b0;

	WhishboneElinkInterface wb_interface(	
		.wb_clk_i(wb_clk_i),                  
		.wb_res_i(wb_res_i),           
		.wb_adr_i(wb_adr_i),     
		.wb_wen_i(wb_wen_i),           
		.wb_dat_o(wb_dat_o),   
		.wb_stb_i(wb_stb_i),
		.wb_dat_i(wb_dat_i), 
		.wb_int_o(wb_int_o),  
		.wb_ack_o(wb_ack_o),
		.ENABLE(ENABLE),		
		.STATUS(STATUS[7:0]),                
		.CTRL(CTRL[7:0]),                   
		.CTRL_modifEn(CTRL_modifEn),         
		.CTRL_int_in(CTRL_int_in[7:0]),      
		.DATARX(DATARX[63:0]),        
		.DATATX(DATATX[63:0]),          		
		.int_event(int_event)             
	);

	 ElinkInterfaceControl # (
		 .EPort_int_adr(8'h01),
		 .Sconnect(4'ha),
		 .Sreset(4'hb),
		 .Stest(4'hc),
		 .Spkt2sca(4'h0)
	)AtlanticInterface (
		 //generic
		 .clk(wb_clk_i), 
		 .reset(~wb_res_i),   
		 //Atlentic interface signals
		 .tx_ena(tx_ena), 
		 .tx_dav(tx_dav), 
		 .tx_cmd_test(tx_cmd_test), 
		 .tx_cmd_reset(tx_cmd_reset), 
		 .tx_cmd_sabm(tx_cmd_sabm), 
		 .rx_cmd_ua(rx_cmd_ua), 
		 .tx_dat(tx_dat), 
		 .rx_dat(rx_dat), 
		 .rx_ena(rx_ena), 
		 //Memory mapped interface
		 .regSCA_ch_in(DATATX[7:0]), 
		 .regSCA_Tr_in(DATATX[15:8]), 
		 .regSCA_cmd_in(DATATX[23:16]), 
		 .regSCA_len_in(DATATX[31:24]), 
		 .regData0_in(DATATX[39:32]), 
		 .regData1_in(DATATX[47:40]), 
		 .regData2_in(DATATX[55:48]), 
		 .regData3_in(DATATX[63:56]), 
		 
		 .regSCA_ch_out(DATARX[7:0]), 
		 .regSCA_Tr_out(DATARX[15:8]), 
		 .regSCA_len_out(DATARX[23:16]), 
		 .regData0_out(DATARX[31:24]), 
		 .regData1_out(DATARX[39:32]), 
		 .regData2_out(DATARX[47:40]), 
		 .regData3_out(DATARX[55:48]), 
		 .regData4_out(DATARX[63:56]), 
		 
		 .ctrReg_Eport_in(CTRL[7:0]), 
		 .ctrReg_Eport_ack(CTRL_int_in[7:0]), 
		 .ctrReg_modifEn(CTRL_modifEn),
		 
		 .SCA_new_pkt_rec(int_event),
		 .SCA_new_pkt_ack(SCA_new_pkt_ack)             
    );
	  
	  M_elink #(1, 1) Elink_Master (
            
		 .disable_aux(1'b1), //in
		
		 .resetb(~wb_res_i), //in
		 .link_clk(wb_clk_i), //in
		 .link_clk_aux(wb_clk_i), //in

		 .tx_dat(tx_dat[15:0]), //in 
		 .rx_dat(rx_dat[15:0]), //out
		 
		 .rx_sd(HDLC_sd_miso), //in
		 .rx_sd_aux(1'b0), //in
		 .tx_sd(sd_mosi), //out
		 .tx_sd_aux(), //out
		 
		 .rx_dav(1'b1), //in
		 .rx_ena(rx_ena),//out  
		 .rx_eop(), //out
		 .tx_dav(tx_dav), //out
		 .rx_sop(), //out
		 .rx_err(), //out
		 
		 .tx_ena(tx_ena), //in
		 .user_clk(sd_clk), //out
		 .tx_adr(8'h00), //in
				  
		 .tx_cmd_reset(tx_cmd_reset), //in
		 .tx_cmd_test(tx_cmd_test), //in
		 .tx_cmd_sabm(tx_cmd_sabm), //in
		 .rx_cmd_ua(rx_cmd_ua), //out
		 
		 .rx_adr(), //out
		 .rx_cmd_reset(), //out
		 .rx_cmd_test(), //out
		 .tx_cset(4'b0000), //in
		 .tx_ns(), //out
		 .rx_nr(), //out
		 .rx_ns(), //out
		 .rx_cmd_srej(), //out
		 .cmd_busy(), //out
		 .active_aux()//out
       );

		LED_notifier #(
			.LEN(10000000)
		)txen_notif(
			.clk_i(wb_clk_i),
			.res_i(wb_res_i),
			.event_i(tx_ena | tx_cmd_reset | tx_cmd_test | tx_cmd_sabm),
			.notif_o(LED_tx)
		);
		LED_notifier #(
			.LEN(10000000)
		)rxen_notif(
			.clk_i(wb_clk_i),
			.res_i(wb_res_i),
			.event_i(rx_ena),
			.notif_o(LED_rx)
		);				
		
endmodule

module WhishboneElinkInterface (
	input              wb_clk_i,       
	input              wb_res_i,           
	input	       [2:0] wb_adr_i,          
	input              wb_wen_i,        
	output reg  [31:0] wb_dat_o,         
	input              wb_stb_i,
	input       [31:0] wb_dat_i,  
	output reg         wb_ack_o,
	
	input        [7:0] STATUS,    
	output reg   [7:0] CTRL,         
	output reg         ENABLE,       
	input              CTRL_modifEn,
	input        [7:0] CTRL_int_in,  
	input              int_event,   
	output reg         wb_int_o, 
	input       [63:0] DATARX,    
	output reg  [63:0] DATATX    
	);
	wire    read  = wb_stb & ~wb_wen_i;
	wire    write = wb_stb & wb_wen_i;
	genvar  i,j;
	reg     [31:0] mem[3:0];
	reg     wb_stb;
	
	always @(posedge wb_clk_i) wb_stb <= wb_stb_i;
			
	always @(posedge wb_clk_i or posedge wb_res_i) begin
		if(wb_res_i) CTRL <= 7'b0;
		else if(write && (wb_adr_i == 0)) CTRL <= wb_dat_i[7:0];
		else if(CTRL_modifEn) CTRL <= CTRL_int_in;
	end
	
	always @(posedge wb_clk_i or posedge wb_res_i) begin
		if(wb_res_i) ENABLE <= 1'b1;
		else if(write && (wb_adr_i==6)) ENABLE <= wb_dat_i[0];
	end
	
	always @(posedge wb_clk_i or posedge wb_res_i) begin
		if(wb_res_i) 
			DATATX <= 64'h0;
		else if(write) begin
			case(wb_adr_i)
				2: DATATX <= {DATATX[63:32], wb_dat_i[31:0]};
				3: DATATX <= {wb_dat_i[31:0],  DATATX[31:0]};
			endcase
		end
	end

	always @* begin
		case(wb_adr_i)
				0: wb_dat_o = {24'h0, CTRL[7:0]};
				1: wb_dat_o = {24'h0, STATUS[7:0]};
				6: wb_dat_o = {31'h0, ENABLE};
				2: wb_dat_o = DATARX[31:0];
				3: wb_dat_o = DATARX[63:32];
				default: wb_dat_o = 32'h0;
		endcase
	end
	
	always @(posedge wb_clk_i or posedge wb_res_i) begin
		if(wb_res_i) wb_int_o <=   1'b0;
		else begin
			if(int_event) wb_int_o <= 1'b1;
			else if(read && wb_adr_i == 1) wb_int_o <= 1'b0;
		end
	end
	
	reg  wb_ack_d;
	always @(posedge wb_clk_i or posedge wb_res_i) begin
		if(wb_res_i) wb_ack_d <= 1'b0;
		else if(wb_stb) wb_ack_d <= 1'b1;
		else wb_ack_d <= 1'b0;
	end
	
	always @(posedge wb_clk_i or posedge wb_res_i) begin
		if(wb_res_i) wb_ack_o <= 1'b0;
		else if(wb_stb & wb_ack_d) wb_ack_o <= 1'b1;
		else wb_ack_o <= 1'b0;
	end
	
endmodule

`timescale 1ns / 1ps
module ElinkInterfaceControl #(
    parameter           EPort_int_adr = 8'h01,
	 parameter           Sconnect      = 4'ha,
    parameter           Sreset        = 4'hb,
    parameter           Stest         = 4'hc,
    parameter           Spkt2sca      = 4'h0
	 )(
    input  wire         clk, 
    input  wire         reset, //active low  
    //Atlantic Interface eport signals
    output reg          tx_ena,
    input  wire         tx_dav,
    output reg          tx_cmd_test,
    output reg          tx_cmd_reset,
    output reg          tx_cmd_sabm,
    input  wire         rx_cmd_ua,
    input  wire         rx_ena,
    output reg   [15:0] tx_dat,
    input  wire  [15:0] rx_dat,
    //memory mapped interface
    input  wire   [7:0] regSCA_ch_in , regSCA_Tr_in , regSCA_cmd_in , regSCA_len_in,  
    input  wire   [7:0] regData0_in  , regData1_in  , regData2_in   , regData3_in  ,
    input  wire   [7:0] regData4_in  , regData5_in  , regData6_in   , regData7_in  ,
    output reg    [7:0] regData0_out , regData1_out , regData2_out  , regData3_out ,
    output reg    [7:0] regData4_out , regData5_out , regData6_out  , regData7_out ,
    output reg    [7:0] regSCA_ch_out, regSCA_Tr_out, regSCA_len_out,
    input  wire   [7:0] ctrReg_Eport_in ,
    output reg    [7:0] ctrReg_Eport_ack,
    output reg          ctrReg_modifEn ,
    output reg          SCA_new_pkt_rec,
    input               SCA_new_pkt_ack
    );
    
    localparam  inactive  = 3'h0, ackint      = 3'h1, send_connect   = 3'h2, send_reset   = 3'h3; 
    localparam  send_test = 3'h4, send_packet = 3'h5, wait_cmd_reply = 3'h6, send_ack     = 3'h7;           
    localparam  listen    = 2'h0, receive     = 2'h1, send2usb       = 2'h2, send_cmd_ack = 2'h3;
          
    reg  [2:0] state_tx, snext_tx;
    reg [95:0] data_tx;
    reg  [3:0] CTRL;
    reg  [3:0] count, cd;
    reg  [3:0] count_rx,count_rxd;
    reg        cntm;
    
    always @(posedge clk or negedge reset) begin
        if(~reset) begin
            state_tx <= inactive;
            count <= 0;
            end
        else begin
            state_tx <= snext_tx;
            count <= cd;
            if(state_tx==inactive) begin
                CTRL[3:0] <= ctrReg_Eport_in[3:0];
                data_tx   <= {regData7_in   , regData6_in   , regData5_in  , regData4_in,
                              regData3_in   , regData2_in   , regData1_in  , regData0_in, 
                              regSCA_len_in , regSCA_cmd_in , regSCA_Tr_in , regSCA_ch_in };
                end
        end
    end
            
    always @ (*) begin
        tx_cmd_test      = 1'b0;
        tx_cmd_reset     = 1'b0;
        tx_cmd_sabm      = 1'b0;
        ctrReg_Eport_ack = 0;
        tx_dat           = 16'h0000;
        tx_ena           = 1'b0;
        ctrReg_modifEn   = 0;
        cd               = count; 
        case(state_tx)
            inactive: if(ctrReg_Eport_in[4]) snext_tx = ackint;
                      else snext_tx = inactive;
            ackint:   begin
                        //if(tx_dav)
                        case(CTRL)
                            Sconnect: snext_tx = send_connect;
                            Sreset:   snext_tx = send_reset;
                            Stest:    snext_tx = send_test;
                            Spkt2sca: begin
                                            snext_tx = send_packet;
                                            cd = 0;
                                         end
                            default:    snext_tx = inactive;
                            endcase
                        //else snext_tx = state_tx;
                        ctrReg_Eport_ack = 8'h01;
                        ctrReg_modifEn = 1;
                        end
            send_reset: begin            
                    snext_tx = wait_cmd_reply;
                    tx_cmd_reset = 1'b1;
                    end
            send_test: begin            
                    snext_tx = wait_cmd_reply;
                    tx_cmd_test  = 1'b1;
                    end
            send_connect: begin            
                    snext_tx = wait_cmd_reply;
                    tx_cmd_sabm  = 1'b1;
                    end
            wait_cmd_reply: snext_tx = inactive; //snext_tx = (rx_cmd_ua) ? inactive : state_tx;
            send_packet: begin
                    if(count > ((regSCA_len_in>>2)+4)) begin
                        cd=0;
                        snext_tx = inactive;
                        end
                    else begin
                        cd=count+4'b1;
                        snext_tx = state_tx;
                        end
                    
                    //tx_dat <= {data_tx[(16*count + 7):(16*count)],data_tx[(16*count + 15):(16*count +8)]};
                    tx_ena = 1'b1;
                    case(count) 
                            0:     tx_dat[15:0] = {data_tx[(7):(0)],data_tx[(15):(8)]};
                            1:     tx_dat[15:0] = {data_tx[(16*1 + 7):(16*1)] , data_tx[(16*1 + 15):(16*1 +8)]};
                            2:     tx_dat[15:0] = {data_tx[(16*2 + 7):(16*2)] , data_tx[(16*2 + 15):(16*2 +8)]};
                            3:     tx_dat[15:0] = {data_tx[(16*3 + 7):(16*3)] , data_tx[(16*3 + 15):(16*3 +8)]};
                        //    4:     tx_dat[15:0] = {data_tx[(16*4 + 7):(16*4)] , data_tx[(16*4 + 15):(16*4 +8)]};
                        //    5:     tx_dat[15:0] = {data_tx[(16*5 + 7):(16*5)] , data_tx[(16*5 + 15):(16*5 +8)]};
                        default: tx_dat[15:0] = 16'h0000;
                    endcase    
                    end
            default: snext_tx = inactive;
        endcase 
    end


    always @(posedge clk or negedge reset) begin
    if(~reset) begin
            count_rx <= 0;
            regSCA_len_out <= 0;
            regSCA_Tr_out  <= 0;
            regSCA_ch_out  <= 0;
            regData0_out   <= 0;
            regData1_out   <= 0;
            regData2_out   <= 0;
            regData3_out   <= 0;
            regData4_out   <= 0;
            regData5_out   <= 0;
            regData6_out   <= 0;
            regData7_out   <= 0;
            end
        else begin
            if(rx_ena) begin
                if(SCA_new_pkt_ack) case(count_rx)
                    0:  {regSCA_ch_out  , regSCA_Tr_out} <= rx_dat;
                    1:  {regSCA_len_out , regData0_out } <= rx_dat;
                    2:  {regData1_out   , regData2_out } <= rx_dat;
                    3:  {regData3_out   , regData4_out } <= rx_dat;
                    4:  {regData5_out   , regData6_out } <= rx_dat;
                    5:   regData7_out                    <= rx_dat[7:0];
                    endcase
                count_rx <= count_rx+4'b1;
            end
            else count_rx <= 0;
        end
    end
    
    always @(posedge clk or negedge reset) begin
        if (~reset) begin 
            cntm <= 1'b1;
            SCA_new_pkt_rec <= 1'b0;
            end
        else begin
            if (rx_ena) cntm <= 1'b0;
            if (~rx_ena && ~cntm) begin    
                    SCA_new_pkt_rec <= 1'b1;
                    cntm <= 1'b1;
                    end
            else SCA_new_pkt_rec <= 1'b0;
        end
    end
    
    
endmodule


