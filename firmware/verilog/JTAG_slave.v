// Copyright (c) 2015 
// Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
// All rights reserved.
// You may redistribute and modify this project under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// Please see the CERN OHL v.1.2 for applicable conditions

`timescale 1ns / 1ps

module JTAG_Slave #( 
   parameter BufferSize = 128,
   parameter BitCount   = 7
   )(
	input         wb_clk_i,   
	input         wb_res_i,        
	input	  [5:0] wb_adr_i,            
	output [31:0] wb_dat_o,
	input  [31:0] wb_dat_i,	
	input         wb_wen_i, 
	input         wb_stb_i, 	  
	output        wb_int_o,
	output        wb_ack_o,
   input         TCK_i,
   output        TDO_o,
   input         TDI_i,
   input         TMS_i,
   input         RST_i
   );
   wire    [2:0] CTRL; 
	wire          ENABLE;
   wire  [127:0] DATA_mosi, DATA_miso, CMD_miso;
   wire          int_event    = 1'b0;
	wire          STATUS = {TCK_i,TDO_o,TDI_i,TMS_i,RST_i};
   	
	JTAG_StramSlave JTAGslave (
		.clk(wb_clk_i), 
		.reset(wb_res_i), 
		.enable_b(ENABLE), 
		.CTRL(CTRL), 
		.TCK_i(TCK_i), 
		.TDO_o(TDO_o), 
		.TDI_i(TDI_i), 
		.TMS_i(TMS_i), 
		.RST_i(RST_i), 
		.TDIRX(DATA_miso), 
		.TMSRX(CMD_miso), 
		.TDOTX(DATA_mosi)
	);
	
	WhishboneGenericInterface #(
		.bus_width(32),               
		.ctrlReg_width(3),                  
		.statReg_width(1),                  
		.dataReg_width(32),                  
		.n_W_reg(4),                         
		.n_R_reg(8),                         
		.adr_width(6)
	)wbinterface(	
		.wb_clk_i(wb_clk_i),                  
		.wb_res_i(wb_res_i),           
		.wb_adr_i(wb_adr_i),     
		.wb_wen_i(wb_wen_i),           
		.wb_dat_o(wb_dat_o),   
		.wb_stb_i(wb_stb_i),
		.wb_dat_i(wb_dat_i), 
		.wb_int_o(wb_int_o),  
		.wb_ack_o(wb_ack_o),
		.ENABLE(ENABLE),
		.STATUS(STATUS),               
		.CTRL(CTRL[2:0]),                   
		.CTRL_modifEn(1'b0),   
		.CTRL_int_in(3'h0),     
		.DATA_miso({DATA_miso[(BufferSize-1):0],
                  CMD_miso[(BufferSize-1):0]}),    
		.DATA_mosi(DATA_mosi[(BufferSize-1):0]),        	
		.int_event(int_event)                
	);
	
endmodule

// Input and output data are kept on purpose separated 
// not using a common shiftregister for testing reasons

module JTAG_StramSlave # (
   parameter blen = 128
   )(  
   input       clk,
   input       reset,	
	input       enable_b,
   input [2:0] CTRL,
   input       TCK_i,
   output      TDO_o,
   input       TDI_i,
   input       TMS_i,
   input       RST_i,
	
   output reg [blen-1:0] TDIRX,
   output reg [blen-1:0] TMSRX,
   input      [blen-1:0] TDOTX
   );   

	reg  TCK, TCK_d, TDO_o;
	reg  [7:0] cnto;
	wire MsbLsb =  CTRL[2];	
	
	always @(posedge clk) TCK   <= TCK_i;
	always @(posedge clk) TCK_d <= TCK;
	
	wire tx = (CTRL[0]) ? (~TCK_d & TCK) : (TCK_d & ~TCK);
	wire rx = (CTRL[1]) ? (TCK_d & ~TCK) : (~TCK_d & TCK);

	always @(posedge clk or posedge reset) begin
		if(reset) TDIRX <= 0;
		else if(~RST_i | enable_b) TDIRX <= 0;
		else if(rx) begin
			if(~MsbLsb) TDIRX <= {TDIRX[blen-2:0], TDI_i};
			else        TDIRX <= {TDI_i, TDIRX[blen-1:1]};
		end
	end
	
	always @(posedge clk or posedge reset) begin
		if(reset) TMSRX <= 0;
		else if(~RST_i | enable_b) TMSRX <= 0;
		else if(rx) begin
			if(~MsbLsb) TMSRX <= {TMSRX[blen-2:0], TMS_i};
			else        TMSRX <= {TMS_i, TMSRX[blen-1:1]};
		end 
	end	
	
	always @(posedge clk or posedge reset) begin
		if(reset) cnto <= 0;
		else if(~RST_i | enable_b) cnto <= 0;
		else if(tx) cnto <= cnto+8'h1;
	end
		
	always @(*) case({MsbLsb, CTRL[0]})
			2'b00: TDO_o = (cnto!=0) ? TDOTX[blen-cnto] : 1'b1;
			2'b10: TDO_o = (cnto!=0) ? TDOTX[cnto-1]: 1'b1;
			2'b01: TDO_o = (cnto!=128) ? TDOTX[blen-cnto-1] : 1'b1;
			2'b11: TDO_o = (cnto!=128) ? TDOTX[cnto]: 1'b1;
			default: TDO_o = 1'b0;
	endcase
	
endmodule

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//   localparam  st_wait = 3'h0;
//   localparam  st_cp0  = 3'h1;
//   localparam  st_cp1  = 3'h2; 
//  	assign      grst    = (~RST_i)|(~enable);
//	
//   always @(posedge clk or negedge reset_n) begin
//      if(~reset_n) begin
//         cnt_pos <= 0;
//         cnt_neg <= 0;
//         st <= st_wait;
//      end
//      else begin
//			if(grst) st <= st_wait;
//         else case(st) 
//            st_wait: begin
//                     st <= st_cp0;
//                     cnt_pos <= (MsbLsb) ? 0 : {BitCount{1'b1}};
//                     cnt_neg <= (MsbLsb) ? 0 : {BitCount{1'b1}};
//                  end
//            st_cp0: begin
//                     if(TCK_i) begin
//                        st <= st_cp1;
//                        cnt_pos <= (MsbLsb) ? cnt_pos+7'h1 : cnt_pos-7'h1;
//                     end
//                  end
//            st_cp1: begin
//                     if(~TCK_i) begin
//                        st <= st_cp0;
//                        cnt_neg <= (MsbLsb) ? cnt_neg+7'h1 : cnt_neg-7'h1;
//                     end
//                  end       
//            default: begin
//                  st <= st_wait;
//                  cnt_pos <= (MsbLsb) ? 0 : {BitCount{1'b1}};
//                  cnt_neg <= (MsbLsb) ? 0 : {BitCount{1'b1}};
//                  end
//         endcase
//      
//      end
//   end
//   
//      always@(posedge clk or negedge reset_n) begin
//         if(~reset_n) begin
//            DataRX <= 0;
//            CmdRX  <= 0;
//            TDO_o  <= 1'b0;
//         end
//         else begin
//            case(JTAGSlaveCTRL)
//            0:   begin
//                  DataRX[cnt_pos] <= TDI_i;
//                  CmdRX[cnt_pos]  <= TMS_i;
//                  TDO_o           <= DataTX[cnt_neg];
//               end
//            1:   begin
//                  DataRX[cnt_neg] <= TDI_i;
//                  CmdRX[cnt_neg]  <= TMS_i;
//                  if(MsbLsb) 
//                     TDO_o <= (cnt_pos==0) ? TDO_o : DataTX[cnt_pos-1];
//                  else
//                     TDO_o <= (cnt_pos=={BitCount{1'b1}}) ? TDO_o : DataTX[cnt_pos+1];
//               end
//            2:   begin
//                  DataRX[cnt_neg] <= TDI_i;
//                  CmdRX[cnt_neg]  <= TMS_i;
//                  TDO_o           <= (cnt_pos==0) ? TDO_o : DataTX[cnt_pos-1];
//               end      
//                  
//            3:   begin
//                  DataRX[cnt_pos] <= TDI_i;
//                  CmdRX[cnt_pos]  <= TMS_i;
//                  TDO_o           <= (cnt_neg==0) ? TDO_o : DataTX[cnt_neg-1];
//               end            
//            endcase
//         end
//   end   
//endmodule   
