// Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
// This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 


`define    Use_Interrupt_sender  //for using interrupt signal
`define    difclkdomain          //depending on the wb topology
`define    Allow_CTRL_int_modif  //for double direction access to control register
`timescale 1ns / 1ps

module WhishboneGenericInterface #(
	parameter integer bus_width     = 32,  //bus width (8,16,32)
	parameter integer ctrlReg_width = 8,   //control register width
	parameter integer statReg_width = 8,   //status register width
	parameter integer dataReg_width = 32,  //data registers width
	parameter integer n_W_reg       = 32,   //number of writable registers
	parameter integer n_R_reg       = 31,   //number of read-only registers
	parameter integer adr_width     = 8
	)(
	input                                 wb_clk_i,       
	input                                 wb_res_i,           
	input	              [(adr_width-1):0] wb_adr_i,          
	input                                 wb_wen_i,        
	output reg          [(bus_width-1):0] wb_dat_o,         
	input                                 wb_stb_i,
	input               [(bus_width-1):0] wb_dat_i,  
	output reg                            wb_ack_o,
	
	input             [statReg_width-1:0] STATUS,       //status_register
	output reg        [ctrlReg_width-1:0] CTRL,         //control register
	output reg                     [31:0] CTRL2,
	output reg                            ENABLE,       //enable flag
	`ifdef Allow_CTRL_int_modif
	input                                 CTRL_modifEn, //enable CTRL modif
	input             [ctrlReg_width-1:0] CTRL_int_in,  //internal CTRL new value
	`endif
	`ifdef Use_Interrupt_sender
	input                                 int_event,    //event edge generate an interrupt
	output reg                            wb_int_o,     //interrupt interface (clean with read)
	`endif
	input   [(n_R_reg*dataReg_width)-1:0] DATA_miso,    //data from slave to master
	output  [(n_W_reg*dataReg_width)-1:0] DATA_mosi     //data from master to slave
	);
	
	wire    read  = wb_stb & ~wb_wen_i;
	wire    write = wb_stb & wb_wen_i;
	genvar  i,j;
	reg     [dataReg_width-1:0] mem[n_W_reg+n_R_reg-1:0];
			
	`ifdef difclkdomain
		reg wb_stb;
		always @(posedge wb_clk_i) wb_stb <= wb_stb_i;
	`else
		wire wb_stb = wb_stb_i;
	`endif
			
	always @(posedge wb_clk_i or posedge wb_res_i) begin
		if(wb_res_i) 
			CTRL <=   {ctrlReg_width{1'b0}};
		else if(write && (wb_adr_i[(adr_width-1):0] == 0))
			CTRL <=   wb_dat_i[(ctrlReg_width-1):0];
		`ifdef Allow_CTRL_int_modif
		else if(CTRL_modifEn) 
			CTRL <=   CTRL_int_in;
		`endif
	end
	
	always @(posedge wb_clk_i or posedge wb_res_i) begin
		if(wb_res_i) 
			ENABLE <= 1'b1;
		else if(write && (wb_adr_i[(adr_width-1):0]==(2+n_W_reg+n_R_reg)))
			ENABLE <= wb_dat_i[0];
	end
	
	always @(posedge wb_clk_i or posedge wb_res_i) begin
		if(wb_res_i) 
			CTRL2 <= 32'h0;
		else if(write && (wb_adr_i[(adr_width-1):0]==(2+n_W_reg+n_R_reg+1)))
			CTRL2 <= wb_dat_i[31:0];
	end
	
	generate 
		for(i=0;i<n_W_reg;i=i+1) begin: Write_MMdataRegisters
			always @(posedge wb_clk_i or posedge wb_res_i) begin
				if(wb_res_i) 
					mem[i][dataReg_width-1:0] <=   {dataReg_width{1'b0}};		
				else if(write && (wb_adr_i[(adr_width-1):0] == (i+2)))
					mem[i][dataReg_width-1:0] <=   wb_dat_i[(dataReg_width-1):0];
			end	
			assign DATA_mosi[(dataReg_width*i) +: (dataReg_width)] = mem[i][dataReg_width-1:0];
		end
		for (j=0;j<n_R_reg;j=j+1) begin: demux
			always @* mem[j+n_W_reg][dataReg_width-1:0] = DATA_miso[(dataReg_width*j) +: (dataReg_width)]; 
		end
	endgenerate
	
	always @* begin
		case(wb_adr_i[(adr_width-1):0])
			0:     wb_dat_o = {{(bus_width-ctrlReg_width){1'b0}}, CTRL[(ctrlReg_width-1):0]};
			1:     wb_dat_o = {{(bus_width-statReg_width){1'b0}}, STATUS[(statReg_width-1):0]};
			2+n_W_reg+n_R_reg: wb_dat_o = {{(bus_width-1){1'b0}}, ENABLE};
			3+n_W_reg+n_R_reg: wb_dat_o = CTRL2;
			default: begin
				if(wb_adr_i<(2+n_W_reg+n_R_reg)) 
					wb_dat_o = {{(bus_width-dataReg_width){1'b0}}, mem[wb_adr_i-2][dataReg_width-1:0]};
				else 
					wb_dat_o = {bus_width{1'b0}};
			end
		endcase
	end
	
	`ifdef Use_Interrupt_sender
	always @(posedge wb_clk_i or posedge wb_res_i) begin
		if(wb_res_i) wb_int_o <=   1'b0;
		else begin
			if(int_event) wb_int_o <=  1'b1;
			else if(read && wb_adr_i == 1) wb_int_o <=  1'b0;
		end
	end
	`endif
	
	reg  wb_ack_d;
	always @(posedge wb_clk_i or posedge wb_res_i) begin
		if(wb_res_i) wb_ack_d <= 1'b0;
		else if(wb_stb) wb_ack_d <= 1'b1;
		else wb_ack_d <= 1'b0;
	end
	
	`ifdef difclkdomain
	always @(posedge wb_clk_i or posedge wb_res_i) begin
		if(wb_res_i) wb_ack_o <= 1'b0;
		else if(wb_stb & wb_ack_d) wb_ack_o <= 1'b1;
		else wb_ack_o <= 1'b0;
	end
	`else
	always @(wb_ack_d) wb_ack_o = wb_ack_d;
	`endif
	
endmodule

`undef Use_Interrupt_sender
`undef Allow_CTRL_int_modif
`undef difclkdomain
