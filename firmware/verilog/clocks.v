// Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
// This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

`timescale 1ns / 1ps

module clocks(
	input  clkin_i,
	input  reset,
	input  [2:0] ctrl,
	output clk_100_o,
	output clk_40_o,
	output clk_80_o
   );
	
	wire clk_40;
	
	DCM_BASE  #(
		.CLKFX_MULTIPLY(2),                  // F_CLKFX   = F_CLKIN * CLKFX_MULTIPLY / CLKFX_DIVIDE
		.CLKFX_DIVIDE(5),                    // F_CLKFX   = F_CLKIN * CLKFX_MULTIPLY / CLKFX_DIVIDE
		.CLKIN_DIVIDE_BY_2("FALSE"),         // TRUE/FALSE to enable CLKIN divide by two feature
      .CLKIN_PERIOD(10.0),                 // Specify period of input clock in ns from 1.25 to 1000.00
      .CLKOUT_PHASE_SHIFT("NONE"),         // Specify phase shift mode of NONE or FIXED
      .CLK_FEEDBACK("NONE"),               // Specify clock feedback of NONE or 1X
      .DCM_PERFORMANCE_MODE("MAX_SPEED"),  // Can be MAX_SPEED or MAX_RANGE
      .DESKEW_ADJUST("SYSTEM_SYNCHRONOUS"),// SOURCE_SYNCHRONOUS, SYSTEM_SYNCHRONOUS or an integer from 0 to 15
      .DFS_FREQUENCY_MODE("LOW"),          // LOW or HIGH frequency mode for frequency synthesis
      .DLL_FREQUENCY_MODE("LOW"),          // LOW, HIGH, or HIGH_SER frequency mode for DLL
      .DUTY_CYCLE_CORRECTION("TRUE"),      // Duty cycle correction, TRUE or FALSE
      .FACTORY_JF(16'hf0f0),               // FACTORY JF value suggested to be set to 16'hf0f0
      .PHASE_SHIFT(0),                     // Amount of fixed phase shift from -255 to 1023
      .STARTUP_WAIT("FALSE")               // Delay configuration DONE until DCM LOCK, TRUE/FALSE
	) pll40A (
		.RST(1'b0),                          
		.CLKIN(clk_100_i),                     
		.CLKFX(clk_40_o),
		//.CLKFX(clk_40),
		.CLK0(clk_100_o)
	);
	
	DCM_BASE  #(
		.CLKFX_MULTIPLY(4),                  
		.CLKFX_DIVIDE(5),                    
		.CLKIN_DIVIDE_BY_2("FALSE"),        
      .CLKIN_PERIOD(10.0),                 
      .CLKOUT_PHASE_SHIFT("NONE"),         
      .CLK_FEEDBACK("NONE"),               
      .DCM_PERFORMANCE_MODE("MAX_SPEED"),  
      .DESKEW_ADJUST("SYSTEM_SYNCHRONOUS"),
      .DFS_FREQUENCY_MODE("LOW"),          
      .DLL_FREQUENCY_MODE("LOW"),          
      .DUTY_CYCLE_CORRECTION("TRUE"),      
      .FACTORY_JF(16'hf0f0),               
      .PHASE_SHIFT(0),                    
      .STARTUP_WAIT("FALSE")               
	) pll40B (
		.RST(1'b0),                          
		.CLKIN(clk_100_i),                     
		.CLKFX(clk_80_o),                 
		.CLK0()
	);
	
	IBUFG inbuffer (
		.I(clkin_i),
		.O(clk_100_i)
	);
	
	
endmodule
