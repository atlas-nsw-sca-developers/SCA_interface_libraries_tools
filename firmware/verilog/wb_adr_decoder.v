// Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
// This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

`timescale 1ns / 1ps

module wb_adr_decoder #(
	parameter integer NSLAVES     = 8,
	parameter integer BASEADRBITS = 8
	)(
	input                    wb_clk_m,
	input             [31:0] wb_adr_m,
	input                    wb_stb_m,
	output            [31:0] wb_dat_m,
	output reg               wb_ack_m,
	output reg               wb_int_m,
	
	input   [32*NSLAVES-1:0] wb_dat_s,
	output     [NSLAVES-1:0] wb_stb_s,
	input      [NSLAVES-1:0] wb_ack_s,
	input      [NSLAVES-1:0] wb_int_s
   );	
	wire   [BASEADRBITS-1:0] select;
	wire                     wb_ack, wb_int;
	
	assign select        = wb_adr_m[31:(32-BASEADRBITS)] ;
	assign wb_stb_s      = (wb_stb_m && (select<NSLAVES)) ? (1'b1 << select) : {NSLAVES{1'b0}};
	assign wb_dat_m      = (select<NSLAVES) ? wb_dat_s[(select*32) +: 32] : 32'h0;
	assign wb_ack        = (|wb_ack_s);
	assign wb_int        = (|wb_int_s);
	
	always @(posedge wb_clk_m) wb_ack_m <= wb_ack;
	always @(posedge wb_clk_m) wb_int_m <= wb_int;

endmodule
