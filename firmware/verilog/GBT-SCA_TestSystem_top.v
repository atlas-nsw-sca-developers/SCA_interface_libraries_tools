// Copyright (c) 2015 
// Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
// All rights reserved.
// You may redistribute and modify this project under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// Please see the CERN OHL v.1.2 for applicable conditions


//Submodules address table:
`define nsubmodules         24
`define adr_pcb_ctrl        0
`define adr_sca_serial_port 1
`define adr_sca_eport_pri   2
`define adr_sca_eport_aux   3
`define adr_sca_slave_GPIO  4
`define adr_sca_slave_SPI   5
`define adr_sca_slave_JTAG  6
`define adr_sca_control     7
`define adr_sca_slave_i2c0  8 
`define adr_sca_slave_i2c1  9 
`define adr_sca_slave_i2c2  10 
`define adr_sca_slave_i2c3  11 
`define adr_sca_slave_i2c4  12 
`define adr_sca_slave_i2c5  13 
`define adr_sca_slave_i2c6  14 
`define adr_sca_slave_i2c7  15 
`define adr_sca_slave_i2c8  16 
`define adr_sca_slave_i2c9  17 
`define adr_sca_slave_i2c10 18  
`define adr_sca_slave_i2c11 19  
`define adr_sca_slave_i2c12 20  
`define adr_sca_slave_i2c13 21 
`define adr_sca_slave_i2c14 22  
`define adr_sca_slave_i2c15 23 

`timescale 1ns / 1ps
module TopCom_SCAonFPGA(  
	//USB signals
	input           clk_i,
	input           clk_usb,
	input           reset,   
	inout    [7:0]  pdb,
	input           astb,
	input           dstb,       
	input           pwr,       
	output          pwait,
	//Interface board control
	input    [7:0]  SWITCH, 
	output   [7:0]  LEDs,
	inout    [7:0]  extJA,
	inout    [7:0]  extJB,
	output          i2c_scl,
	inout           i2c_sda,
	output          supply_en,
	//GBT-SCA signals
	output          sca_elink_ck_pri_p, sca_elink_ck_pri_n,
	output          sca_elink_tx_pri_p, sca_elink_tx_pri_n,
	input           sca_elink_rx_pri_p, sca_elink_rx_pri_n,
	output          sca_elink_ck_Aux_p, sca_elink_ck_Aux_n,
	output          sca_elink_tx_Aux_p, sca_elink_tx_Aux_n,
	input           sca_elink_rx_Aux_p, sca_elink_rx_Aux_n,
	output          sca_axudisable,
	output          sca_fuseprog,
	output          sca_testenable,
	output          sca_reset_b,
	output          sca_auxport_scl,
	inout           sca_auxport_sda,
	input    [15:0] sca_i2c_scl,
	inout    [15:0] sca_i2c_sda,
	inout     [7:0] sca_gpio,
	input     [7:0] sca_spi_ss,
	output          sca_spi_miso,
	input           sca_spi_mosi,
	input           sca_spi_clk,
	output          sca_jtag_tdi,
	input           sca_jtag_tck,
	input           sca_jtag_tdo,
	input           sca_jtac_tms,
	input           sca_jtag_res
	);		
	
	localparam nwbs = `nsubmodules;
	wire            clk_80, clk_40, clk_100;
	wire      [7:0] CTRL0, CTRL1, CTRL2, CTRL3; 
	wire      [7:0] status_reg, dac_out1, led_elink_Aux_dat;
	wire            led_elink_Aux_stb, led_elink_Aux_we;
	wire            wb_ack_i2c, wb_ack_regs, flag_40, wb_res;
	wire            led_elink_pri_tx, led_elink_pri_rx;
	wire            scl_master_out, scl_in, sda_master_out, sda_master_in;
	wire            sca_elink_ck_pri, sca_elink_tx_pri, sca_elink_rx_pri;
	wire            sca_elink_ck_Aux, sca_elink_tx_Aux, sca_elink_rx_Aux;
	wire     [31:0] wbmdato, wbmdati, wbmadr;	
	wire     [31:0] wb_dat_mosi, wb_adr, wb_dat_miso;     
	wire            wb_stb,   wb_ack,   wb_int,  wb_wen, wb_clk;
	wire [nwbs-1:0] wb_stb_s, wb_ack_s, wb_int_s;
	wire     [31:0] wb_dat_s[0:nwbs-1];
	wire     [31:0] GPIO;
	wire            sca_elink_rx_pri_d, sca_elink_tx_pri_d;
	wire            sca_elink_rx_Aux_d, sca_elink_tx_Aux_d;
	
	assign     supply_en      =  CTRL0[0]; 	
	assign     sca_axudisable =  CTRL1[0]; 
	assign     sca_testenable =  CTRL1[1]; 
	assign     sca_reset_b    =  CTRL1[2]; 
	wire       elinkpriphctrl =  CTRL1[3];
	wire       elinkauxphctrl =  CTRL1[4];	
	wire [2:0] clksel         =  CTRL2[2:0];
	wire       I2C_external   =  CTRL2[6];
	wire       SPI_external   =  CTRL2[7]; 	
	assign     sca_fuseprog   =  CTRL3[0]; 

	//Clock PLLs	
	clocks clkgen(
		.clkin_i(clk_i),
		.reset(reset),
		.ctrl(clksel),
		.clk_100_o(clk_100),
		.clk_40_o(clk_40),
		.clk_80_o(clk_80)
   );

	//USB interface
	USB_WishboneMaster USB_DEPP(
			.sys_clk(clk_100),
			.reset(reset),
			.usb_clk(clk_usb), 
			.pdb(pdb[7:0]), 
			.astb(astb), 
			.dstb(dstb), 
			.pwr(pwr), 
			.pwait(pwait),
			.CTRL0(CTRL0),
			.CTRL1(CTRL1),
			.CTRL2(CTRL2),
			.CTRL3(CTRL3),
			.wb_clk_o(wb_clk),
			.wb_adr_o(wb_adr),
			.wb_dat_i(wb_dat_miso),
			.wb_dat_o(wb_dat_mosi),
			.wb_wen_o(wb_wen),
			.wb_stb_o(wb_stb),
			.wb_ack_i(wb_ack),
			.wb_int_i(wb_int_s),
			.wb_res_o(wb_res)
	);
	
	genvar modules;
	wire [(nwbs*32)-1:0] wb_dat_slaves; 
	generate 
		for(modules=0; modules<nwbs; modules=modules+1) begin:wbsassing 
			assign wb_dat_slaves[(modules*32) +: 32] = wb_dat_s[modules];
		end
	endgenerate
	
	//Wishbone address decoder
	wb_adr_decoder #(
		.NSLAVES(nwbs)
	)wb_decoder(
		.wb_clk_m(wb_clk),
		.wb_adr_m(wb_adr),
		.wb_stb_m(wb_stb),
		.wb_ack_m(wb_ack),
		.wb_dat_m(wb_dat_miso),
		.wb_int_m(),
		.wb_dat_s(wb_dat_slaves),
		.wb_stb_s(wb_stb_s),
		.wb_ack_s(wb_ack_s),
		.wb_int_s(wb_int_s)
   );
	
	//GBT-SCA control signals
	control_signals SCA_control_signals(
		.wb_clk_i(wb_clk),
		.wb_adr_i(wb_adr[4:0]), 
		.wb_res_i(reset),
		.wb_dat_i(wb_dat_mosi),                           
		.wb_wen_i(wb_wen),         
		.wb_stb_i(wb_stb_s[`adr_sca_control]),         
		.wb_dat_o(wb_dat_s[`adr_sca_control]),
		.wb_ack_o(wb_ack_s[`adr_sca_control]),
		.supply_en(),
		.sca_axudisable(),
		.sca_fuseprog(),
		.sca_testenable(),
		.sca_reset_b()
	 );

	//Board control (I2C Master)
	i2c_master_top PCB_control( 
		.scl(scl_master_out),
		.sda_out(sda_master_out),
		.sda_in(sda_master_in),
		.wb_int_o(wb_int_s[`adr_pcb_ctrl]),
		.wb_ack_o(wb_ack_s[`adr_pcb_ctrl]),
		.wb_err_o(),
		.wb_dat_o(wb_dat_s[`adr_pcb_ctrl]), 
		.wb_dat_i(wb_dat_mosi), 
		.wb_adr_i(wb_adr[4:0]),
		.wb_rst_i(reset),
		.wb_sel_i(4'b1111), 
		.wb_we_i(wb_wen),
		.wb_stb_i(wb_stb_s[`adr_pcb_ctrl]),
		.wb_cyc_i(1'b1),
		.wb_clk_i(wb_clk)
	);
	
	assign i2c_scl = scl_master_out;
	assign sda_master_in  = i2c_sda;	
	PULLUP pullup1(i2c_sda);
	assign i2c_sda = (~sda_master_out) ? 1'b0 : 1'bz;
	//assign extJB[3] = scl_master_out;	
	//assign i2c_sda = (~extJB[4])       ? 1'b0 : 1'bz;
	//assign sda_master_in = (~i2c_sda | ~extJB[4]) ? 1'b0 : 1'b1;
	//PULLUP pullup2(extJB[4]);
	
	//GBT-SCA Aux Serial Port
//	i2c_master_top AuxPort( 
//		.scl(scl_auxport_out),
//		.sda_out(sda_auxport_out),
//		.sda_in(sda_auxport_in),
//		.wb_int_o(wb_int_s[`adr_sca_serial_port]),
//		.wb_ack_o(wb_ack_s[`adr_sca_serial_port]),
//		.wb_err_o(),
//		.wb_dat_o(wb_dat_s[`adr_sca_serial_port]), 
//		.wb_dat_i(wb_dat_mosi), 
//		.wb_adr_i(wb_adr[4:0]),
//		.wb_rst_i(reset),
//		.wb_sel_i(4'b1111), 
//		.wb_we_i(wb_wen),
//		.wb_stb_i(wb_stb_s[`adr_sca_serial_port]),
//		.wb_cyc_i(1'b1),
//		.wb_clk_i(wb_clk)
//	);
//
//	assign sca_auxport_scl = scl_auxport_out;
//	assign sda_auxport_in  = sca_auxport_sda;	
//	PULLUP pullup2(sca_auxport_sda);
//	assign sca_auxport_sda = (~sda_auxport_out) ? 1'b0 : 1'bz;
//	
	assign sca_auxport_sda = 1'bz;
	assign sca_auxport_scl = 1'bz;
//	
//

	//Primary E-Port Master
	ELinkControl ELink_pri(
		.wb_clk_i(clk_40),   
		.wb_res_i(reset),        
		.wb_adr_i(wb_adr[2:0]),            
		.wb_dat_o(wb_dat_s[`adr_sca_eport_pri]),
		.wb_dat_i(wb_dat_mosi),	
		.wb_wen_i(wb_wen), 
		.wb_stb_i(wb_stb_s[`adr_sca_eport_pri]), 	  
		.wb_cyc_i(1'b1),
		.wb_int_o(wb_int_s[`adr_sca_eport_pri]),
		.wb_ack_o(wb_ack_s[`adr_sca_eport_pri]),
		.HDLC_sd_miso(sca_elink_rx_pri), 
		.HDLC_sd_mosi(sca_elink_tx_pri_d), 
		.HDLC_sd_clk(sca_elink_ck_pri),
		.LED_tx(led_elink_pri_tx),
		.LED_rx(led_elink_pri_rx)
	);
	
	ELinkPhaseSelect PhaseSelect(
		.clk_80(clk_80),
		.phctrl_pri(elinkpriphctrl),
		.tx_pri_i(sca_elink_tx_pri_d),
		.tx_pri_o(sca_elink_tx_pri),
		.rx_pri_i(sca_elink_rx_pri_d),
		.rx_pri_o(sca_elink_rx_pri),
		.phctrl_aux(elinkauxphctrl),
		.tx_aux_i(sca_elink_tx_Aux_d),
		.tx_aux_o(sca_elink_tx_Aux),
		.rx_aux_i(sca_elink_rx_Aux_d),
		.rx_aux_o(sca_elink_rx_Aux)
   );
		
	//Auxiliary E-Port Master
	ELinkControl ELink_Aux(
		.wb_clk_i(clk_40),   
		.wb_res_i(reset),        
		.wb_adr_i(wb_adr[2:0]),            
		.wb_dat_o(wb_dat_s[`adr_sca_eport_aux]),
		.wb_dat_i(wb_dat_mosi),	
		.wb_wen_i(wb_wen), 
		.wb_stb_i(wb_stb_s[`adr_sca_eport_aux]), 	  
		.wb_cyc_i(1'b1),
		.wb_int_o(wb_int_s[`adr_sca_eport_aux]),
		.wb_ack_o(wb_ack_s[`adr_sca_eport_aux]),
		.HDLC_sd_miso(sca_elink_rx_Aux), 
		.HDLC_sd_mosi(sca_elink_tx_Aux_d), 
		.HDLC_sd_clk(sca_elink_ck_Aux),
		.LED_tx(led_elink_Aux_tx),
		.LED_rx(led_elink_Aux_rx)
	);

	//I2C slaves
	generate 
		for(genvar nI2Cslave=`adr_sca_slave_i2c0; nI2Cslave<=`adr_sca_slave_i2c14; nI2Cslave=nI2Cslave+1) begin:I2C_slave_gen
		I2C_Slave_7b10b sca_slave_I2Cs(
			.wb_clk_i(wb_clk),   
			.wb_res_i(reset),        
			.wb_adr_i(wb_adr[5:0]),            
			.wb_dat_o(wb_dat_s[nI2Cslave][31:0]),
			.wb_dat_i(wb_dat_mosi),	
			.wb_wen_i(wb_wen), 
			.wb_stb_i(wb_stb_s[nI2Cslave]), 	  
			.wb_cyc_i(1'b1),
			.wb_int_o(wb_int_s[nI2Cslave]),
			.wb_ack_o(wb_ack_s[nI2Cslave]),
			.SDA(sca_i2c_sda[(nI2Cslave-`adr_sca_slave_i2c0)]),		
			.SCL(sca_i2c_scl[(nI2Cslave-`adr_sca_slave_i2c0)])
		);
		end
	endgenerate

	I2C_Slave_7b10b sca_slave_I2C15 (	
		.wb_clk_i(wb_clk),   
		.wb_res_i(reset),        
		.wb_adr_i(wb_adr[5:0]),            
		.wb_dat_o(wb_dat_s[`adr_sca_slave_i2c15][31:0]),
		.wb_dat_i(wb_dat_mosi),	
		.wb_wen_i(wb_wen), 
		.wb_stb_i(wb_stb_s[`adr_sca_slave_i2c15]), 	  
		.wb_cyc_i(1'b1),
		.wb_int_o(wb_int_s[`adr_sca_slave_i2c15]),
		.wb_ack_o(wb_ack_s[`adr_sca_slave_i2c15]),
		.SDA(sca_i2c_sda[15]),		
		.SCL(sca_i2c_scl[15])
	);	
	
	//GPIO slave
	GPIO_slave sca_slave_GPIO(
		.wb_clk_i(wb_clk),   
		.wb_res_i(reset),        
		.wb_adr_i(wb_adr[1:0]),            
		.wb_dat_o(wb_dat_s[`adr_sca_slave_GPIO]),
		.wb_dat_i(wb_dat_mosi),	
		.wb_wen_i(wb_wen), 
		.wb_stb_i(wb_stb_s[`adr_sca_slave_GPIO]), 	  
		.wb_ack_o(wb_ack_s[`adr_sca_slave_GPIO]),
		.GPIO(sca_gpio[7:0])
	);

	//JTAG slave
	JTAG_Slave sca_slave_JTAG(
		.wb_clk_i(wb_clk),   
		.wb_res_i(reset),        
		.wb_adr_i(wb_adr[5:0]),            
		.wb_dat_o(wb_dat_s[`adr_sca_slave_JTAG]),
		.wb_dat_i(wb_dat_mosi),	
		.wb_wen_i(wb_wen), 
		.wb_stb_i(wb_stb_s[`adr_sca_slave_JTAG]), 	  
		.wb_ack_o(wb_ack_s[`adr_sca_slave_JTAG]),
		.wb_int_o(wb_int_s[`adr_sca_slave_JTAG]),
		.TCK_i(sca_jtag_tck),
		.TDO_o(sca_jtag_tdi),
		.TDI_i(sca_jtag_tdo),
		.TMS_i(sca_jtac_tms),
		.RST_i(sca_jtag_res)
	);	
	
	SPI_Slave sca_slave_SPI(
		.wb_clk_i(wb_clk),   
		.wb_res_i(reset),        
		.wb_adr_i(wb_adr[5:0]),            
		.wb_dat_o(wb_dat_s[`adr_sca_slave_SPI]),
		.wb_dat_i(wb_dat_mosi),	
		.wb_wen_i(wb_wen), 
		.wb_stb_i(wb_stb_s[`adr_sca_slave_SPI]), 	  
		.wb_ack_o(wb_ack_s[`adr_sca_slave_SPI]),
		.wb_int_o(wb_int_s[`adr_sca_slave_SPI]),
		.SCLK_i(sca_spi_clk),
		.MISO_o(sca_spi_miso),
		.MOSI_i(sca_spi_mosi),
		.SSEL_i(sca_spi_ss[3])
	);	
		
	//On-board LEDs (debug)
	mux ledmux(
		 .ctrl(SWITCH[3:0]),
		 .out(LEDs[7:0]),
		 .in0({led_elink_Aux_tx, led_elink_Aux_rx, 4'b0 ,led_elink_pri_tx, led_elink_pri_rx}), 
		 .in1({sca_reset_b, sca_testenable, sca_axudisable, supply_en}), 
		 .in2(sca_gpio[7:0]), 
		 .in3({sca_jtag_tck, sca_jtag_tdi, sca_jtag_tdo, sca_jtac_tms, sca_jtag_res}), 
		 .in4({sca_spi_miso, sca_spi_mosi, sca_spi_clk}), 
		 .in5(sca_spi_ss[7:0]), 
		 .in6(), 
		 .in7(),
		 .in8(),
		 .in9(),
		 .inA(),
		 .inB(),
		 .inC(sca_i2c_scl[7:0]),
		 .inD(sca_i2c_sda[7:0]),
		 .inE(sca_i2c_sda[15:8]),
		 .inF(sca_i2c_scl[15:8])
		 );

//	GBT_SCA_demo SCA_demo(
//	
//		.link_clk(sca_elink_ck_Aux),	
//		.tx_sd(sca_elink_rx_Aux),
//		.rx_sd(sca_elink_tx_Aux),			
//
//		.disable_aux(1'b1),
//		.dac_out_1(dac_out1), 
//		.dac_out_2(), 
//		.dac_out_3(), 
//		.dac_out_4(),
//		
//		.gpio_ext_pad()
//	);

	assign extJA[2] = sca_spi_clk;
	assign extJA[1] = sca_spi_mosi;	
	//assign sca_spi_miso = extJA[0];
	assign extJA[3] = sca_spi_ss[0];
	
	OBUFDS #(
		.IOSTANDARD("DEFAULT")
	)elink_ck_pri_buf(
		.O(sca_elink_ck_pri_p),
		.OB(sca_elink_ck_pri_n),
		.I(sca_elink_ck_pri)
	);
	
	OBUFDS #(
		.IOSTANDARD("DEFAULT")
	)elink_tx_pri_buf(
		.O(sca_elink_tx_pri_p),
		.OB(sca_elink_tx_pri_n),
		.I(sca_elink_tx_pri)
	);	
	
	IBUFDS #(
		.IOSTANDARD("DEFAULT"),
		.DIFF_TERM("TRUE")
	)elink_rx_pri_buf(
		.I(sca_elink_rx_pri_p),
		.IB(sca_elink_rx_pri_n),
		.O(sca_elink_rx_pri_d)
	);		
	
	OBUFDS #(
		.IOSTANDARD("DEFAULT")
	)elink_ck_Aux_buf(
		.O(sca_elink_ck_Aux_p),
		.OB(sca_elink_ck_Aux_n),
		.I(sca_elink_ck_Aux)
	);
	OBUFDS #(
		.IOSTANDARD("DEFAULT")
	)elink_tx_Aux_buf(
		.O(sca_elink_tx_Aux_p),
		.OB(sca_elink_tx_Aux_n),
		.I(sca_elink_tx_Aux)
	);	
	IBUFDS #(
		.IOSTANDARD("DEFAULT"),
		.DIFF_TERM("TRUE")
	)elink_rx_Aux_buf(
		.I(sca_elink_rx_Aux_p),
		.IB(sca_elink_rx_Aux_n),
		.O(sca_elink_rx_Aux_d)
	);		
	
endmodule
