// Copyright (c) 2015 
// Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
// All rights reserved.
// You may redistribute and modify this project under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// Please see the CERN OHL v.1.2 for applicable conditions

`timescale 1ns / 1ps

`define reg_wbcmd		8'h00
`define reg_wbintv	8'h01
`define reg_status   8'h02
`define reg_ctrl_0   8'h03
`define reg_ctrl_1   8'h04
`define reg_ctrl_2   8'h05
`define reg_ctrl_3   8'h06
`define reg_reset    8'h07
`define reg_wbadr0	8'h08
`define reg_wbadr1	8'h09
`define reg_wbadr2	8'h0a
`define reg_wbadr3	8'h0b
`define reg_wbdat0	8'h0c
`define reg_wbdat1	8'h0d
`define reg_wbdat2	8'h0e
`define reg_wbdat3	8'h0f



module USB_WishboneMaster (
		input              usb_clk,   //usb clock
		input              sys_clk,
      input              reset,     //system reset
      inout        [7:0] pdb,       //USB_EPP DB0 to DB7
      input              astb,      //USB_EPP ASTB  (Address Strobe)           
      input              dstb,      //USB_EPP DSTB  (Data Strobe)             
      input              pwr,       //USB_EPP WRITE (Transfer direction control)     
      output             pwait,     //USB_EPP WAIT  (Handshake signal) 
		output             wb_clk_o,
		output reg  [31:0] wb_adr_o,
		input       [31:0] wb_dat_i,
		output reg  [31:0] wb_dat_o,       
		output reg         wb_wen_o,
		output             wb_stb_o,
		input              wb_ack_i,
		input        [7:0] wb_int_i,
		output             wb_res_o,
		output reg   [7:0] CTRL0,
		output reg   [7:0] CTRL1,
		output reg   [7:0] CTRL2,
		output reg   [7:0] CTRL3     
		);                                  
      reg          [7:0] EPPstate, busEppData, regEppAdr, ctrl, wb_int;
		reg         [31:0] adr_o, dat_o, dat_i;
		reg                wreq, rreq, sack, wbrw, CMD, wreqd, rreqd, sackd;
		reg          [2:0] wbstate;
		reg          [1:0] cntreset;
		wire         [7:0] busEppIn   = pdb;
		wire               ctlEppDir  = EPPstate[1];
      wire               ctlEppAwr  = EPPstate[2];
      wire               ctlEppDwr  = EPPstate[3];
		wire         [7:0] status_reg = {EPPstate[7:4], 1'b0, wbstate[2:0]};
		
      assign  pwait      = EPPstate[0];
      assign  pdb        = ((pwr && ctlEppDir)||(pwr && ~astb)||(pwr && ~dstb)) ? busEppData : {8{1'bz}};         
		assign  wb_clk_o   = sys_clk;
		
      always @(posedge usb_clk or posedge reset) begin
			if(reset) 
				regEppAdr <= 8'h00;
			else if (ctlEppAwr)
				regEppAdr[7:0] <= busEppIn[7:0];
			else if ((EPPstate==stReadData2 || EPPstate==stWriteData2) & dstb)
				regEppAdr <= regEppAdr+1;
      end
		
		always@(*) begin
			case(regEppAdr) 
					`reg_wbcmd:  busEppData = {7'h0, CMD};
					`reg_wbintv: busEppData = wb_int;
					`reg_wbadr0: busEppData = adr_o[ 7: 0];
					`reg_wbadr1: busEppData = adr_o[15: 8];
					`reg_wbadr2: busEppData = adr_o[23:16];
					`reg_wbadr3: busEppData = adr_o[31:24];
					`reg_wbdat0: busEppData = dat_i[ 7: 0];
					`reg_wbdat1: busEppData = dat_i[15: 8];
					`reg_wbdat2: busEppData = dat_i[23:16];
					`reg_wbdat3: busEppData = dat_i[31:24];
					`reg_ctrl_0: busEppData = CTRL0;
					`reg_ctrl_1: busEppData = CTRL1;
					`reg_ctrl_2: busEppData = CTRL2;
					`reg_ctrl_3: busEppData = CTRL3;
					`reg_status: busEppData = status_reg;
					default:   busEppData = 8'h0;
			endcase	
      end
		
      localparam [7:0] 
			stReady      = {4'h0 , 4'b0000},
			stWriteAdr1  = {4'h1 , 4'b0100},
			stWriteAdr2  = {4'h2 , 4'b0001}, 
			stReadAdr1   = {4'h5 , 4'b0010},
			stReadAdr2   = {4'h6 , 4'b0011},   
			stWriteData1 = {4'h3 , 4'b1000},
			stWriteData2 = {4'h4 , 4'b0001},     
			stReadData1  = {4'h7 , 4'b0010},
			stReadData2  = {4'h8 , 4'b0011};
      

      always @ (posedge usb_clk or posedge reset) begin: DEPP_StateMachine
            if(reset) EPPstate <= stReady;
            else case(EPPstate) 
                        stReady:      case({astb,dstb})
                                           2'b01:   EPPstate <= (~pwr) ? stWriteAdr1  : stReadAdr1;   
                                           2'b10:   EPPstate <= (~pwr) ? stWriteData1 : stReadData1;  
                                           default: EPPstate <= EPPstate;
                                      endcase   
                        stWriteAdr1:  EPPstate <= stWriteAdr2;
                        stWriteAdr2:  if(astb) EPPstate <= stReady;
                        stWriteData1: EPPstate <= stWriteData2;
                        stWriteData2: if(dstb) EPPstate <= stReady;
                        stReadAdr1:   EPPstate <= stReadAdr2;
                        stReadAdr2:   if(astb) EPPstate <= stReady;                        
                        stReadData1:  EPPstate <= stReadData2;
                        stReadData2:  if(dstb) EPPstate <= stReady;
                        default:      EPPstate <= stReady;
                 endcase
      end
		
		always @ (posedge usb_clk or posedge reset) begin
			if(reset) begin
				adr_o <= 32'h0;
				dat_o <= 32'h0;
			end
			else if (ctlEppDwr)
				case(regEppAdr) 
					`reg_wbadr0:	adr_o[ 7: 0] <= busEppIn;
					`reg_wbadr1:	adr_o[15: 8] <= busEppIn;
					`reg_wbadr2:	adr_o[23:16] <= busEppIn;
					`reg_wbadr3:	adr_o[31:24] <= busEppIn;
					`reg_wbdat0:	dat_o[ 7: 0] <= busEppIn;
					`reg_wbdat1:	dat_o[15: 8] <= busEppIn;
					`reg_wbdat2:	dat_o[23:16] <= busEppIn;
					`reg_wbdat3:	dat_o[31:24] <= busEppIn;
					`reg_ctrl_0:   CTRL0[7:0]   <= busEppIn;
					`reg_ctrl_1:   CTRL1[7:0]   <= busEppIn;
					`reg_ctrl_2:   CTRL2[7:0]   <= busEppIn;
					`reg_ctrl_3:   CTRL3[7:0]   <= busEppIn;
				endcase	
		end  
		
		always @ (posedge usb_clk or posedge reset) begin
			if(reset) cntreset <= 2'b0;
			else if(ctlEppDwr && (regEppAdr==`reg_reset)) cntreset <= 2'b11;
			else if (cntreset>0) cntreset <= cntreset-2'b1;
		end
		
		assign wb_res_o = (cntreset==0 || reset) ? 1'b1 : 1'b0;
				
		always @ (posedge usb_clk or posedge reset) begin
			if(reset) begin
				wreq <= 1'b0;
				rreq <= 1'b0;
				CMD  <= 1'b1;
			end
			else begin
				if(ctlEppDwr && (regEppAdr==`reg_wbcmd))
					case (busEppIn)
						8'h1: begin
									wreq <= 1'b1;
									rreq <= 1'b0;
									CMD  <= 1'b0;
						      end
						8'h2: begin
									wreq <= 1'b0;
									rreq <= 1'b1;
									CMD  <= 1'b0;
						      end
						default: begin
									wreq <= 1'b0;
									rreq <= 1'b0;
						      end
					endcase
				else if( sackd ) begin
					wreq <= 1'b0;
					rreq <= 1'b0;
					CMD  <= 1'b1;
				end
			end
		end
		
		always @(posedge wb_clk_o) wreqd <= wreq;
		always @(posedge wb_clk_o) rreqd <= rreq;
		always @(posedge usb_clk)  sackd <= sack;
		
		localparam wbidle=3'h0, wbsync=3'h1, whsync2=3'h3, wbwrite=3'h2, wbread=3'h4;
		
		always @ (posedge wb_clk_o or posedge reset) begin: Wishbone_control
			if(reset) begin
				wbstate  <= wbidle;
				wb_adr_o <= 32'h0;
				wb_dat_o <= 32'h0;
			end
			else case(wbstate)
				wbidle:  begin
								if(wreqd) begin
									wbstate <= wbsync;
									wbrw    <= 1'b1;
									sack    <= 1'b1;
								end
								else if(rreqd) begin
									wbstate <= wbsync;
									wbrw    <= 1'b0;
									sack    <= 1'b1;
								end
								else begin
									sack    <= 1'b0;
									wbstate <= wbidle;
								end
							end
				wbsync:  begin
								wb_adr_o <= adr_o;
								wb_dat_o <= dat_o;
								wbstate  <= whsync2;
								wb_wen_o <= (wbrw) ? 1'b1 : 1'b0;
							end
				whsync2: wbstate <= (wbrw)     ? wbwrite : wbread;
				wbwrite: wbstate <= (wb_ack_i) ? wbidle  : wbwrite;
				wbread:  if (wb_ack_i) begin
								wbstate  <= wbidle;
								dat_i    <= wb_dat_i;
							end		
				default: wbstate <= wbidle;
			endcase
		end
				
		assign wb_stb_o =  (wbstate==wbwrite) | (wbstate==wbread);
		
		
		always @ (posedge usb_clk or posedge reset) begin
			if(reset) wb_int <= 7'h0;
			else      wb_int <= wb_int_i;
		end
		
		
		
		
endmodule
      
      
      
      
      
      
