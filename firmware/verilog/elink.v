// Sandro Bonacini 
// modified by Alessandro Caratelli <alessandro.caratelli@cern.ch>.
// This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 


module M_elink #(
	parameter MASTER = 0,
	 HEADER_FIELD = 1,
	 ADDR_WIDTH = 5,
	 MAX_PACKET_LENGTH = 16, 
	 THRESHOLD = 2**ADDR_WIDTH -MAX_PACKET_LENGTH-1
) (
	// THRESHOLD = 1<<(ADDR_WIDTH-1), // for unknown packet length

	input [15:0] tx_dat,
	output [15:0] rx_dat,
	input rx_sd, rx_sd_aux, link_clk, link_clk_aux, rx_dav, 
	output tx_sd, tx_sd_aux, rx_ena, rx_eop, tx_dav, rx_sop, rx_err,
	input resetb, tx_ena,
	output user_clk,
	input [7:0] tx_adr, 
	output [7:0] rx_adr,
	input tx_cmd_reset, tx_cmd_test, tx_cmd_sabm, disable_aux,
	output rx_cmd_reset, rx_cmd_test, rx_cmd_ua,
	input [3:0] tx_cset,
	output [2:0] tx_ns, rx_nr,rx_ns,
	output [6:0] rx_cmd_srej,
	output cmd_busy,
	output active_aux
);

	wire [1:0] rx_sdr_pri, rx_sdr_aux, tx_sdr;

	wire [2:0] rx_vr;
	wire [6:0] tx_cmd_srej;

	M_HDLC #(.MASTER(MASTER),.HEADER_FIELD(HEADER_FIELD), .ADDR_WIDTH(ADDR_WIDTH),			.MAX_PACKET_LENGTH(MAX_PACKET_LENGTH)) HDLC (
		.user_clk_pri(user_clk_pri), 
		.user_clk_aux(user_clk_aux),
		.user_clk(user_clk),
		.rx_dat(rx_dat),
		.rx_sdr(rx_sdr_pri),
		.rx_sdr_aux(rx_sdr_aux),
		.rx_ena(rx_ena),
		.rx_dav(rx_dav),
		.rx_eop(rx_eop),
		.rx_sop(rx_sop),
		.rx_adr(rx_adr),
		.rx_err(rx_err),
		.rx_nr(rx_nr),
		.rx_ns(rx_ns),
		.rx_cmd_ua(rx_cmd_ua),
		.rx_cmd_srej(rx_cmd_srej),
		.resetb(resetb),
		.rx_cmd_reset(rx_cmd_reset),
		.rx_cmd_test(rx_cmd_test),
		.tx_dat(tx_dat),
		.tx_sdr(tx_sdr),
		.tx_ena(tx_ena),
		.tx_dav(tx_dav),
		.tx_adr(tx_adr),
		.tx_ns(tx_ns),
		.cmd_busy(cmd_busy),
		.tx_cmd_sabm(tx_cmd_sabm),
		.tx_cmd_reset(tx_cmd_reset),
		.tx_cmd_test(tx_cmd_test),
		.active_aux(active_aux),
		.disable_aux(disable_aux)
	);
	


	wire [7:0] rx_dat_pri,rx_dat_aux,tx_dat_;
	M_serdes serdes_pri (
		.rx_sd(rx_sd),
		.tx_sd(tx_sd),
		.rx_clk(link_clkb),
		.rx_dat(rx_dat_pri),
		.tx_dat(tx_dat_),
		.clk40(user_clk_pri),
		.clkdiv_skip(gnd),
		.resetb(vdd),
		.rx_off(gnd),
		.tx_off(tx_off)
	);
		//.DATARATE(2'b00),

	assign link_clkb= ~link_clk;
	assign rx_sdr_pri = rx_dat_pri[1:0];
	assign tx_dat_ = {6'b0,tx_sdr};
	assign tx_off = tx_cset==4'b1111;
	assign gnd = 1'b0;
	assign vdd=1'b1;
	M_serdes serdes_aux (
		.rx_sd(rx_sd_aux),
		.tx_sd(tx_sd_aux),
		.rx_clk(link_clk_auxb),
		.rx_dat(rx_dat_aux),
		.tx_dat(tx_dat_),
		.clk40(user_clk_aux),
		.clkdiv_skip(gnd),
		.resetb(vdd),
		.rx_off(gnd),
		.tx_off(tx_off)
	);

	assign link_clk_auxb= ~link_clk_aux;
	assign rx_sdr_aux = rx_dat_aux[1:0];

		//.DATARATE(2'b00),
	
endmodule


`timescale 1ns/1ps
module M_serdes (
	 rx_sd, rx_clk, resetb, tx_sd, clk40, tx_dat, rx_dat, rx_off, tx_off, clkdiv_skip
);
	parameter DATARATE = 0;

	input rx_sd, rx_clk, resetb;
	output  tx_sd;
	output  clk40;
	input [7:0] tx_dat;
	output [7:0] rx_dat;
	input rx_off, tx_off;
	input clkdiv_skip;
	
	reg tx_sd, clk40;
	reg [7:0] rx_dat;
	
	
	reg [1:0] clkdiv;
	reg [7:0] des,ser,tx_dat_reg;
	reg tx_sd_neg, tx_sd_pos, tx_sd_negneg;
	reg tx_trig;
	reg ckg_des1, ckg_des2, ckg_des4;
	reg ckg_ser1, ckg_ser2, ckg_ser4;
	reg skip_done;
	
	wire [2:0] __CLKDIV__ = 3'b001 << DATARATE;
	
	reg [1:0] des_;
	
	
	
	always @* begin
		des[1:0] = des_[1:0];

		if (~tx_off) //cadence map_to_mux
			ckg_ser1  = rx_clk;
		else ckg_ser1 = 0;

		if ((DATARATE==1) || (DATARATE==2)) /* cadence map_to_mux */ begin
			ckg_des2  = ckg_des1;
			ckg_ser2  = ckg_ser1;
		end
		else begin
			ckg_des2  = 0;
			ckg_ser2  = 0;
		end

		if (DATARATE==2) /* cadence map_to_mux */ begin
			ckg_des4  = ckg_des2;
			ckg_ser4  = ckg_ser2;
		end
		else begin 
			ckg_ser4  = 0;
			ckg_des4  = 0;
		end

		// took off map_to_mux to enhance synthesis performance
		case (DATARATE) 
			2: begin
				tx_trig = clkdiv[0] & (~clkdiv[1]);
				clk40 = clkdiv[1];
			end
			1: begin
				tx_trig = ~clkdiv[0];
				clk40 = clkdiv[0];

			end
			default: begin
				tx_trig = 1;
				clk40 = ~rx_clk;
			end
		endcase 
		
		if (ckg_ser1) // cadence map_to_mux
			tx_sd = tx_sd_negneg;
		else tx_sd = tx_sd_pos; 
		
	end

	
	always @(posedge ckg_ser1) begin
		// MSB out first << for serializer
		tx_sd_pos <=#0.1 ser[__CLKDIV__*2-1];
		ser[1:0] <=#0.1 tx_dat_reg[1:0];
	end


	always @(negedge ckg_ser1) begin
		tx_sd_neg <=#0.1 ser[__CLKDIV__*2-2];
		tx_sd_negneg <=#0.1 tx_sd_neg;
	end


	always @(posedge ckg_des2) begin
		des[2]<=#0.1 des[0];
		des[3]<=#0.1 des[1];
	end

	always @(posedge ckg_ser2) begin
		if (tx_trig) ser[3:2] <=#0.1 tx_dat_reg[3:2];
		else begin
			ser[3] <=#0.1 ser[1];
			ser[2] <=#0.1 ser[0];
		end
	end


	always @(posedge ckg_des4) begin
		des[4]<=#0.1 des[2];
		des[5]<=#0.1 des[3];
		des[6]<=#0.1 des[4];
		des[7]<=#0.1 des[5];
	end

	always @(posedge ckg_ser4) begin
		if (tx_trig) ser[7:4] <=#0.1 tx_dat_reg[7:4];
		else begin
			ser[7] <=#0.1 ser[5];
			ser[6] <=#0.1 ser[4];
			ser[5] <=#0.1 ser[3];
			ser[4] <=#0.1 ser[2];
		end
	end

	
	
	always @(negedge rx_clk or negedge resetb) begin
		if (~resetb) begin
			clkdiv <=#0.1 0;
		end
		else begin
			clkdiv <=#0.1 clkdiv;
			skip_done <=#0.1 skip_done;
			if ((!clkdiv_skip) || (skip_done)) begin
				clkdiv <=#0.1 clkdiv + 1;
			end
			
			skip_done <=#0.1 clkdiv_skip;
		end
	end


	always @(posedge clk40) begin
		rx_dat <=#0.1 des;
		tx_dat_reg <=#0.1 tx_dat;
	end
	
// DDRDES
	reg rx_sd_neg;
	
	always @* begin
		if (~rx_off) //cadence map_to_mux
			ckg_des1  = rx_clk;
		else  ckg_des1 = 0;
	end

	always @(negedge ckg_des1) begin
 		rx_sd_neg<=#0.1 rx_sd;
	end

	always @(posedge ckg_des1) begin
		// LSB in first << for deserializer
		des_[1] <=#0.1 rx_sd_neg;
		des_[0] <=#0.1 rx_sd;
	end


endmodule

// Triplicate with 
// python tri_netlist.py clkmux < HDLC.v > HDLC_tri.v

`timescale 1ns/1ns
module M_HDLC #(

	parameter MASTER = 0,
	 HEADER_FIELD = 1,
	 ADDR_WIDTH = 5,
	 MAX_PACKET_LENGTH = 16, 
	 THRESHOLD = 2**ADDR_WIDTH -MAX_PACKET_LENGTH-1
) (
	// THRESHOLD = 1<<(ADDR_WIDTH-1), // for unknown packet length

	input [15:0] tx_dat,
	output [15:0] rx_dat,
	input  rx_dav,user_clk_pri, user_clk_aux, 
	output rx_ena, rx_eop, tx_dav, rx_sop, rx_err,
	input resetb, tx_ena, disable_aux,
	output user_clk,
	input [7:0] tx_adr,
	output [7:0] rx_adr,
	input tx_cmd_reset, tx_cmd_test, tx_cmd_sabm,
	output rx_cmd_reset, rx_cmd_test, rx_cmd_ua,
	output [2:0] tx_ns, rx_nr,rx_ns,
	output [6:0] rx_cmd_srej,
	output cmd_busy,
	output [1:0] tx_sdr,
	input [1:0] rx_sdr, rx_sdr_aux,
	output active_aux
);
	wire rx_cmd_reset_, rx_cmd_test_;

	wire [2:0] rx_vr;
	wire [6:0] tx_cmd_srej, tx_cmd_srej_;

	M_rx #(.HEADER_FIELD(HEADER_FIELD), .ADDR_WIDTH(ADDR_WIDTH)) rx (
		.rx_dat(rx_dat),
		.rx_sdr_pri(rx_sdr),
		.clk_pri(user_clk_pri),
		.rx_sdr_aux(rx_sdr_aux),
		.clk_aux(user_clk_aux),
		.clk_active(user_clk),
		.rx_ena(rx_ena),
		.rx_dav(rx_dav),
		.rx_eop(rx_eop),
		.rx_sop(rx_sop),
		.rx_adr(rx_adr),
		.rx_err(rx_err),
		.rx_vr(rx_vr),
		.rx_nr(rx_nr),
		.rx_ns(rx_ns),
		.cmd_busy(cmd_busy),
		.rx_cmd_ua(rx_cmd_ua),
		.tx_cmd_ua(tx_cmd_ua),
		.rx_cmd_srej(rx_cmd_srej),
		.tx_cmd_srej(tx_cmd_srej),
		.resetb(resetb),
		.rx_cmd_reset(rx_cmd_reset_),
		.rx_cmd_test(rx_cmd_test_),
		.tx_cmd_reset(tx_cmd_reset_),
		.active_aux(active_aux),
		.disable_aux(disable_aux)
	);
		//.resetb(MASTER ? resetb : 1'b1),

	M_tx #(.HEADER_FIELD(HEADER_FIELD), .ADDR_WIDTH(ADDR_WIDTH), .THRESHOLD(THRESHOLD)) tx (
		.tx_dat(tx_dat),
		.tx_sdr(tx_sdr),
		.clk(user_clk),
		.tx_ena(tx_ena),
		.tx_dav(tx_dav),
		.tx_adr(tx_adr),
		.tx_ns(tx_ns),
		.tx_nr(rx_vr),
		.cmd_busy(cmd_busy),
		.tx_cmd_sabm(tx_cmd_sabm_),
		.tx_cmd_ua(tx_cmd_ua_),
		.tx_cmd_srej(tx_cmd_srej_),
		.tx_cmd_reset(tx_cmd_reset_),
		.tx_cmd_test(tx_cmd_test_),
		.rx_cmd_reset(rx_cmd_reset),
		.resetb(resetb)
	);
		//.resetb(MASTER ? resetb : 1'b1)
	
	assign rx_cmd_reset = MASTER ? 1'b0 : rx_cmd_reset_ ;
	assign rx_cmd_test = MASTER ? rx_cmd_test_ : 1'b0 ;
	assign tx_cmd_reset_ = MASTER ? tx_cmd_reset : 1'b0 ;
	assign tx_cmd_test_ = MASTER ? tx_cmd_test : rx_cmd_test_ ;

	assign	tx_cmd_sabm_ = MASTER ? tx_cmd_sabm : 1'b0;
	assign  tx_cmd_ua_ = MASTER ? 1'b0 : tx_cmd_ua;
	assign	tx_cmd_srej_ = MASTER ? 7'b0 : tx_cmd_srej;

	
	M_clkmux clkmux (
		.clk_pri(user_clk_pri), .clk_aux(user_clk_aux), .sel(active_aux),
		.out(user_clk)
	);

endmodule

`timescale 1ns/1ns
// Triplicate with 
// python tri_netlist.py Hamming16_24 DELAY6_C < rx.v > rx_tri.v

module M_rx #(
	 parameter 	HEADER_FIELD = 1,
	 			ADDR_WIDTH = 8
) (
	output [15:0] rx_dat,
	output rx_ena, rx_eop, rx_sop, rx_err,
	output [7:0] rx_adr,
	output rx_cmd_reset, rx_cmd_test,rx_cmd_ua, active_aux, tx_cmd_ua,
	output [6:0] rx_cmd_srej,
	output [6:0] tx_cmd_srej,
	output [2:0] rx_vr, rx_nr, rx_ns,
	input clk_pri, rx_dav, clk_aux, clk_active,
	input [1:0] rx_sdr_pri, rx_sdr_aux,
	input resetb, disable_aux,
	input cmd_busy, tx_cmd_reset
);	
	
	wire [7:0] phy_data_pri, phy_data_aux;
	wire [16:0] fifo_data_w_pri, fifo_data_w_aux;
	wire [16:0] fifo_data_r;
	wire [ADDR_WIDTH-1:0] fifo_addr_r_pri, fifo_addr_w_pri, fifo_addr_w;
	wire [ADDR_WIDTH-1:0] fifo_addr_r_aux, fifo_addr_w_aux, fifo_addr_r;
	wire [16:0] fifo_data_w = active_aux ? fifo_data_w_aux : fifo_data_w_pri;
	wire [6:0] rx_cmd_srej_pri, rx_cmd_srej_aux;
	wire [6:0] tx_cmd_srej_pri, tx_cmd_srej_aux;
	wire [2:0] rx_vr_pri, rx_nr_pri, rx_ns_pri;
	wire [2:0] rx_vr_aux, rx_nr_aux, rx_ns_aux;
	wire [15:0] rx_dat_pri, rx_dat_aux;
	wire [7:0] rx_adr_pri, rx_adr_aux;
	
	
	
	wire [23:0] dec_in, enc_out;

	wire [15:0] crc_pri, crc_aux;
	wire rx_cmd_reset_aux;
	
	M_MAC_rx #(.HEADER_FIELD(HEADER_FIELD), .ADDR_WIDTH(ADDR_WIDTH)) MAC_rx_pri (
		.clk(clk_pri),
		.resetb(resetb),
		.rx_dav(rx_dav),
		.fifo_data_r(fifo_data_r),
		.cmd_busy(cmd_busy),
		.rx_dat(rx_dat_pri),
		.rx_ena(rx_ena_pri),
		.rx_ena_pre(rx_ena_pre_pri),
		.rx_eop(rx_eop_pri),
		.rx_sop(rx_sop_pri),
		.rx_adr(rx_adr_pri),
		.rx_err(rx_err_pri),
		.rx_vr(rx_vr_pri),
		.rx_ns(rx_ns_pri),
		.rx_nr(rx_nr_pri),
		.rx_cmd_reset(rx_cmd_reset_pri),
		.rx_cmd_test(rx_cmd_test_pri),
		.rx_cmd_ua(rx_cmd_ua_pri),
		.rx_cmd_sabm(rx_cmd_sabm_pri),
		.rx_cmd_srej(rx_cmd_srej_pri),
		.tx_cmd_srej(tx_cmd_srej_pri),
		.tx_cmd_reset(tx_cmd_reset),
		.phy_data(phy_data_pri),
		.phy_dvalid(phy_dvalid_pri),
		.phy_dstrobe(phy_dstrobe_pri),
		.crc_zero(crc_zero_pri),
		.fifo_data_w(fifo_data_w_pri),
		.fifo_addr_w(fifo_addr_w_pri),
		.fifo_addr_r(fifo_addr_r_pri),
		.fifo_w(fifo_w_pri),
		.active(active_pri),
		.disconnect(disconnect_pri_dis),
		.tx_cmd_ua(tx_cmd_ua_pri),
		.activate(activate_pri),
		.other_active(active_aux_del)
	);
	
	M_MAC_rx #(.HEADER_FIELD(HEADER_FIELD), .ADDR_WIDTH(ADDR_WIDTH)) MAC_rx_aux (
		.clk(clk_aux_dis),
		.resetb(resetb_dis),
		.rx_dav(rx_dav),
		.fifo_data_r(fifo_data_r),
		.cmd_busy(cmd_busy),
		.rx_dat(rx_dat_aux),
		.rx_ena(rx_ena_aux),
		.rx_ena_pre(rx_ena_pre_aux),
		.rx_eop(rx_eop_aux),
		.rx_sop(rx_sop_aux),
		.rx_adr(rx_adr_aux),
		.rx_err(rx_err_aux),
		.rx_vr(rx_vr_aux),
		.rx_ns(rx_ns_aux),
		.rx_nr(rx_nr_aux),
		.rx_cmd_reset(rx_cmd_reset_aux),
		.rx_cmd_test(rx_cmd_test_aux),
		.rx_cmd_ua(rx_cmd_ua_aux),
		.rx_cmd_sabm(rx_cmd_sabm_aux),
		.rx_cmd_srej(rx_cmd_srej_aux),
		.tx_cmd_srej(tx_cmd_srej_aux),
		.tx_cmd_reset(tx_cmd_reset),
		.phy_data(phy_data_aux),
		.phy_dvalid(phy_dvalid_aux),
		.phy_dstrobe(phy_dstrobe_aux),
		.crc_zero(crc_zero_aux),
		.fifo_data_w(fifo_data_w_aux),
		.fifo_addr_w(fifo_addr_w_aux),
		.fifo_addr_r(fifo_addr_r_aux),
		.fifo_w(fifo_w_aux),
		.active(active_aux),
		.disconnect(disconnect_aux),
		.tx_cmd_ua(tx_cmd_ua_aux),
		.activate(activate_aux),
		.other_active(active_pri_del)
	);
	assign crc_zero_aux = crc_aux==16'b0;

//	DELAY6_C delay_active_aux (.Z(active_aux_del1),.A(active_aux), .VDD(),.GND(),.NW(), .SX());
//	DELAY6_C delay_active_aux1 (.Z(active_aux_del2),.A(active_aux_del1), .VDD(),.GND(),.NW(), .SX());
//	DELAY6_C delay_active_aux2 (.Z(active_aux_del3),.A(active_aux_del2), .VDD(),.GND(),.NW(), .SX());
//	DELAY6_C delay_active_aux3 (.Z(active_aux_del4),.A(active_aux_del3), .VDD(),.GND(),.NW(), .SX());
//	DELAY6_C delay_active_aux4 (.Z(active_aux_del),.A(active_aux_del4), .VDD(),.GND(),.NW(), .SX());
//
//	DELAY6_C delay_active_pri (.Z(active_pri_del1),.A(active_pri), .VDD(),.GND(),.NW(), .SX());
//	DELAY6_C delay_active_pri1 (.Z(active_pri_del2),.A(active_pri_del1), .VDD(),.GND(),.NW(), .SX());
//	DELAY6_C delay_active_pri2 (.Z(active_pri_del3),.A(active_pri_del2), .VDD(),.GND(),.NW(), .SX());
//	DELAY6_C delay_active_pri3 (.Z(active_pri_del4),.A(active_pri_del3), .VDD(),.GND(),.NW(), .SX());
//	DELAY6_C delay_active_pri4 (.Z(active_pri_del),.A(active_pri_del4), .VDD(),.GND(),.NW(), .SX());

	assign active_aux_del = active_aux;
	assign active_pri_del = active_pri;

	M_Hamming16_24 Hamming16_24 (
		.enc_in(fifo_data_w[15:0]),
		.enc_out(enc_out),
		.dec_in(dec_in),
		.dec_out(fifo_data_r[15:0])
	);

	wire [24:0] SRAM_data_r, SRAM_data_w;
	M_SRAM #(.DATA_WIDTH(25),.ADDR_WIDTH(ADDR_WIDTH)) fifo (
		.data_w(SRAM_data_w),
		.data_r(SRAM_data_r),
		.addr_w(fifo_addr_w),
		.addr_r(fifo_addr_r),
		.w_enable(fifo_w),
		.clk_w(clk_activeb)
	);
	assign	SRAM_data_w={fifo_data_w[16], enc_out};
	assign	fifo_data_r[16]=SRAM_data_r[24];
	assign  dec_in=SRAM_data_r[23:0];
	assign clk_activeb=~clk_active;

	M_crc #(.DATA_WIDTH(8)) crc16_8_pri (
		.d(phy_data_pri), 
		.init(phy_dvalid_prib),
		.reset_b(resetb),
		.clk(clk_pri),
		.d_valid(phy_dstrobe_pri),
		.crc(crc_pri)
	);
	assign phy_dvalid_prib=~phy_dvalid_pri;

	M_crc #(.DATA_WIDTH(8)) crc16_8_aux (
		.d(phy_data_aux), 
		.init(phy_dvalid_auxb),
		.reset_b(resetb_dis),
		.clk(clk_aux_dis),
		.d_valid(phy_dstrobe_aux),
		.crc(crc_aux)
	);

	assign phy_dvalid_auxb=~phy_dvalid_aux;

	M_PHY_HDLC_rx PHY_HDLC_rx_pri (
		.rx_data(phy_data_pri),
		.rx_sdr(rx_sdr_pri),
		.rx_clk(clk_pri),
		.rx_dvalid(phy_dvalid_pri),
		.rx_dstrobe(phy_dstrobe_pri),
		.resetb(resetb)
	);

	M_PHY_HDLC_rx PHY_HDLC_rx_aux (
		.rx_data(phy_data_aux),
		.rx_sdr(rx_sdr_aux),
		.rx_clk(clk_aux_dis),
		.rx_dvalid(phy_dvalid_aux),
		.rx_dstrobe(phy_dstrobe_aux),
		.resetb(resetb_dis)
	);
	
	M_pri_aux_control pri_aux_control (
		.resetb_pri(resetb),
		.resetb_aux(resetb_dis),
		.disconnect_pri(disconnect_pri_dis),
		.rx_cmd_reset_pri(rx_cmd_reset_pri),
		.rx_cmd_sabm_pri(rx_cmd_sabm_pri),
		.tx_cmd_reset(tx_cmd_reset),
		.disconnect_aux(disconnect_aux),
		.rx_cmd_reset_aux(rx_cmd_reset_aux),
		.rx_cmd_sabm_aux(rx_cmd_sabm_aux), 
		.active_aux(active_aux_int),
		.active_pri(active_pri),
		.clk_pri(clk_pri),
		.clk_aux(clk_aux_dis)
	);
	
	assign active_aux = active_aux_int & (~disable_aux);
	assign clk_aux_dis = clk_aux & (~disable_aux);
	assign resetb_dis = resetb & (~disable_aux);
	assign fifo_addr_r = active_aux ? fifo_addr_r_aux : fifo_addr_r_pri;
	assign fifo_addr_w = active_aux ? fifo_addr_w_aux : fifo_addr_w_pri;
	assign fifo_w = active_aux ? fifo_w_aux : fifo_w_pri;
	assign rx_dat = active_aux ? rx_dat_aux : rx_dat_pri;
	assign rx_ena = active_aux ? rx_ena_aux : rx_ena_pri;
	assign rx_eop = active_aux ? rx_eop_aux : rx_eop_pri;
	assign rx_sop = active_aux ? rx_sop_aux : rx_sop_pri;
	assign rx_err = active_aux ? rx_err_aux : rx_err_pri;
	assign rx_adr = active_aux ? rx_adr_aux : rx_adr_pri;
	assign rx_cmd_reset = rx_cmd_reset_aux | rx_cmd_reset_pri;
	assign rx_cmd_test = active_aux ? rx_cmd_test_aux : rx_cmd_test_pri;
	assign rx_cmd_ua = active_aux ? rx_cmd_ua_aux : rx_cmd_ua_pri;
	assign tx_cmd_ua = active_aux ? tx_cmd_ua_aux : tx_cmd_ua_pri;
	assign rx_cmd_srej = active_aux ? rx_cmd_srej_aux : rx_cmd_srej_pri;
	assign tx_cmd_srej = active_aux ? tx_cmd_srej_aux : tx_cmd_srej_pri;
	assign rx_vr = active_aux ? rx_vr_aux : rx_vr_pri;
	assign rx_nr = active_aux ? rx_nr_aux : rx_nr_pri;
	assign rx_ns = active_aux ? rx_ns_aux : rx_ns_pri;

	assign disconnect_aux = activate_pri;
	assign disconnect_pri = activate_aux;
	assign crc_zero_pri = crc_pri==16'b0;
	assign disconnect_pri_dis = disconnect_pri & (~disable_aux);
	
endmodule
`timescale 1ns/1ns
// Triplicate with
// python tri_netlist.py Hamming16_24 regbank < tx.v > tx_tri.v

module M_tx #(
	parameter HEADER_FIELD = 1,
	ADDR_WIDTH = 12,
	THRESHOLD = 3072
) (
	input [15:0] tx_dat,
	input clk,tx_ena,
	input [7:0] tx_adr,
	input resetb,
	input tx_cmd_reset, tx_cmd_test, rx_cmd_reset,
	output tx_dav,
	output [1:0] tx_sdr,
	output [2:0] tx_ns,
	input [2:0] tx_nr,
	input [6:0] tx_cmd_srej,
	input tx_cmd_sabm,
	input tx_cmd_ua,
	output cmd_busy
);


	wire [7:0] phy_tx_data;
	wire [15:0] crc;
	wire [16:0] fifo_data_w;
	wire [16:0] fifo_data_r;
	wire [ADDR_WIDTH-1:0] fifo_addr_r, fifo_addr_w;
	
	wire [23:0] dec_in, enc_out;


	M_MAC_tx #(.HEADER_FIELD(HEADER_FIELD), .ADDR_WIDTH(ADDR_WIDTH), .THRESHOLD(THRESHOLD)) MAC_tx (
		.tx_dat(tx_dat),
		.clk(clk),
		.tx_ena(tx_ena),
		.tx_dav(tx_dav),
		.tx_adr(tx_adr),
		.tx_cmd_reset(tx_cmd_reset),
		.tx_cmd_test(tx_cmd_test),
		.tx_cmd_sabm(tx_cmd_sabm),
		.tx_cmd_ua(tx_cmd_ua),
		.tx_cmd_srej(tx_cmd_srej),
		.rx_cmd_reset(rx_cmd_reset),
		.cmd_busy(cmd_busy),
		.tx_ns(tx_ns),
		.tx_nr(tx_nr),
		.resetb(resetb),
		.phy_tx_data(phy_tx_data),
		.phy_bytesel(phy_bytesel),
		.phy_dvalid(phy_dvalid),
		.crc(crc),
		.crc_strobe(crc_strobe),
		.phy_dstrobe(phy_dstrobe),
		.fifo_data_w(fifo_data_w),
		.fifo_data_r(fifo_data_r),
		.fifo_addr_w(fifo_addr_w),
		.fifo_addr_r(fifo_addr_r),
		.fifo_w(fifo_w)
	);

	M_Hamming16_24 Hamming16_24 (
		.enc_in(fifo_data_w[15:0]),
		.enc_out(enc_out),
		.dec_in(dec_in),
		.dec_out(fifo_data_r[15:0])
	);

	wire [24:0] SRAM_data_r, SRAM_data_w;

	M_SRAM #(.DATA_WIDTH(25),.ADDR_WIDTH(ADDR_WIDTH)) fifo (
		.data_w(SRAM_data_w),
		.data_r(SRAM_data_r),
		.addr_w(fifo_addr_w),
		.addr_r(fifo_addr_r),
		.w_enable(fifo_w),
		.clk_w(clkb)
	);
	assign	SRAM_data_w={fifo_data_w[16], enc_out};
	assign	fifo_data_r[16]=SRAM_data_r[24];
	assign dec_in = SRAM_data_r[23:0];
	assign clkb = ~clk;

	wire [7:0] phy_tx_data_delayed;
	M_regbank regbank (.D(phy_tx_data), .Q(phy_tx_data_delayed), 
		.clk(clk)
	);



	M_crc #(.DATA_WIDTH(8)) crc16_8 (
		.d(phy_tx_data_delayed), 
		.init(phy_dvalidb),
		.reset_b(resetb),
		.clk(clk),
		.d_valid(crc_strobe),
		.crc(crc)
	);

	assign phy_dvalidb=~phy_dvalid;

	M_PHY_HDLC_tx PHY_HDLC_tx (
		.tx_data(phy_tx_data),
		.tx_sdr(tx_sdr),
		.rx_clk(clk),
		.tx_dvalid(phy_dvalid),
		.tx_dstrobe(phy_dstrobe),
		.resetb(phy_resetb)
	);

	assign phy_resetb = resetb && (~rx_cmd_reset);

endmodule

module M_pri_aux_control (
	resetb_pri, resetb_aux, disconnect_pri, rx_cmd_reset_pri,
		rx_cmd_sabm_pri, tx_cmd_reset, disconnect_aux, rx_cmd_reset_aux,
		rx_cmd_sabm_aux, clk_pri, clk_aux,
	active_aux, active_pri 
);


	input resetb_pri, resetb_aux, disconnect_pri, rx_cmd_reset_pri;
	input 	rx_cmd_sabm_pri, tx_cmd_reset, disconnect_aux, rx_cmd_reset_aux;
	input	rx_cmd_sabm_aux, clk_pri, clk_aux;
	output active_aux, active_pri ;


	reg active_aux, active_pri;
	wire disconnectb_resetb_pri = resetb_pri & (~disconnect_pri);
	wire activate_commands_pri = rx_cmd_reset_pri || rx_cmd_sabm_pri || tx_cmd_reset;

	always @(posedge clk_pri or negedge disconnectb_resetb_pri) begin
		if (~disconnectb_resetb_pri) active_pri <=#1 0;
		else begin
			active_pri <=#1 active_pri;
			if (activate_commands_pri) active_pri <=#1 1;
		end			
	end


	wire disconnectb_resetb_aux = resetb_aux & (~disconnect_aux);
	wire activate_commands_aux = rx_cmd_reset_aux || rx_cmd_sabm_aux || tx_cmd_reset;

	always @(posedge clk_aux or negedge disconnectb_resetb_aux) begin
		if (~disconnectb_resetb_aux) active_aux <=#1 0;
		else begin
			active_aux <=#1 active_aux;
			if (activate_commands_aux) active_aux <=#1 1;
		end			
	end

endmodule

// Triplicate with:
// python tri.py < pri_aux_control.v > pri_aux_control_tri.v
`timescale 1ns/1ns
module M_MAC_tx (tx_dat, tx_ena, tx_dav, tx_adr, clk, tx_nr, tx_ns, tx_cmd_srej,
				resetb, tx_cmd_reset, tx_cmd_test, phy_bytesel, phy_dvalid,
				phy_tx_data, phy_dstrobe, crc, fifo_data_w, fifo_data_r,
				fifo_addr_w, fifo_addr_r, fifo_w, crc_strobe, tx_cmd_sabm,
				tx_cmd_ua, cmd_busy, rx_cmd_reset);

	parameter ADDR_WIDTH = 12;
	parameter THRESHOLD = 3072;
	parameter HEADER_FIELD = 1; 

	input [15:0] tx_dat, crc;
	input tx_ena, clk;
	input [7:0] tx_adr;
	input resetb;
	input tx_cmd_reset, tx_cmd_test, rx_cmd_reset;
	input [6:0] tx_cmd_srej;
	input tx_cmd_sabm;
	input tx_cmd_ua;
	output cmd_busy;
	output tx_dav, crc_strobe;
	output [7:0] phy_tx_data;
	output phy_bytesel, phy_dvalid;
	input phy_dstrobe;
	output fifo_w;
	output [16:0] fifo_data_w;
	input [16:0] fifo_data_r;
	output [ADDR_WIDTH-1:0] fifo_addr_r, fifo_addr_w;
	input [2:0] tx_nr;
	output [2:0] tx_ns;	

	reg [2:0] tx_ns;	
	wire phy_dstrobe;
	reg [ADDR_WIDTH-1:0] fifo_addr_r, fifo_addr_w, fifo_addr_w_last;
	reg phy_bytesel, phy_dvalid;
	reg [1:0] tx_ena_old;
	reg [15:0] tx_dat_old;
	reg [16:0] fifo_data_w;
	reg fifo_w;
	wire [15:0] crc;
	reg crc_strobe;
	//wire [15:0] header = {8'h03,tx_adr}; 
	wire [ADDR_WIDTH-1:0] fifo_nItems = fifo_addr_w - fifo_addr_r;
	
	reg [11:0] issue_cmd;
	reg cmd_in_progress;
	reg [15:0] phy_tx_data16;
	reg [7:0] phy_tx_data;
	reg overflow;
	wire cmd_busy = issue_cmd[0];
	wire resetb_rx_cmd_resetb = resetb & (~rx_cmd_reset);

	always @(posedge clk or negedge resetb_rx_cmd_resetb) begin
		if (~resetb_rx_cmd_resetb) begin
			fifo_addr_w <=#1 0;
			tx_ena_old <=#1 0;
			tx_dat_old <=#1 0;
			tx_ns <=#1 0;
			overflow <=#1 0;
		end
/*		else if (rx_cmd_reset) begin
			fifo_addr_w <=#1 0;
			tx_ena_old <=#1 0;
			tx_dat_old <=#1 0;
			tx_ns <=#1 0;
			overflow <=#1 0;
		end
*/		else begin
			tx_ns <=#1 tx_ns;
			tx_dat_old <=#1 tx_dat_old;
			fifo_addr_w <=#1 fifo_addr_w;
			overflow <=#1 overflow;
			fifo_addr_w_last <=#1 fifo_addr_w_last;
			
			
			tx_ena_old <=#1 {tx_ena_old[0], tx_ena};

			if (((tx_ena | (tx_ena_old[0])) && HEADER_FIELD) ||
				((tx_ena) && (!HEADER_FIELD))) begin

				tx_dat_old <=#1 tx_dat;
				if ((fifo_nItems != {ADDR_WIDTH{1'b1}}) && (~overflow)) 
					fifo_addr_w <=#1 fifo_addr_w + 1;
				else begin
					overflow <=#1 1;
					fifo_addr_w <=#1 fifo_addr_w_last;
				end
			end

			if ((~tx_ena) & (tx_ena_old[0])) tx_ns <=#1 tx_ns + 1;
			if (tx_cmd_reset | tx_cmd_sabm) tx_ns <=#1 0;

			if (!tx_ena_old[0]) fifo_addr_w_last <=#1 fifo_addr_w;


			if ((~tx_ena)&&(fifo_nItems != {ADDR_WIDTH{1'b1}})) overflow <=#1 0;

		end
	end


	assign tx_dav = ~((fifo_nItems >= THRESHOLD) || tx_ena || tx_ena_old[0] ||
						(tx_ena_old[1] && HEADER_FIELD));

	reg [1:0] state;
	parameter [1:0] IDLE = 0,
					START = 1,
					DATA = 2,
					END = 3;
					
	always @(posedge clk or negedge resetb_rx_cmd_resetb) begin
		if (~resetb_rx_cmd_resetb) begin
			fifo_addr_r <=#1 0;
			phy_bytesel <=#1 0;
			phy_dvalid <=#1 0;
			state <=#1 0;
			issue_cmd <=#1 0;
		end
/*		else if (rx_cmd_reset) begin
			fifo_addr_r <=#1 0;
			phy_bytesel <=#1 0;
			phy_dvalid <=#1 0;
			state <=#1 0;
			issue_cmd <=#1 0;
		end
*/		else begin
			state <=#1 state;
			phy_dvalid <=#1 phy_dvalid;
			phy_bytesel <=#1 phy_bytesel;
			fifo_addr_r <=#1 fifo_addr_r;
			issue_cmd <=#1 issue_cmd;
			cmd_in_progress <=#1 cmd_in_progress;

			if (!issue_cmd) begin
				if (tx_cmd_ua && HEADER_FIELD) issue_cmd <=#1 8'h63;
				if (tx_cmd_srej && HEADER_FIELD) issue_cmd <=#1 {tx_cmd_srej,5'h0d};
				if (tx_cmd_sabm && HEADER_FIELD) issue_cmd <=#1 8'h2f;
				if (tx_cmd_test && HEADER_FIELD) issue_cmd <=#1 8'he3;
				if (tx_cmd_reset && HEADER_FIELD) issue_cmd <=#1 8'h8f;
			end
		
			if (phy_dstrobe & phy_dvalid) begin
				if (phy_bytesel) begin
					phy_bytesel <=#1 0;
				end
				else begin
					phy_bytesel <=#1 1;
				end
			end
		
			case (state) // synopsys parallel_case
				IDLE: begin
					phy_dvalid <=#1 0;
					if ((~phy_dvalid) && ((fifo_nItems!=0) || issue_cmd)) begin
						cmd_in_progress <=#1 issue_cmd[0];
						phy_dvalid <=#1 1;
						state <=#1 START;
					end
				end
				
				START: begin
					phy_dvalid <=#1 1;
					if (phy_dstrobe) begin
						if (phy_bytesel) begin
							state <=#1 DATA;
							if (~cmd_in_progress) begin
								fifo_addr_r <=#1 fifo_addr_r + 1;
							end
							else begin
								if (!issue_cmd[11:8]) state <=#1 END;
							end							
						end
					end
				end
			
				DATA: begin
					phy_dvalid <=#1 1;
					if (phy_dstrobe) begin
						if (phy_bytesel) begin
							if (~cmd_in_progress) fifo_addr_r <=#1 fifo_addr_r + 1;
							else state <=#1 END;
						end
					end

					if ((~cmd_in_progress) && (fifo_data_r[16] || (fifo_nItems==0))) begin
						state <=#1 END;
					end	
				end
				
				END: begin
					if (phy_dstrobe) begin
						if (phy_bytesel) begin
							state <=#1 IDLE;
							if (cmd_in_progress) begin
								cmd_in_progress <=#1 0;
								issue_cmd <=#1 0;
							end
						end
					end
				end
			
			endcase
		end
	end
	
	// COMBINATORIAL LOGIC
	always @* begin
		case (state)
			START: begin
				if (HEADER_FIELD) begin
					phy_tx_data16 = {tx_nr,1'b0,fifo_data_r[11:9],1'b0,fifo_data_r[7:0]};
					if (cmd_in_progress) phy_tx_data16 = {issue_cmd, 8'hff};
				end
				else phy_tx_data16 = fifo_data_r;
			end
			DATA: begin
				phy_tx_data16 = fifo_data_r;
				if (cmd_in_progress) phy_tx_data16 = {issue_cmd[10:8],1'b1,issue_cmd[7:5],1'b1};
			end
			END: phy_tx_data16 = crc;
			default: phy_tx_data16 = fifo_data_r;
		endcase

		phy_tx_data = phy_bytesel ? phy_tx_data16[15:8] : phy_tx_data16[7:0];
		crc_strobe = phy_dstrobe & phy_dvalid & (state!=END);
	
		fifo_w = 0;
		fifo_data_w = tx_dat_old;
		if (HEADER_FIELD) begin
			if (tx_ena & (~tx_ena_old[0])) begin
				fifo_data_w =  {1'b1,4'b0,tx_ns,1'b0,tx_adr};
				fifo_w = 1;
			end
			if (tx_ena_old[0]) begin
				fifo_w = 1;
			end
		end
		else begin
			if (tx_ena) begin
				fifo_data_w = {~tx_ena_old[0], tx_dat};
				fifo_w = 1;
			end
		end
	end
	

endmodule

`timescale 1ns/1ns
module M_MAC_rx (rx_dat,  rx_ena, rx_ena_pre,  rx_eop, rx_err, rx_vr, rx_nr,
			rx_sop, rx_adr, clk, rx_dav, resetb, rx_cmd_reset, rx_cmd_test,
			phy_data, phy_dvalid, phy_dstrobe,  crc_zero,fifo_data_w,
			fifo_data_r, fifo_addr_w, fifo_addr_r, fifo_w, cmd_busy, active,
			rx_cmd_ua, rx_cmd_srej, tx_cmd_srej, rx_ns, rx_cmd_sabm,
			disconnect, tx_cmd_ua, tx_cmd_reset, activate, other_active );

	parameter ADDR_WIDTH = 12;
	parameter HEADER_FIELD = 1; 
	parameter MAX_LENGTH = 1<<ADDR_WIDTH;

	output [15:0] rx_dat;
	output rx_ena, rx_eop, rx_sop, rx_ena_pre,  rx_err;
	output [7:0] rx_adr;
	output rx_cmd_reset, rx_cmd_test,rx_cmd_ua,tx_cmd_ua, rx_cmd_sabm;
	output [6:0] rx_cmd_srej;
	output [6:0] tx_cmd_srej;
	input phy_dstrobe;
	output fifo_w;
	output [16:0] fifo_data_w;
	output [ADDR_WIDTH-1:0] fifo_addr_r, fifo_addr_w;
	output [2:0] rx_vr,rx_nr, rx_ns;
	output activate;
	
	input disconnect, other_active, tx_cmd_reset, active;
	input clk, rx_dav;
	input resetb;
	input [7:0] phy_data;
	input phy_dvalid;
	input crc_zero, cmd_busy;
	input [16:0] fifo_data_r;

	reg [ADDR_WIDTH-1:0] fifo_addr_r, fifo_addr_w;
	reg [16:0] fifo_data_w;
	reg fifo_w;

	reg [ADDR_WIDTH-1:0] fifo_addr_w_int, fifo_addr_w_last;
	reg phy_dvalid_old, phy_bytesel;
	wire phy_dstrobe, phy_dvalid;
	wire [7:0] phy_data;
	reg rx_sop;
	reg [7:0] rx_adr;
	reg overflow;
	reg [15:0] rx_dat;
	reg rx_eop;
	wire rx_err = 1'b0;
	reg [7:0] phy_data_lowbyte;
	reg [15:0] phy_data_old,phy_data_old2;
	reg [15:0] packet_length;
	reg rx_ena, rx_ena_pre, rx_cmd_reset, rx_cmd_test, rx_cmd_sabm,rx_cmd_ua,tx_cmd_ua;
	reg [6:0] rx_cmd_srej;
	reg [6:0] tx_cmd_srej;
	reg [2:0] rx_vr, rx_nr, rx_ns;
	reg [7:0] header;

	wire [ADDR_WIDTH-1:0] fifo_nItems = fifo_addr_w_int - fifo_addr_r;
	wire [ADDR_WIDTH-1:0] fifo_nItems_complete = fifo_addr_w_last - fifo_addr_r;
	wire fifo_full = (fifo_nItems==(MAX_LENGTH-1));
	wire [2:0] rx_ns_minus_one = header[3:1]-1;
	
	wire disconnectb_resetb = resetb & (~disconnect);

	wire activate_commands = rx_cmd_reset || rx_cmd_sabm || tx_cmd_reset;
	reg activate;
	always @(posedge activate_commands or negedge other_active) begin
		if (~other_active) activate <=#1 0;
		else activate <=#1 1;
	end


	
	always @(posedge clk or negedge disconnectb_resetb) begin
		if (~disconnectb_resetb) begin
			fifo_addr_w_int <=#1 0;
			//phy_bytesel <=#1 0;
			phy_dvalid_old <=#1 0;
			fifo_addr_w_last <=#1 0;
			//phy_data_old <=#1 0;
			//phy_data_lowbyte <=#1 0;
			//phy_data_old2 <=#1 0;
			rx_cmd_reset <=#1 0;
			rx_cmd_test <=#1 0;
			rx_vr <=#1 0;
			rx_cmd_reset <=#1 0;
			rx_cmd_sabm <=#1 0;
			tx_cmd_srej <=#1 0;
			tx_cmd_ua <=#1 0;
		end
		else if (rx_cmd_reset) begin
			fifo_addr_w_int <=#1 0;
			phy_bytesel <=#1 0;
			phy_dvalid_old <=#1 0;
			fifo_addr_w_last <=#1 0;
			phy_data_old <=#1 0;
			phy_data_lowbyte <=#1 0;
			phy_data_old2 <=#1 0;
			overflow <=#1 0;
			rx_cmd_reset <=#1 0;
			rx_cmd_test <=#1 0;
			rx_vr <=#1 0;
			rx_cmd_sabm <=#1 0;
			tx_cmd_ua <=#1 1;
		end
		else begin
			rx_cmd_reset <=#1 0;
			rx_cmd_test <=#1 0;
			tx_cmd_srej <=#1 0;
			rx_cmd_srej <=#1 0;
			rx_cmd_sabm <=#1 0;
			rx_cmd_ua <=#1 0;
			tx_cmd_ua <=#1 rx_cmd_sabm;
			phy_data_old <=#1 phy_data_old;
			fifo_addr_w_last <=#1 fifo_addr_w_last;
			phy_bytesel <=#1 phy_bytesel;
			fifo_addr_w_int <=#1 fifo_addr_w_int;
			phy_data_lowbyte <=#1 phy_data_lowbyte;
			phy_data_old2 <=#1 phy_data_old2;
			packet_length <=#1 packet_length;
			phy_dvalid_old <=#1 phy_dvalid;
			overflow <=#1 overflow;
			rx_vr <=#1 rx_vr;
			
			if (tx_cmd_reset) rx_vr <=#1 0;
			if (rx_cmd_sabm) rx_vr <=#1 0;
			
			if (phy_dvalid) begin
				if (phy_dstrobe) begin
					if (phy_bytesel) begin
						if (fifo_full || overflow) begin 
							overflow <=#1 1;
						end 
						else begin
							fifo_addr_w_int <=#1 fifo_addr_w_int + 1;
						end
						phy_data_old <=#1 {phy_data,phy_data_lowbyte};
						phy_data_old2 <=#1 phy_data_old;
						phy_bytesel <=#1 0;
						packet_length <=#1 packet_length + 1;
						if ((packet_length==0) && HEADER_FIELD) begin // HEADER
							header <=#1 phy_data;
						end
					end
					else begin
						phy_data_lowbyte <=#1 phy_data;
						phy_bytesel <=#1 1;
					end
				end
			end
			else begin
				packet_length <=#1 0;
				if (fifo_full); else overflow <=#1 0;
				phy_bytesel <=#1 0;
			end
			
			
			if ((~phy_dvalid) && phy_dvalid_old) begin
				if (crc_zero) begin
					if (((packet_length)==2) && HEADER_FIELD) begin
						// COMMAND FRAME
						fifo_addr_w_int <=#1 fifo_addr_w_last;
						if (phy_data_old2[15:8] == 8'h8f) rx_cmd_reset <=#1 1;
						if (phy_data_old2[15:8] == 8'h2f) rx_cmd_sabm <=#1 1;
						if (phy_data_old2[15:8] == 8'h63) rx_cmd_ua <=#1 1;
						if (phy_data_old2[15:8] == 8'he3) rx_cmd_test <=#1 1;
						//if (phy_data_old2[15:11] == 5'h0d) rx_cmd_srej[2:0] <=#1 phy_data_old2[10:8];
					end
					else begin
						if ((!HEADER_FIELD) || (HEADER_FIELD && (!header[0]))) begin
							// INFORMATION FRAME END
							fifo_addr_w_last <=#1 fifo_addr_w_int-1;
							fifo_addr_w_int <=#1 fifo_addr_w_int-1;
							// Receive state variable:
							rx_vr <=#1 header[3:1]+1;
							if (header[3:1]!=rx_vr) begin
								tx_cmd_srej <=#1 {1'b1,rx_ns_minus_one, rx_vr};
							end

							rx_nr <=#1 header[7:5];
						end
						else begin // SREJ FRAME END
							fifo_addr_w_int <=#1 fifo_addr_w_last;
							rx_cmd_srej <=#1 {1'b1,phy_data_old2[7:5],phy_data_old2[3:1]};
						end
					end
				end
				else begin // CRC FAILED
					fifo_addr_w_int <=#1 fifo_addr_w_last;
				end
			end
		end
	end
	
	// COMBINATORIAL LOGIC
	always @* begin
		fifo_w = 0;
		fifo_addr_w = fifo_addr_w_int;
		fifo_data_w = {1'b0,phy_data,phy_data_lowbyte};
		
		if (phy_dstrobe && phy_dvalid && (~fifo_full)) begin
			if (phy_bytesel) begin
				fifo_w=1;
			end
		end

		if ((~phy_dvalid) && phy_dvalid_old) begin
			if (~(((fifo_addr_w_int-fifo_addr_w_last)==2) && HEADER_FIELD)) begin
				fifo_addr_w = fifo_addr_w_int-2;
				fifo_data_w = {1'b1,phy_data_old2};
				fifo_w=1;
			end
		end
	end
	
	
	// FIFO READ INTERFACE 
	always @(posedge clk or negedge disconnectb_resetb) begin
		if (~disconnectb_resetb) begin
			fifo_addr_r <=#1 0;
			//rx_sop <=#1 0;
			//rx_ena <=#1 0;
			//rx_adr <=#1 0;
			rx_ena_pre <=#1 0;
		end
		else if (rx_cmd_reset) begin
			fifo_addr_r <=#1 0;
			rx_sop <=#1 0;
			rx_ena <=#1 0;
			rx_adr <=#1 0;
			rx_ena_pre <=#1 0;
		end
		else begin
			rx_adr <=#1 rx_adr;
			rx_ena <=#1 rx_ena_pre;
			fifo_addr_r <=#1 fifo_addr_r;
			rx_ena_pre <=#1 rx_ena_pre;

			rx_sop <=#1 0;
			rx_eop <=#1 fifo_data_r[16] & rx_ena_pre;

			if ((~rx_ena_pre) && (fifo_nItems_complete!=0) && rx_dav) begin
				rx_ena_pre <=#1 1;
				rx_dat <=#1 fifo_data_r[15:0];				
				fifo_addr_r <=#1 fifo_addr_r + 1;
				if (HEADER_FIELD) begin
					rx_adr <=#1 fifo_data_r[7:0];
					rx_ns <=#1 fifo_data_r[11:9];
				end
				else begin
					rx_sop <=#1 1;
					rx_ena <=#1 1;
					rx_eop <=#1 fifo_data_r[16];
				end
			end

			if (rx_eop & rx_ena) begin
				rx_ena <=#1 0;
				rx_ena_pre <=#1 0;
			end
			
			if (rx_ena_pre) begin
				if (~rx_ena) rx_sop <=#1 1;
				rx_dat <=#1 fifo_data_r[15:0];
				if (~rx_eop) fifo_addr_r <=#1 fifo_addr_r + 1;
			end
			
			if (fifo_nItems_complete==0) begin
				fifo_addr_r <=#1 fifo_addr_r;
				rx_ena_pre <=#1 0;
				rx_ena <=#1 0;
			end
		end
	end



endmodule

// Triplicate with:
// python tri.py < MAC_rx.v > MAC_rx_tri.v
module M_PHY_HDLC_rx (rx_data, rx_sdr, rx_dvalid, rx_dstrobe, rx_clk, resetb);

	output [7:0] rx_data;
	input rx_clk;
	input [1:0] rx_sdr;
	input resetb;
	output rx_dvalid, rx_dstrobe;
	
	reg [8:0] rx_reg;
	reg [7:0] rx_data;

	reg [2:0] ones_count;
	reg [3:0] bit_count;
	reg rx_dvalid, rx_dstrobe;
	reg start;
	
	always @(posedge rx_clk or negedge resetb) begin
		if (~resetb) begin
			//rx_reg <=#1 0;
			//ones_count <=#1 0;
			//bit_count <=#1 0;
			//rx_dvalid <=#1 0;
			//rx_dstrobe <=#1 0;
			//start <=#1 0;
			//rx_dvalid <=#1 0;
			//rx_data <=#1 0;
		end
		else begin
			rx_data <=#1 rx_data;
			start <=#1 start;
			rx_dvalid <=#1 rx_dvalid;
			bit_count <=#1 bit_count;
			ones_count <=#1 ones_count;
			rx_reg <=#1 rx_reg;
		
			rx_dstrobe <=#1 0;
			if (rx_dvalid) start <=#1 0;
			rx_reg <=#1 {rx_sdr[0], rx_sdr[1], rx_reg[8:2]};
			bit_count <=#1 bit_count + 2;
			if ((bit_count==8) && rx_dvalid) begin
				rx_data <=#1 rx_reg[8:1];
				bit_count <=#1 2;
				rx_dstrobe <=#1 1;
			end
			if ((bit_count==9) && rx_dvalid) begin
				rx_data <=#1 rx_reg[7:0];
				bit_count <=#1 3;
				rx_dstrobe <=#1 1;
			end


			if ((ones_count==4) && (~rx_sdr[0]) && (rx_sdr[1])) begin
				rx_reg <=#1 {rx_sdr[1], rx_reg[8:1]};
				bit_count <=#1 bit_count + 1;
				if (bit_count==8) begin
					bit_count <=#1 1;
					rx_data <=#1 rx_reg[8:1];
					rx_dstrobe <=#1 1;
				end
				if (bit_count==9) begin
					bit_count <=#1 2;
					rx_data <=#1 rx_reg[7:0];
					rx_dstrobe <=#1 1;
				end
			end

			if ((ones_count==5) && (~rx_sdr[1])) begin
				rx_reg <=#1 {rx_sdr[0], rx_reg[8:1]};
				bit_count <=#1 bit_count + 1;
				if (bit_count==8) begin
					bit_count <=#1 1;
					rx_data <=#1 rx_reg[8:1];
					rx_dstrobe <=#1 1;
				end
				if (bit_count==9) begin
					bit_count <=#1 2;
					rx_data <=#1 rx_reg[7:0];
					rx_dstrobe <=#1 1;
				end
			end
			
			if (rx_sdr[0] && rx_sdr[1]) begin
				if (ones_count<6) ones_count <=#1 ones_count + 2;
				if (ones_count==6) ones_count <=#1 7;
			end
			else begin
				if (start) rx_dvalid <=#1 1;
				if (~rx_sdr[0]) ones_count <=#1 0;
				else ones_count <=#1 1;
			end
			
			
			if ((ones_count==5) && (~rx_sdr[0]) && (rx_sdr[1])) begin
				rx_dvalid <=#1 0;
				bit_count<=#1 0;
				start <=#1 1;
			end

			if (ones_count==6) begin
				rx_dvalid <=#1 0;
				bit_count<=#1 1;
				if (~rx_sdr[1]) start <=#1 1;
				else start <=#1 0;
			end

			if (ones_count==7) begin
				rx_dvalid <=#1 0;
				bit_count<=#1 0;
				start<=#1 0;
			end

		end
	end
	

endmodule
module M_PHY_HDLC_tx (tx_data, tx_dvalid, tx_dstrobe, tx_sdr, rx_clk, resetb);

	input [7:0] tx_data;
	input rx_clk;
	input tx_dvalid;
	output reg [1:0] tx_sdr;
	output tx_dstrobe;
	input resetb;
	
	reg [8:0] tx_reg;
	
	reg [2:0] ones_count;
	reg [3:0] bit_count;
	reg [1:0] state;
	reg tx_dstrobe;
	reg tx_dvalid_internal;
	
	parameter [1:0] IDLE = 2'd0,
					START= 2'd1,
					TX   = 2'd2,
					END  = 2'd3;
	
	always @(posedge rx_clk or negedge resetb) begin
		if (~resetb) begin
			tx_reg<=#1 9'h1ff;
			tx_sdr[0] <=#1 1;
			tx_sdr[1] <=#1 1;
			ones_count <=#1 0;
			bit_count <=#1 0;
			state <=#1 0;
			tx_dstrobe <=#1 0;
		end
		else begin
			state <=#1 state;
			bit_count <=#1 bit_count;
			ones_count <=#1 ones_count;
			tx_sdr[1] <=#1 tx_sdr[1];
			tx_sdr[0] <=#1 tx_sdr[0];
			tx_reg <=#1 tx_reg;
			tx_dvalid_internal <=#1 tx_dvalid_internal;

			tx_dstrobe <=#1 0;
		
			if (tx_reg[0] && tx_reg[1]) begin
				if (ones_count<6) ones_count <=#1 ones_count + 2;
				if (ones_count==6) ones_count <=#1 7;
			end
			else begin
				if (~tx_reg[1]) ones_count <=#1 0;
				else ones_count <=#1 1;
			end
			
			if (~tx_dvalid) tx_dvalid_internal<=#1 0;

			case (state) // synopsys parallel_case
				//IDLE state is default
				START: begin
					bit_count <=#1 bit_count - 2;
					tx_reg <=#1 tx_reg>>2;
					tx_sdr[0] <=#1 tx_reg[1];
					tx_sdr[1] <=#1 tx_reg[0];
					if ((bit_count ==2) || (bit_count ==1)) begin
						state <=#1 TX;
						tx_reg[7:0] <=#1 tx_data;
						tx_dstrobe <=#1 1;
						bit_count<=#1 8;
					end
					
					if (~tx_dvalid) state <=#1 IDLE;
				end
			
				TX: begin				
					tx_reg <=#1 tx_reg>>2;
					tx_sdr[0] <=#1 tx_reg[1];
					tx_sdr[1] <=#1 tx_reg[0];
					bit_count <=#1 bit_count - 2;
					
					if ((ones_count!=5) && ((ones_count!=4) || (~tx_reg[0]))) begin
						if (bit_count==3) begin
							if (tx_dvalid_internal & tx_dvalid) begin
								tx_dstrobe <=#1 1;
								bit_count <=#1 9;
								tx_reg[8:1] <=#1 tx_data;
							end
							else begin
								bit_count <=#1 1;
								tx_reg[8:1] <=#1 'h7e;
							end
						end
						if (bit_count==2) begin
							bit_count <=#1 8;
							if (tx_dvalid_internal & tx_dvalid) begin
								tx_dstrobe <=#1 1;
								tx_reg[7:0] <=#1 tx_data;
							end
							else begin
								tx_reg[8:0] <=#1 'b101111110;
								state <=#1 END;
							end
						end
					end

					
					if ((ones_count==4) && tx_reg[0]) begin
						tx_sdr[0] <=#1 0;
						tx_sdr[1] <=#1 1;
						tx_reg <=#1 tx_reg>>1;
						ones_count <=#1 0;
						bit_count <=#1 bit_count - 1;
						if (bit_count==2) begin
							bit_count <=#1 9;
							if (tx_dvalid_internal & tx_dvalid) begin
								tx_reg[8:1] <=#1 tx_data;
								tx_dstrobe <=#1 1;
							end
							else begin
								tx_reg[8:1] <=#1 'h7e;
								state <=#1 END;
							end
						end
					end
					
					if (ones_count==5) begin
						tx_sdr[0] <=#1 tx_reg[0];
						tx_sdr[1] <=#1 0;
						tx_reg <=#1 tx_reg>>1;
						ones_count <=#1 tx_reg[0];
						bit_count <=#1 bit_count - 1;
						if (bit_count==2) begin
							bit_count <=#1 9;
							if (tx_dvalid_internal & tx_dvalid) begin
								tx_dstrobe <=#1 1;
								tx_reg[8:1] <=#1 tx_data;
							end
							else begin
								tx_reg[8:1] <=#1 'h7e;
								state <=#1 END;
							end
						end
					end
					
					if (bit_count==1) begin
						state <=#1 END;
						bit_count <=#1 7;
					end
				end
				
				END: begin
					tx_reg <=#1 tx_reg>>2;
					tx_sdr[0] <=#1 tx_reg[1];
					tx_sdr[1] <=#1 tx_reg[0];
					bit_count <=#1 bit_count - 2;
					if ((bit_count==0) || (bit_count==1)) state <=#1 IDLE;

					if ((ones_count==5) && (tx_reg[0])) begin
						tx_sdr[0]<=#1 0;
						if (tx_dvalid) begin
							tx_dvalid_internal<=#1 1;
							state <=#1 TX;
							tx_reg[7:0] <=#1 tx_data;
							tx_dstrobe <=#1 1;
							bit_count<=#1 8;
						end
						else begin
							tx_reg<=#1 'h7f;
							state <=#1 IDLE;
						end
					end

					if (ones_count==6) begin
						tx_sdr[1]<=#1 0;
						if (tx_dvalid) begin
							tx_dvalid_internal<=#1 1;
							state <=#1 TX;
							tx_reg[6:0] <=#1 tx_data[7:1];
							tx_sdr[0] <=#1 tx_data[0];
							tx_dstrobe <=#1 1;
							bit_count<=#1 7;
							ones_count <=#1 tx_data[0];
						end
						else begin
							tx_reg <=#1 'h7f;
							tx_sdr[0]<=#1 1;
							state <=#1 IDLE;
						end
					end
					
				end
				
				default: begin
					//tx_sdr[1]<=#1 1;
					//tx_sdr[0]<=#1 1;
					tx_reg <=#1 tx_reg>>2;
					tx_sdr[0] <=#1 tx_reg[1];
					tx_sdr[1] <=#1 tx_reg[0];
					bit_count <=#1 8;
					tx_dvalid_internal <=#1 0;
					if ((~tx_reg[1]) || (~tx_reg[0]))begin
						if (tx_dvalid) begin
							tx_sdr <=#1 2'b11;
							tx_reg[7:0] <=#1 'h7e;
							bit_count <=#1 8;
							state <=#1 START;
							tx_dvalid_internal<=#1 1;
						end
						else tx_reg<=#1 'h7f;
					end
				end


			endcase

		end
	end


endmodule
//  Author: 	BONACINI Sandro, CERN EP/MIC
//
//  CRC Generator/Checker
//  
//  CRC is calculated right-shifting, the polynomial must therefore
//  be reversed.
//  The output crc is directly the crc register value. There is no
//  inversion in between.
//  Default parameters produce a CRC16-CCITT output.
//  When checking, correct reception is signaled by a zero in the crc
//  output.
//
//  Parameters:
//
//  CRC_WIDTH	    bit width of the crc output;
//
//  DATA_WIDTH	    bit width of the data input (must be at least 2);
//  	    	    
//  INIT_VAL	    initialization value of the crc register,
//  	    	    suggested values are all-zeros and all-ones;
//
//  POLY    	    Polynomial (remeber to reverse it)
//  	    	    i.e. CCITT 1021h has POLY = 'h8408.
//
/////////////////////////////////////////////////////////////////////


`timescale 1ns/1ns
`define delay 1
module M_crc (d, init, reset_b, clk, d_valid, crc);
    // synopsys template

    parameter CRC_WIDTH = 16;
    parameter DATA_WIDTH= 16;
    parameter INIT_VAL	= 'hffff;
    parameter POLY  	= 'h8408;


    // I/Os
    
    input   [(DATA_WIDTH-1): 0] d;
    input   	    	    	init;
    input   	    	    	d_valid;
    input         	    	clk;
    input         	    	reset_b;

    output  [(CRC_WIDTH-1) : 0] crc;
    
    
    // Output regs
    
    reg     [(CRC_WIDTH-1) : 0] crc;


    // Internal wires & regs
    
    reg     [(CRC_WIDTH-1) : 0] next_crc;

    
    // Always statements
    
    always @(d or crc or init) begin
    	next_crc = crc_calc(crc, d);
		if (init) next_crc = crc_calc(INIT_VAL, d);
    end
    
    
    // synopsys async_set_reset "reset_b"
    always @ (posedge clk or negedge reset_b) begin
		if (~reset_b) begin
			crc <=#1 INIT_VAL;
		end
		else begin
			crc <=#1 crc;
			if (init && (~d_valid)) begin
				crc <=#1 INIT_VAL;
			end
			
			if (d_valid) crc <=#1 next_crc;
		end
	end


    // Functions

    function [(CRC_WIDTH-1) : 0] crc_calc;
		input   [(CRC_WIDTH-1) : 0] crc_in;
		input   [(DATA_WIDTH-1): 0] d;
		integer     	    	    i;
    	reg     [(CRC_WIDTH-1) : 0] p_crc[0 :(DATA_WIDTH-2)];
		begin
	    	p_crc[0] = crc_atom(crc_in, d[0]);

	    	for (i=1; i< (DATA_WIDTH-1); i=i+1) begin
	    		p_crc[i] = crc_atom(p_crc[i-1], d[i]);
	    	end

	    	crc_calc = crc_atom(p_crc[DATA_WIDTH-2], d[DATA_WIDTH-1]);
		end
    endfunction
    

    function [(CRC_WIDTH-1) : 0] crc_atom;
		input   [(CRC_WIDTH-1) : 0] crc_in;
		input   	    	    d;
		begin
    	    if (!(crc_in[0] ^ d)) crc_atom = (crc_in >> 1);
			else crc_atom = (crc_in >> 1) ^ POLY[(CRC_WIDTH-1):0];

		end
    endfunction
    
endmodule
module M_clkmux ( input clk_pri, clk_aux, sel, output reg out);
	reg sel_pri_latched, sel_aux_latched;
	always @* begin
		if (clk_pri) ;
		else begin
			sel_pri_latched = 1'b1;
			if (sel) sel_pri_latched = 1'b0;
		end
		
		if (clk_aux) ;
		else begin
			sel_aux_latched = 1'b0;
			if (sel) sel_aux_latched = 1'b1;
		end 
		
		out = (clk_pri & sel_pri_latched) | (clk_aux & sel_aux_latched);
	end
endmodule

/*	always @* begin
		if (active_aux)	// cadence map_to_mux 
			user_clk = user_clk_aux;
		else user_clk = user_clk_pri; 
	end
	*/


/////////////////////////////////////////////////////////////////////
// 
//	Split 16 bits into 2 blocks of 8 bits, do Hamming, and viceversa
//	Created by Sandro Bonacini
//	(long time ago)
//

module M_Hamming16_24 (enc_in, enc_out, dec_in, dec_out);
	input [15:0] enc_in;
	output [23:0] enc_out;
	input [23:0] dec_in;
	output [15:0] dec_out;
	
	wire [23:0] pre_int_enc_out;
	reg [23:0] enc_out, post_int_dec_in;

	wire [2:0] unconnected_dec_out0,unconnected_dec_out1,unconnected_enc_out0,unconnected_enc_out1;

	M_Hamming #(.CHECK_WIDTH(4)) Hamming1 (
		.enc_in({3'b0,enc_in[7:0]}),
		.enc_out({unconnected_enc_out0,pre_int_enc_out[11:0]}),
		.dec_in({3'b0,post_int_dec_in[11:0]}),
		.dec_out({unconnected_dec_out0, dec_out[7:0]})
	);

	M_Hamming #(.CHECK_WIDTH(4)) Hamming2 (
		.enc_in({3'b0,enc_in[15:8]}),
		.enc_out({unconnected_enc_out1,pre_int_enc_out[23:12]}),
		.dec_in({3'b0,post_int_dec_in[23:12]}),
		.dec_out({unconnected_dec_out1,dec_out[15:8]})
	);

	
	integer i;
	always @* begin
		for (i=0; i<12; i=i+1) begin
			enc_out[i<<1] = pre_int_enc_out[i];
			enc_out[(i<<1)+1] = pre_int_enc_out[i+12];
			post_int_dec_in[i] = dec_in[i<<1];
			post_int_dec_in[i+12] = dec_in[(i<<1)+1];
		end
	end

endmodule

////////////////////////
// 
//	Hamming encoder decoder
//	Created by Sandro Bonacini
//	(long time ago)
//

module M_Hamming (enc_in, enc_out, dec_in, dec_out);
	parameter CHECK_WIDTH=3; // this is log2(CODE_WIDTH+1) = log2(n+1) = n-k.
	
	parameter CODE_WIDTH=(1<<CHECK_WIDTH)-1; // n
	parameter DATA_WIDTH=CODE_WIDTH-CHECK_WIDTH; // k
	
	input [DATA_WIDTH:1] enc_in;
	output [CODE_WIDTH:1] enc_out;
	output [DATA_WIDTH:1] dec_out;
	input [CODE_WIDTH:1] dec_in;
	reg [DATA_WIDTH:1] dec_out;
	reg [CODE_WIDTH:1] enc_out;
	reg [CODE_WIDTH:1] syndrome;
	reg [CHECK_WIDTH-1:0] err_pos;
	reg [CODE_WIDTH:1] corrected;
	
	integer i,p,q;
	always @(enc_in) begin
		enc_out = 0;
		for (i=0;i<CHECK_WIDTH;i=i+1) begin
			for (q=1;q<(1<<i);q=q+1) begin
				enc_out[(1<<i)+q] = enc_in[(1<<i)+q-i-1];
				//$display("enc_out[%d] = enc_in[%d];",(1<<i)+q,(1<<i)+q-i-1);
				//$display("enc_out=%b",enc_out);
			end
		end

		for (i=0;i<CHECK_WIDTH;i=i+1) begin
			enc_out[1<<i] = 0;

			for (p=0; p<=((1<<(CHECK_WIDTH-i-1))-1); p=p+1) begin
				for (q=0;q<(1<<i);q=q+1) begin
					enc_out[1<<i] = enc_out[1<<i] ^ enc_out[(p<<(i+1)) + (1<<i) +q];
					//$display("enc_out[%d] += enc_out[%d];",(1<<i),(p<<(i+1)) + (1<<i) +q);
					//$display("enc_out=%b",enc_out);
				end
			end

		end
	end

	always @(dec_in) begin
		err_pos = 0;
		syndrome = dec_in;
		
		for (i=0;i<CHECK_WIDTH;i=i+1) begin
			syndrome[1<<i] = 0;

			for (p=0; p<=((1<<(CHECK_WIDTH-i-1))-1); p=p+1) begin
				for (q=0;q<(1<<i);q=q+1) begin
					syndrome[1<<i] = syndrome[1<<i] ^ syndrome[(p<<(i+1)) + (1<<i) +q];
				end
			end

			//if (syndrome[1<<i] != dec_in[1<<i]) err_pos = err_pos + (1<<i);
			err_pos[i]= syndrome[1<<i] ^ dec_in[1<<i];
		end

		//$display("syndrome: %b, err_pos: %d", syndrome, err_pos);


		corrected = dec_in;
		if (err_pos) corrected[err_pos] = ~corrected[err_pos];
		
		for (i=1;i<CHECK_WIDTH;i=i+1) begin
			for (q=1;q<(1<<i);q=q+1) begin
				dec_out[(1<<i)+q-i-1] = corrected[(1<<i)+q];
			end
		end
		
	end


endmodule


module M_regbank #(
	parameter WIDTH=8
) (
	input [WIDTH-1:0] D,
	output reg [WIDTH-1:0] Q,
	input clk
);

	always @(posedge clk) Q<=#1 D;


endmodule


`timescale 1ns/1ns
/*
// MADE WITH LATCHES: (but IBM's smallest latch is bigger than smallest FF!)
// Artisan TLATX1TF is smaller... 4.4x3.6 um2
//
module SRAM (data_w, data_r, addr_w, addr_r, w_enable, clk_w);
	parameter ADDR_WIDTH = 8;
	parameter DATA_WIDTH = 16;
	parameter SIZE = 1<<ADDR_WIDTH;
 	
	input [ADDR_WIDTH-1:0] addr_w, addr_r;
 	output [DATA_WIDTH-1:0] data_r;
	input [DATA_WIDTH-1:0] data_w;
	input w_enable, clk_w;
	
	reg [SIZE-1:0] wl_w;
	reg [DATA_WIDTH-1:0] mem [SIZE-1:0];
	
	integer i;
	always @* for (i=0; i<SIZE; i=i+1)	begin 
		wl_w[i] = clk_w || (!((i==addr_w) && w_enable));
		if (!wl_w[i]) mem[i] = data_w;
	end

	wire [DATA_WIDTH-1:0] data_r = mem[addr_r];

endmodule
*/


// MADE WITH FFs

/*
module SRAM #(
	parameter ADDR_WIDTH = 3,
	parameter DATA_WIDTH = 4,
	parameter SIZE = 1<<ADDR_WIDTH
)( 	
	input [ADDR_WIDTH-1:0] addr_w, addr_r,
 	output [DATA_WIDTH-1:0] data_r,
	input [DATA_WIDTH-1:0] data_w,
	input w_enable, clk_w
);	
	reg [SIZE-1:0] wl_w, wl_w_latched;
	reg [DATA_WIDTH-1:0] mem [SIZE-1:0];


//	//integer i;
//	always @* for (i=0; i<SIZE; i=i+1)	begin
//		if (!clk_w) begin
//			wl_w_latched[i] = w_enable && (i==addr_w) ;
//		end
//		wl_w[i] = wl_w_latched[i] && clk_w;
	//end
	
	reg w_enable_latched;
	always @* begin
		if (!clk_w) w_enable_latched = w_enable;
	end
	
	wire clk_w_gated = clk_w && w_enable_latched;
	
	generate
		genvar j;
		for (j=0; j<SIZE; j=j+1)	begin: mygen
 			always @(posedge wl_w[j]) mem[j] <=#1 data_w;

			always @* begin
				if (!clk_w_gated) wl_w_latched[j] = (j==addr_w) ;
				wl_w[j] = wl_w_latched[j] && clk_w_gated;
			end
		end
	endgenerate

	assign data_r = mem[addr_r];

endmodule
*/


module M_SRAM (data_w, data_r, addr_w, addr_r, w_enable, clk_w);
	parameter ADDR_WIDTH = 8;
	parameter DATA_WIDTH = 16;
	parameter SIZE = 1<<ADDR_WIDTH;
 	
	input [ADDR_WIDTH-1:0] addr_w, addr_r;
 	output [DATA_WIDTH-1:0] data_r;
	input [DATA_WIDTH-1:0] data_w;
	input w_enable, clk_w;
	
	reg [DATA_WIDTH-1:0] mem [SIZE-1:0];


	always @(posedge clk_w) begin
		if(w_enable) mem[addr_w] <=#1 data_w;
	end
	wire [DATA_WIDTH-1:0] data_r = mem[addr_r];

endmodule


