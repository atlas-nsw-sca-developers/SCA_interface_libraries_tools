// Copyright (c) 2015 
// Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
// All rights reserved.
// You may redistribute and modify this project under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// Please see the CERN OHL v.1.2 for applicable conditions

`timescale 1ns / 1ps

module control_signals(
   input          wb_clk_i,
	input    [2:0] wb_adr_i, 
	input          wb_res_i,
	input   [31:0] wb_dat_i,                           
	input          wb_wen_i,         
	input          wb_stb_i,         
	output  [31:0] wb_dat_o,
	output         wb_ack_o,
	
	output         supply_en,
	output         sca_axudisable,
	output         sca_fuseprog,
	output         sca_testenable,
	output         sca_reset_b
	 );

	assign sca_fuseprog = 1'b0;
	
	WhishboneGenericInterface #(
		.bus_width(32),             
		.ctrlReg_width(1),               
		.statReg_width(4),          
		.dataReg_width(1),                  
		.n_W_reg(6),                         
		.n_R_reg(1),                         
		.adr_width(4)
	)wb_interface(	
		.wb_clk_i(wb_clk_i),                  
		.wb_res_i(wb_res_i),           
		.wb_adr_i(wb_adr_i),     
		.wb_wen_i(wb_wen_i),           
		.wb_dat_o(wb_dat_o),   
		.wb_stb_i(wb_stb_i),
		.wb_dat_i(wb_dat_i), 
		.wb_int_o(),  
		.wb_ack_o(wb_ack_o),
				
		.STATUS({sca_reset_b, sca_testenable, sca_axudisable, supply_en}),     
		.CTRL(),                  
		.CTRL_modifEn(1'b0),    
		.CTRL_int_in(1'b0),      
		.DATA_miso(),       
		.DATA_mosi({sca_reset_b, sca_testenable, sca_axudisable, supply_en}),         
		.int_event()                
	);
	
	
endmodule
