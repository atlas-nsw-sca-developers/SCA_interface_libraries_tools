// Copyright (c) 2015 
// Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
// All rights reserved.
// You may redistribute and modify this project under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// Please see the CERN OHL v.1.2 for applicable conditions

`timescale 1ns / 1ps

module ELinkPhaseSelect(
	input      clk_80,
	input      phctrl_pri, phctrl_aux,
	input      rx_pri_i,   rx_aux_i,
	output     rx_pri_o,   rx_aux_o,
	input      tx_pri_i,   tx_aux_i,
	output reg tx_pri_o,   tx_aux_o
   );
	
	reg rx_pri_ph1, rx_pri_ph2;
	reg rx_aux_ph1, rx_aux_ph2;
	
	assign rx_pri_o = phctrl_pri ? rx_pri_ph1 : rx_pri_ph2;
	assign rx_aux_o = phctrl_aux ? rx_aux_ph1 : rx_aux_ph2;	
	
	always @(negedge clk_80) tx_pri_o   <= tx_pri_i;	
	always @(posedge clk_80) rx_pri_ph1 <= rx_pri_i;
	always @(negedge clk_80) rx_pri_ph2 <= rx_pri_i;
	
	always @(negedge clk_80) tx_aux_o   <= tx_aux_i;	
	always @(posedge clk_80) rx_aux_ph1 <= rx_aux_i;
	always @(negedge clk_80) rx_aux_ph2 <= rx_aux_i;	

endmodule

//
//
//module tb;
//
//	reg clk_80, tx_i;
//	
//	ELinkPhaseSelect dut(
//		.clk_80(clk_80),
//		.rx_i(rx_i),
//		.rx_o(rx_o),
//		.tx_i(tx_i),
//		.tx_o(tx_o),
//		.phctrl(phctrl)
//	);
//	
//	initial begin
//		clk_80 = 0;
//		forever #10 clk_80 =~clk_80;
//	end
//	
//	always @( posedge clk_80 ) begin
//		tx_i <= #10 $random;
//	end
//endmodule 
