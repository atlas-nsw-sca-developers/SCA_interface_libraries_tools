`timescale 1ns / 1ps

module mux(
    input      [3:0] ctrl,
	 input      [7:0] in0, in1, in2, in3, in4, in5, in6, in7,
	 input      [7:0] in8, in9, inA, inB, inC, inD, inE, inF,
	 output reg [7:0] out
	 );
	always @(*) begin
		case(ctrl) 
			4'h0: out = in0;
			4'h1: out = in1;
			4'h2: out = in2;
			4'h3: out = in3;
			4'h4: out = in4;
			4'h5: out = in5;
			4'h6: out = in6;
			4'h7: out = in7;
			4'h8: out = in8;
			4'h9: out = in9;
			4'ha: out = inA;
			4'hb: out = inB;
			4'hc: out = inC;
			4'hd: out = inD;
			4'he: out = inE;
			4'hf: out = inF;
			default: out = 8'h0;
		endcase
	end
endmodule
