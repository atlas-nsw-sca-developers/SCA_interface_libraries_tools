// Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
// This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

`timescale 1ns / 1ps

module SPI_Slave #( 
   parameter BufferSize = 128,
   parameter BitCount   = 7
   )(
   input         wb_clk_i,   
   input         wb_res_i,        
   input	  [5:0] wb_adr_i,            
   output [31:0] wb_dat_o,
   input  [31:0] wb_dat_i,	
   input         wb_wen_i, 
   input         wb_stb_i, 	  
   output        wb_int_o,
   output        wb_ack_o,
   input         SCLK_i,
   output        MISO_o,
   input         MOSI_i,
   input         SSEL_i
   );
   wire    [2:0] CTRL; 
   wire          ENABLE;
   wire  [127:0] DATA_mosi, DATA_miso;
   wire          int_event    = 1'b0;
   wire          STATUS = {SCLK_i,MISO_o,MOSI_i,SSEL_i};
   	
	SPI_StramSlave SPIslave (
		.clk(wb_clk_i), 
		.reset(wb_res_i), 
		.enable_b(ENABLE), 
		.CTRL(CTRL), 
		.SCLK_i(SCLK_i), 
		.MISO_o(MISO_o), 
		.MOSI_i(MOSI_i), 
		.SSEL_i(SSEL_i), 
		.MOSIRX(DATA_miso), 
		.MISOTX(DATA_mosi)
	);
	
	WhishboneGenericInterface #(
		.bus_width(32),               
		.ctrlReg_width(3),                  
		.statReg_width(5),                  
		.dataReg_width(32),                  
		.n_W_reg(4),                         
		.n_R_reg(4),                         
		.adr_width(6)
	)wbinterface(	
		.wb_clk_i(wb_clk_i),                  
		.wb_res_i(wb_res_i),           
		.wb_adr_i(wb_adr_i),     
		.wb_wen_i(wb_wen_i),           
		.wb_dat_o(wb_dat_o),   
		.wb_stb_i(wb_stb_i),
		.wb_dat_i(wb_dat_i), 
		.wb_int_o(wb_int_o),  
		.wb_ack_o(wb_ack_o),
		.ENABLE(ENABLE),
		.STATUS(STATUS),               
		.CTRL(CTRL[2:0]),                   
		.CTRL_modifEn(1'b0),   
		.CTRL_int_in(3'h0),     
		.DATA_miso(DATA_miso[(BufferSize-1):0]),
		.DATA_mosi(DATA_mosi[(BufferSize-1):0]),        	
		.int_event(int_event)                
	);
	
endmodule

// Input and output data are kept on purpose separated 
// not using a common shiftregister for testing reasons

module SPI_StramSlave # (
	parameter blen = 128
	)(  
	input       clk,
	input       reset,	
	input       enable_b,
	input [2:0] CTRL,
	input       SCLK_i,
	output      MISO_o,
	input       MOSI_i,
	input       SSEL_i,
	output reg [blen-1:0] MOSIRX,
	input      [blen-1:0] MISOTX
	);   

	reg  SCLK, SCLK_d, MISO_o;
	reg  [7:0] cnto;
	wire MsbLsb =  CTRL[2];	
	
	always @(posedge clk) SCLK   <= SCLK_i;
	always @(posedge clk) SCLK_d <= SCLK;
	
	wire tx = (CTRL[0] & ~SSEL_i) ? (~SCLK_d & SCLK) : (SCLK_d & ~SCLK);
	wire rx = (CTRL[1] & ~SSEL_i) ? (SCLK_d & ~SCLK) : (~SCLK_d & SCLK);

	always @(posedge clk or posedge reset) begin
		if(reset) MOSIRX <= 0;
		else if(SSEL_i | enable_b) MOSIRX <= 0;
		else if(rx) begin
			if(~MsbLsb) MOSIRX <= {MOSIRX[blen-2:0], MOSI_i};
			else        MOSIRX <= {MOSI_i, MOSIRX[blen-1:1]};
		end
	end
	
	always @(posedge clk or posedge reset) begin
		if(reset) cnto <= 0;
		else if(SSEL_i | enable_b) cnto <= 0;
		else if(tx) cnto <= cnto+8'h1;
	end
		
	always @(*) case({MsbLsb, CTRL[0]})
			2'b00: MISO_o = (cnto!=0)   ? MISOTX[blen-cnto]  : 1'b1;
			2'b10: MISO_o = (cnto!=0)   ? MISOTX[cnto-1]     : 1'b1;
			2'b01: MISO_o = (cnto!=128) ? MISOTX[blen-cnto-1]: 1'b1;
			2'b11: MISO_o = (cnto!=128) ? MISOTX[cnto]       : 1'b1;
			default: MISO_o = 1'b0;
	endcase
	
endmodule
