

`timescale 1ns / 1ps

module LED_notifier #(
	parameter LEN = 10000000
   )(
	input  clk_i,
	input  res_i,
	input  event_i,
	output notif_o
	);
	reg [31:0] cnt;
	assign notif_o = (cnt==32'h0) ? 1'b0 : 1'b1;
	
	always @(posedge clk_i or posedge res_i) begin
		if(res_i) 
			cnt <= 32'h0;
		else begin
			if(event_i) 
				cnt <= LEN;
			else if(cnt>0)  
				cnt <= cnt - 32'h1;
			else 
				cnt <= 32'h0;
		end
	end
endmodule


