// Copyright (c) 2015 
// Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
// All rights reserved.
// You may redistribute and modify this project under the terms of the
// CERN OHL v.1.2. (http://ohwr.org/cernohl). This project is distributed
// WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
// SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 
// Please see the CERN OHL v.1.2 for applicable conditions


`timescale 1ns / 1ps
// control register:  0x0
// data in unlatched: 0x1
// data in latched:   0x3
// data out:          0x2


module GPIO_slave(
	input                 wb_clk_i,
	input           [2:0] wb_adr_i, 
	input                 wb_res_i,
	input          [31:0] wb_dat_i,                           
	input                 wb_wen_i,         
	input                 wb_stb_i,         
	output         [31:0] wb_dat_o,
	output                wb_ack_o,
	inout          [31:0] GPIO
   );
	
	wire           [31:0] GPIO_i, GPIO_o;	
	wire                  direction;

	generate 
		for (genvar i=0; i<32; i=i+1) begin:GPIO_assign
			assign GPIO[i] = (direction) ? GPIO_o[i] : 1'bz;
			assign GPIO_i[i] = GPIO[i];
		end
	endgenerate
	
	WhishboneGenericInterface #(
		.bus_width(32),             
		.ctrlReg_width(1),               
		.statReg_width(8),          
		.dataReg_width(32),                  
		.n_W_reg(1),                         
		.n_R_reg(1),                         
		.adr_width(2)
	)wb_interface(	
		.wb_clk_i(wb_clk_i),                  
		.wb_res_i(wb_res_i),           
		.wb_adr_i(wb_adr_i),     
		.wb_wen_i(wb_wen_i),           
		.wb_dat_o(wb_dat_o),   
		.wb_stb_i(wb_stb_i),
		.wb_dat_i(wb_dat_i), 
		.wb_int_o(),  
		.wb_ack_o(wb_ack_o),
				
		.STATUS(GPIO_i[7:0]),     
		.CTRL(direction),                  
		.CTRL_modifEn(1'b0),    
		.CTRL_int_in(1'b0),      
		.DATA_miso(GPIO_i[31:0]),       
		.DATA_mosi(GPIO_o[31:0]),         
		.int_event()                
	);
	
endmodule
