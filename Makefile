
TB_ROOT:=$(shell pwd)
FW_ROOT=$(TB_ROOT)/firmware/Workspace
SW_ROOT=$(TB_ROOT)/software
ISE_VER=ise14
BIT=$(FW_ROOT)/TopCom_SCAonFPGA.bit
MCSFILE=$(FW_ROOT)/TopCom_SCAonFPGA.mcs
BOARD_TYPE=Genesys
IMPACT_VER := $(shell impact  > /dev/null)
SHELL := /bin/bash
XIL_VERSION ?= 14.7
XIL_ARCH ?= 64
XIL_ISE_ENV  ?= /opt/Xilinx/$(XIL_VERSION)/ISE_DS/settings$(XIL_ARCH).sh
XIL_ISE_PATH ?= /opt/Xilinx/$(XIL_VERSION)/ISE_DS/ISE/bin/lin$(XIL_ARCH)/ise
IMPACT_PATH  ?= /opt/Xilinx/$(XIL_VERSION)/ISE_DS/ISE/bin/lin$(XIL_ARCH)/impact
XIL_SETUP = source $(XIL_ISE_ENV)
DESIGN_FILE := $(DESIGN_PATH)/$(DESIGN).bit
DTS_FILE := $(DTS_PATH)/$(DTS).dts



help:   
	@echo ""     
	@echo "GBT-SCA Test System"
	@echo ""
	@echo "  Software Commands:"
	@echo "  |-  test              - Test the communication"
	@echo "  |-  start             - Start the Test shell"
	@echo "" 
	@echo "  Firmware commands:"
	@echo "  |-  download          - download BIT stream to FPGA (non volatile memory)"
	@echo "  |-  flash             - download MCS stream to on-board flash (volatile memory)"
	@echo "  |-  mcs               - create MCS file"
	@echo "  |-  firmware          - open firmware in ISE"
	@echo ""

mcs:$(MCSFILE)
$(MCSFILE): $(BIT)
	promgen  -s 16384 -p mcs -spi -w  -u 0 $(BIT) 

flash: $(MCSFILE)
	$(IMPACT_PATH) -batch scripts/impact_flash.cmd

download:
	@echo "Downloading Bitstream onto the target board using adept"
	djtgcfg prog -i 0 -d Genesys -f $(FW_ROOT)/TopCom_SCAonFPGA.bit

test:
	python $(SW_ROOT)/SCA_TB.py

start:
	python -i $(SW_ROOT)/SCA_TB.py 

ise:
	export XILINXD_LICENSE_FILE="2112@lxlic01:2112@lxlic02:2112@lxlic03:/opt/Xilinx/Xilinx.lic"
	$(XIL_ISE_PATH) $(FW_ROOT)/SCA_TestSystem.xise &

plot:
	gnuplot software/SCATB/constants/opplt.cmd &
	gnuplot software/SCATB/constants/erplt.cmd &
	


