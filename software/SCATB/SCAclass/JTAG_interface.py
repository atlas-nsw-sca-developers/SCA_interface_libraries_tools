# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 


from ..constants import *
from re import findall
from ..constants import *

class JTAG_interface:
	def __init__(self, eport_handler, ID):
		self._chID            = ID
		self._eport_handler   = eport_handler
		self._Send            = eport_handler.send
		self._Receive         = eport_handler.Receive
		self._SendAndGetReply = eport_handler.SendAndGetReply

	def IsEnable(self): return IsEnable(self._chID, self._eport_handler)
	def Enable(self):   return Enable(  self._chID, self._eport_handler)
	def Disable(self):  return Disable( self._chID, self._eport_handler)
	
	def GetControlRegister(self, raw=False):
		val = {}
		pktr = self._SendAndGetReply(self._chID, JTAG_R_ctrl)
		rec  = unpack( pktr.VAL )
		if raw: return pktr.VAL
		val['TxNEG']   = True if rec[2] & JTAG_TxNEG_mask   else False
		val['RxNEG']   = True if rec[2] & JTAG_RxNEG_mask   else False
		val['LSB']     = True if rec[2] & JTAG_LSB_mask     else False
		val['INVSCLK'] = True if rec[2] & JTAG_INVSCLK_mask else False
		val['IE']      = True if rec[2] & JTAG_IE_mask      else False
		val['CharLen'] = rec[3] & JTAG_CharLen_mask
		val['GO']      = True if rec[2] & JTAG_GO_mask      else False
		val['ARES']    = True if rec[2] & JTAG_RES_mask     else False
		return val
				
	def SetControlRegister(self, field, val=True):
		if type(field) is int: 
			pktr = self._SendAndGetReply(self._chID, JTAG_W_ctrl, reord(field))
		elif type(field) is str:
			if   field is 'TxNEG':   mask = JTG_TxNEG_mask
			elif field is 'RxNEG':   mask = JTG_RxNEG_mask	
			elif field is 'LSB':     mask = JTG_LSB_mask
			elif field is 'INVSCLK': mask = JTG_INVSCLK_mask	
			elif field is 'IE':      mask = JTG_IE_mask
			elif field is '_GO':     mask = JTG_GO_mask	
			elif field is 'RES':    mask = JTG_RES_mask
			else:                    return False
			pktr = self._SendAndGetReply(self._chID, JTAG_R_ctrl)
			if pktr is False: return False 
			rec = reord( pktr.VAL )
			rec = (rec | mask) if val else (rec & ~mask)
			pktr = self._SendAndGetReply(self._chID, JTAG_W_ctrl, reord(rec))
			#rec  = unpack( pktr.VAL )
			#reg  = rec[2] | mask if val else rec[2] & ~mask
			#reg  = pack(rec[3], reg)
			#pktr = self._SendAndGetReply(self._chID, JTAG_W_ctrl, reg)
		return False if pktr is False else True
			
	def SetTrLen(self,val):
		pktr = self._SendAndGetReply(self._chID, JTAG_R_ctrl)
		if pktr is False: return False
		rec  = unpack( pktr.VAL )
		reg  = pack(val, rec[2])
		pktr = self._SendAndGetReply(self._chID, JTAG_W_ctrl, reg)
		return True if pktr is not False else False
		
		
	def SetDivider(self, val): 
		pktr = self._SendAndGetReply(self._chID, JTAG_W_DIV, reord(val))
		return False if pktr is False else True
		
	def GetDivider(self):
		pktr = self._SendAndGetReply(self._chID, JTAG_R_DIV)
		return reord(pktr.VAL) if pktr is not False else False
	
	def StartTransmission(self):
		if self.SetControlRegister('IE', True) is False: return False
		return True if self._SendAndGetReply(self._chID, JTAG_GO) is not False else False
		
	def Cmd(self, cmd, val=0):
		return self._SendAndGetReply(self._chID, cmd, val)
		
	def SetTransmit(self, TDO, TMS, direction = 'msb'):
		nbyte = len(TDO) if len(TDO)<17 else 16
		if len(TMS) != nbyte:
			print '->  JTAG request. Number of bits in TDO and TMS vectors does not match'
			return False 
		for i in range(0,16-nbyte): 
			TDO.append(0)
			TMS.append(0)

		pktr = self._SendAndGetReply(self._chID,  JTAG_W_TDO_TX0, pack( TDO[0],  TDO[1],  TDO[2],  TDO[3]  ))
		if pktr is False: return False
		pktr = self._SendAndGetReply(self._chID,  JTAG_W_TMS_TX0, pack( TMS[0],  TMS[1],  TMS[2],  TMS[3]  ))
		if pktr is False: return False
		if nbyte>4:  
			pktr = self._SendAndGetReply(self._chID,  JTAG_W_TDO_TX1, pack( TDO[4],  TDO[5],  TDO[6],  TDO[7]  ))
			if pktr is False: return False
			pktr = self._SendAndGetReply(self._chID,  JTAG_W_TMS_TX1, pack( TMS[4],  TMS[5],  TMS[6],  TMS[7]  ))
		if nbyte>8:  
			pktr = self._SendAndGetReply(self._chID,  JTAG_W_TDO_TX2, pack( TDO[8],  TDO[9],  TDO[10], TDO[11] ))
			if pktr is False: return False
			pktr = self._SendAndGetReply(self._chID,  JTAG_W_TMS_TX2, pack( TMS[8],  TMS[9],  TMS[10], TMS[11] ))
		if nbyte>12: 
			pktr = self._SendAndGetReply(self._chID,  JTAG_W_TDO_TX3, pack( TDO[12], TDO[13], TDO[14], TDO[15] ))	
			if pktr is False: return False
			pktr = self._SendAndGetReply(self._chID,  JTAG_W_TMS_TX3, pack( TMS[12], TMS[13], TMS[14], TMS[15] ))	
		if pktr is False: return False
		return True

	def GetReceived(self, direction = 'msb', nbyte=16):
			TDI=[]
			pktr = self._SendAndGetReply(self._chID,  JTAG_R_TDI_RX3)		
			if pktr is False: return False
			TDI.extend( unpack(pktr.VAL) )
			pktr = self._SendAndGetReply(self._chID,  JTAG_R_TDI_RX2)		
			if pktr is False: return False
			TDI.extend( unpack(pktr.VAL) )
			pktr = self._SendAndGetReply(self._chID,  JTAG_R_TDI_RX1)		
			if pktr is False: return False
			TDI.extend( unpack(pktr.VAL) )
			pktr = self._SendAndGetReply(self._chID,  JTAG_R_TDI_RX0)		
			if pktr is False: return False
			TDI.extend( unpack(pktr.VAL) )
			return TDI[16-nbyte:16]
		

	
		
	def GO(self, TDO, TMS, direction='msb', mode=0):
		if len(TMS) != len(TDO):
			print '->  JTAG request. Number of bits in TDO and TMS vectors does not match'
			return False 	
		nbyte = len(TDO)
		if self.SetTrLen( nbyte*8 ) is False: return False
		if direction is 'lsb': self.SetControlRegister('LSB',  True)
		else:                  self.SetControlRegister('LSB',  False)
		if mode & 1:           self.SetControlRegister('TxNEG',True)
		else:                  self.SetControlRegister('TxNEG',False)
		if mode & 2:           self.SetControlRegister('RxNEG',True)
		else:                  self.SetControlRegister('RxNEG',False)		
		if self.SetTransmit(TDO=TDO, TMS=TMS, direction=direction) is False: return False
		if self.StartTransmission() is False: return False
		return self.GetReceived(direction, nbyte)
		
	def ARES(self, leng=128): 
		self.SetTrLen(100)	
		return self.SetControlRegister('RES')	
	

	

		
		
		
		
			
			
