# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 


from ..constants import *

class DAC_interface:
	def __init__(self, eport_handler, channel):
		self._send            = eport_handler.send
		self._Receive         = eport_handler.Receive
		self._SendAndGetReply = eport_handler.SendAndGetReply
		self._wtable          = [DAC_W_OUT0, DAC_W_OUT1, DAC_W_OUT2, DAC_W_OUT3]
		self._rtable          = [DAC_R_OUT0, DAC_R_OUT1, DAC_R_OUT2, DAC_R_OUT3]
		self._chID            = channel
	
	def SetCode(self, which, val):
		self._SendAndGetReply(self._chID, self._wtable[which], val)
	
	def SetVoltage(self, which, val):
		val = float(val)*256/1.2
		self._SendAndGetReply(self._chID, self._wtable[which], val)
	
	def GetVoltage(self, which):
		pkt = self._SendAndGetReply(self._chID, self._rtable[which])
		return pkt.VAL>>24
		
	def Enable(self):
		if self._chID < 8:
			pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRB)
			self._SendAndGetReply(CH_NODE , NC_Write_CRB, (pktr.VAL&0xff) | (0x01 << self._chID))
		elif self._chID < 16:
			pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRC)
			self._SendAndGetReply(CH_NODE , NC_Write_CRC, (pktr.VAL&0xff) | (0x01 << (self._chID-8)))
		else:
			pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRD)
			self._SendAndGetReply(CH_NODE , NC_Write_CRD, (pktr.VAL&0xff) | (0x01 << (self._chID-16)))

	def Disable(self):
		if self._chID < 8:
			pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRB)
			self._SendAndGetReply(CH_NODE , NC_Write_CRB, (pktr.VAL&0xff) & (~(0x01 << self._chID)))
		elif self._chID < 16:
			pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRC)
			self._SendAndGetReply(CH_NODE , NC_Write_CRC, (pktr.VAL&0xff) & (~(0x01 << (self._chID-8))))
		else:
			pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRD)
			self._SendAndGetReply(CH_NODE , NC_Write_CRD, (pktr.VAL&0xff) & (~(0x01 << (self._chID-16))))
