# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 


import time  
import random
from DAC_interface  import *
from ADC_interface  import *
from GPIO_interface import *
from I2C_interface  import *
from SPI_interface  import *
from JTAG_interface import *
from ..constants    import *
from ..com          import *

class SCActrl:
	def __init__(self, com, EH):
		self._com             = com
		self._EH              = EH
		self._eport           = eport_handle(com, self._EH)
		self._SendAndGetReply = self._eport.SendAndGetReply
		self._send            = self._eport.send
		self._connect         = self._eport.connect
		self._reset           = self._eport.reset
		self._receive         = self._eport.Receive
		self.EnableEport      = self._eport.enable
		self.DisableEport     = self._eport.disable
		self.DAC              = DAC_interface( self._eport, CH_DAC )
		self.GPIO             = GPIO_interface(self._eport, CH_GPIO)
		self.ADC              = ADC_interface( self._eport, CH_ADC )
		self.SPI              = SPI_interface( self._eport, CH_SPI )
		self.JTAG             = JTAG_interface(self._eport, CH_JTAG)
		self.SEU              = seucnt(self.JTAG, self._EH)
		self.ID               = idreg(self.ADC)
		self.I2C              = []
		self._debud           = False 
		self.I2C.append( I2C_interface(self._eport, CH_I2C0) )
		self.I2C.append( I2C_interface(self._eport, CH_I2C1) )
		self.I2C.append( I2C_interface(self._eport, CH_I2C2) )
		self.I2C.append( I2C_interface(self._eport, CH_I2C3) )
		self.I2C.append( I2C_interface(self._eport, CH_I2C4) )
		self.I2C.append( I2C_interface(self._eport, CH_I2C5) )
		self.I2C.append( I2C_interface(self._eport, CH_I2C6) )
		self.I2C.append( I2C_interface(self._eport, CH_I2C7) )
		self.I2C.append( I2C_interface(self._eport, CH_I2C8) )
		self.I2C.append( I2C_interface(self._eport, CH_I2C9) )
		self.I2C.append( I2C_interface(self._eport, CH_I2C10))
		self.I2C.append( I2C_interface(self._eport, CH_I2C11))
		self.I2C.append( I2C_interface(self._eport, CH_I2C12))
		self.I2C.append( I2C_interface(self._eport, CH_I2C13))
		self.I2C.append( I2C_interface(self._eport, CH_I2C14))
		self.I2C.append( I2C_interface(self._eport, CH_I2C15))
		self.EnableAuxPort()
		self.DisableSerialPort()
		self._com.setctrl(SCA_RESETB['REG'], SCA_RESETB['BIT'], True)
		
	def DebugMode(self, on = True):
		self._debug = on
		self._eport.DebugMode(on)
		
	def Send(self, ch, cmd, data):
		reply = self._SendAndGetReply(ch, cmd, data)
		return False if reply is False else True
	
	def ReadReg(self, ch, cmd):
		reply = self._SendAndGetReply(ch, cmd)
		return False if reply is False else reply.VAL
		
	def WriteReg(self, ch, cmd, dat):
		reply = self._SendAndGetReply(ch, cmd, dat)
		return False if reply is False else True			
	
	def Connect(self, port = 'pri', doprint=True): 
		if not self._connect(port, doprint):
			return False
		if doprint:
			chipid = self.ID.GetID()
			if chipid is not False: print '    Chip ID is: '+hex(chipid)
			else: print '-x  Impossible to read the chip ID'
		return True
		
	def SoftReset(self): return self._reset()
	
	def HardReset(self):
		self._com.setctrl(SCA_RESETB['REG'], SCA_RESETB['BIT'], False)
		time.sleep(0.1)
		self._com.setctrl(SCA_RESETB['REG'], SCA_RESETB['BIT'], True)
		
	def DisableAuxPort(self):
		self._com.setctrl(SCA_AUX_DIS['REG'], SCA_AUX_DIS['BIT'], True)
		
	def EnableAuxPort(self):
		self._com.setctrl(SCA_AUX_DIS['REG'], SCA_AUX_DIS['BIT'], False)
	
	def EnableSerialPort(self):
		self._com.setctrl(SCA_TEST_EN['REG'], SCA_TEST_EN['BIT'], True)
	
	def DisableSerialPort(self):
		self._com.setctrl(SCA_TEST_EN['REG'], SCA_TEST_EN['BIT'], False)
		
	def enablechannel(self, chlist):
		if isinstance(chlist, int):
			word = 1 << chlist
		elif isinstance(chlist, list):
			word = 0
			for i in chlist:
				word = word | (1<<i)
		pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRB)
		self._SendAndGetReply(CH_NODE , NC_Write_CRB, (pktr.VAL&0xff) | (word&0xff))
		pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRC)
		self._SendAndGetReply(CH_NODE , NC_Write_CRC, (pktr.VAL&0xff) | ((word>>8)&0xff))
		pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRD)
		self._SendAndGetReply(CH_NODE , NC_Write_CRD, (pktr.VAL&0xff) | ((word>>16)&0xff))
	
	def disablechannel(self, chlist):
		if isinstance(chlist, int):
			word = 1 << chlist
		elif isinstance(chlist, list):
			word = 0
			for i in chlist:
				word = word | (1<<i)
		pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRB)
		self._SendAndGetReply(CH_NODE , NC_Write_CRB, (pktr.VAL&0xff) & ~(word&0xff))
		pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRC)
		self._SendAndGetReply(CH_NODE , NC_Write_CRC, (pktr.VAL&0xff) & ~((word>>8)&0xff))
		pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRD)
		self._SendAndGetReply(CH_NODE , NC_Write_CRD, (pktr.VAL&0xff) & ~((word>>16)&0xff))	

	def ResetCahnnel(ch):
		self.disablechannel(ch)
		self.enablechannel(ch)

	def getenablechannels(self):
		enable = []
		pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRB)
		for i in range(0,8):
			enable.append((pktr.VAL>>i) & 0b1)
		pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRC)
		for i in range(0,8):
			enable.append((pktr.VAL>>i) & 0b1)
		pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRD)
		for i in range(0,8):
			enable.append((pktr.VAL>>i) & 0b1)
		return enable
	
	def BurnChipID(self, ID=0x0, leng=0.001):
		self.HardReset()
		self.Connect(doprint=False)
		self.ADC.Enable()
		self.ADC.SetGainCalib(0) #to not affect
		self.ID.SetID(ID&0xffffff)
		val = self.ID.GetID()
		if val != ID&0xffffff: 
			print 'Error in setting the e-fuses value'
			return 
		self._com.setctrl(SCA_FUSE_PROG['REG'], SCA_FUSE_PROG['BIT'], True)
		print '->  Burning ID efuses..'
		time.sleep(leng)
		self._com.setctrl(SCA_FUSE_PROG['REG'], SCA_FUSE_PROG['BIT'], False)
		self.HardReset()
		self.Connect(doprint=False)
		self.ADC.Enable()
		val = self.ID.GetID()
		if val != ID&0xffffff: print '-x  Error in value burned on the e-fuses'
		else: print '->  Efuses successfull burned with value: '+hex(ID&0xffffff)
		self.ADC.Disable()
		
	def GetID(self):
		return self.ID.GetID()
		
	def SetFreq(self, val):
		self._com.setctrl(n=EPORT_FREQ['REG'], val=val)
	
		

class eport_handle:
	def __init__(self, com, EH):
		self._com         =  com
		self._EH          =  EH
		self._eportpri    =  ELinkMaster(com, 2, EH)
		self._eportaux    =  ELinkMaster(com, 3, EH)
		self._serialport  =  i2cMaster(com, 1)
		self._active      =  0
		self._sparepakets = []
		self._debug       = False
		self.NReq         = 0
		self._ertable     = {Err_bit:'Error', Inv_CH:'Invalid channel', Inv_Cmd:'Invalid command', Inv_TR:'Invalid transaction ID',     
		                     Inv_LEN:'Invalid packet lenght', CH_dis:'Channel currently disable', CH_Busy:'Channel currently busy'}
		self.enable('pri')
		self.enable('aux')   
		time.sleep(0.1) 
		    	
		    
	def connect(self, which = 'pri', doprint=True):
		self._active = 0
		if (which is 'pri') or (which is 1):
			done = self._eportpri.connect()
			self._com.setctrl(PHSELSECTPRI['REG'], PHSELSECTPRI['BIT'], False)
			self._eportpri.send(0xff, 0xff, 0xff)
			pktr = self._eportpri.receive(2)
			if pktr is not False: self._active = 1	
			else:
				self._com.setctrl(PHSELSECTPRI['REG'], PHSELSECTPRI['BIT'], True)
				self._eportpri.send(0xff, 0xff, 0xff)
				pktr = self._eportpri.receive(2)    
				if pktr is not False: self._active = 1	
			if self._active == 0:  print '-x  Impossible to communicate with the SCA chip on Primary E-Port'
			elif doprint: print '->  Communication established with the SCA chip throught the Primary E-Port'
		elif (which is 'aux') or (which is 2):
			done = self._eportaux.connect()
			self._com.setctrl(PHSELSECTAUX['REG'], PHSELSECTAUX['BIT'], False)
			self._eportaux.send(0xff, 0xff, 0xff)
			pktr = self._eportaux.receive(2)
			if pktr is not False: self._active = 2	
			else:
				self._com.setctrl(PHSELSECTAUX['REG'], PHSELSECTAUX['BIT'], True)
				self._eportaux.send(0xff, 0xff, 0xff)
				pktr = self._eportaux.receive(2)    
				if pktr is not False: self._active = 2	
			if self._active == 0:  print '-x  Impossible to communicate with the SCA chip on Auxiliary E-Port'
			elif doprint: print '->  Communication established with the SCA chip throught the Auxiliary E-Port'			
		else:
			print '-X  Impossible to send the connect command: Invalid port specified'
			return False   	
		return self._active
		    	
	
	def _IdentifyError(self, pkt):
		if pkt.ERR is 0:
			return False
		else:
			if ((pkt.ERR >> CH_Busy) &0x1) == 1: return 2
			message = '->  Error flag received: '
			for i in range(0,8):	
				if (pkt.ERR>>i)&0x1 is 1:
					message += self._ertable[i]
		print  message
		return True	
	
	def DebugMode(self, on=True): self._debug = on
	
	def Receive(self, pkt_sent=None):
		if self._active is 1:  pkt = self._eportpri.receive(1) 
		else:                  pkt = self._eportaux.receive(1)
		if pkt is False:       return False
		self._EH.rx_operation()
		er = self._IdentifyError(pkt)
		if er == True: return False
		if er == 2:
			if isinstance(pkt_sent, scapkt):
				return pkt if pkt_sent.CMD == 0xde or pkt_sent.CMD == 0xda else 'busy' #Well known BUG!!!!!!
			else: return 'busy' 
		elif self._debug:
			ep = 'primary' if self._active is 1 else 'auxiliary'
			print '->  Received command from '+ep+' eport:\n\t-channel:\t'+hex(pkt.CHN)+'\n\t-ID:     \t'+hex(pkt.TID)+'\n\t-error:  \t'+hex(pkt.ERR)+'\n\t-lenght:\t'+hex(pkt.LEN)+'\n\t-data:   \t'+hex(pkt.VAL)
		return pkt
		
	def ReceiveNonBlocking(self):
		if self._active is 1:
			pkt = self._eportpri.receive(0) 
		else:
			pkt = self._eportaux.receive(0)
		if pkt is False:
			print '[X] Error in receiving message from the SCA'
			return False
		self._EH.rx_operation()
		if self._debug:
			ep = 'primary' if self._active is 1 else 'auxiliary'
			print '->  Received command from '+ep+' eport:\n\t-channel:\t'+hex(pkt.CHN)+'\n\t-ID:     \t'+hex(pkt.TID)+'\n\t-error:\t'+hex(pkt.ERR)+'\n\t-lenght:\t'+hex(pkt.LEN)+'\n\t-data:   \t'+hex(pkt.VAL)
		return pkt
		
	def send(self, channel, command, data):
		if   self._active is 1:
			pkt = self._eportpri.send(channel, command, data)
		else:
			pkt = self._eportaux.send(channel, command, data)
		self._EH.tx_operation()
		if pkt is False:
			print '[X] Error in sending message to the SCA'
			return False
		else:
			if self._debug:
				ep = 'primary' if self._active is 1 else 'auxiliary'
				print '->  Sent command on '+ep+' eport:\n\t-channel:\t'+hex(pkt.CHN)+'\n\t-ID:     \t'+hex(pkt.TID)+'\n\t-command:\t'+hex(pkt.CMD)+'\n\t-data:   \t'+hex(pkt.VAL)
			return pkt
	
	def SendAndGetReply(self, ch, cmd, dat=0x0):
		rep = self._SendAndGetReply(ch, cmd, dat)
		if rep is False: return False
		elif rep is 'busy':
			self.ResetChannel(ch)
			return False
		else: return rep
		
	def _SendAndGetReply(self, ch, cmd, dat=0x0):
		for i in range(0,10):		
			self._pkts = self.send(ch, cmd, dat)
			ep = 'primary' if self._active is 1 else 'auxiliary'
			self._pktr = self.Receive(pkt_sent=self._pkts)
			msgd =  hex(dat)  if isinstance(dat, int) else 'None'
			if self._pktr is False: 
				msg = '\n    Sent command on '+ep+' eport was:\n\t-channel:\t'+hex(ch)+'\n\t-ID:     \t'+hex(self._pkts.TID)+'\n\t-command:\t'+hex(cmd)+'\n\t-data:   \t'+msgd
				self._EH.error(22, 'COM - timeout exceded in receiving paket from SCA'+msg)	
			elif self._pktr is 'busy':
				print 'reiterate'
				continue
			else:
				msg  = '\n    Sent and received commands on '+ep+' eport are\n\t-channel:\t'+hex(ch)+'\t\t'+hex(self._pktr.CHN)+'\n\t-ID:     '
				msg += '\t'+hex(self._pkts.TID)+'\t\t'+hex(self._pktr.TID)+'\n\t-CMD/ER:\t'+hex(cmd)+'\t\t'+hex(self._pktr.ERR)+'\n\t-data:     \t'+msgd+'\t\t'+hex(self._pktr.VAL)			
				if self._pktr.TID != self._pkts.TID and not(self._pktr.TID == 0x0 and (self._pkts.CHN == CH_JTAG or self._pkts.CHN ==  CH_GPIO)):
					self._EH.error(CH_NODE, 'COM - Received packet with wrong transaction ID'+msg)
					#self._sparepakets.append(self._pktr)
				elif self._pktr.CHN != self._pkts.CHN:
					self._EH.error(CH_NODE, 'COM - Received packet with wrong channell number'+msg)
					#self._sparepakets.append(self._pktr)
				else: break
		if self._pktr is False: return False
		if self._pktr is 'busy':
			msg = '\n    Sent command on '+ep+' eport was:\n\t-channel:\t'+hex(ch)+'\n\t-ID:     \t'+hex(self._pkts.TID)+'\n\t-command:\t'+hex(cmd)+'\n\t-data:   \t'+msgd
			self._EH.error(ch, 'CH - Channell is permanently busy, possible problems in interrupt generation - reset the channell'+msg)
		return self._pktr


	def EnableChannel(self, chlist):
		if isinstance(chlist, int):
			word = 1 << chlist
		elif isinstance(chlist, list):
			word = 0
			for i in chlist: word = word | (1<<i)
		pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRB)
		self._SendAndGetReply(CH_NODE , NC_Write_CRB, (pktr.VAL&0xff) | (word&0xff))
		pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRC)
		self._SendAndGetReply(CH_NODE , NC_Write_CRC, (pktr.VAL&0xff) | ((word>>8)&0xff))
		pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRD)
		self._SendAndGetReply(CH_NODE , NC_Write_CRD, (pktr.VAL&0xff) | ((word>>16)&0xff))
	
	def DisableChannel(self, chlist):
		if isinstance(chlist, int):
			word = 1 << chlist
		elif isinstance(chlist, list):
			word = 0
			for i in chlist: word = word | (1<<i)
		pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRB)
		self._SendAndGetReply(CH_NODE , NC_Write_CRB, (pktr.VAL&0xff) & ~(word&0xff))
		pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRC)
		self._SendAndGetReply(CH_NODE , NC_Write_CRC, (pktr.VAL&0xff) & ~((word>>8)&0xff))
		pktr = self._SendAndGetReply(CH_NODE , NC_Read_CRD)
		self._SendAndGetReply(CH_NODE , NC_Write_CRD, (pktr.VAL&0xff) & ~((word>>16)&0xff))	
			
	def enable(self, which):	
		if   (which is 'pri') or (which is 1):   self._eportpri.enable()
		elif (which is 'aux') or (which is 2):   self._eportaux.enable()
	
	def disable(self, which):	
		if   (which is 'pri') or (which is 1):   self._eportpri.disable()
		elif (which is 'aux') or (which is 2):   self._eportaux.disable()	
		
		
	def reset(self):
		if self._active is 1:return self._eportpri.reset()
		elif self._active is 2:return self._eportaux.reset()
		else:return False	

	def ResetChannel(self,ch):
		self.DisableChannel(ch)
		self.DisableChannel(ch)
		self.EnableChannel(ch)
		self.EnableChannel(ch)
		

		
class seucnt:
	def __init__(self, ch, EH): 
		self.ch = ch 
		self._EH = EH
		self._tmp = 0

	def ReadCounter(self):
		rep = self.ch.Cmd( SEU_read )
		if rep is False: return False
		val = reord(rep.VAL)&0xffff
		self._EH.UpdateSeuCounter(val)
		self.CleanCounter()

	def CleanCounter(self): 
		en = self.ch.IsEnable()
		if not en: self.ch.Enable()
		rep = self.ch.Cmd( SEU_clean )
		if not en: self.ch.Disable()		
		return False if rep is False else True

class idreg:
	def __init__(self, ch): self.ch = ch 
	def GetID(self):
		en = self.ch.IsEnable()	
		if not en: self.ch.Enable()
		rep = self.ch.Cmd(SCA_R_ChipID)
		if rep is False: return False
		if not en: self.ch.Disable()
		return reord(rep.VAL)
	def SetID(self, val):
		en = self.ch.IsEnable()	
		if not en: self.ch.Enable()
		rep = self.ch.Cmd(SCA_W_ChipID, reord(val))
		if rep is False: return False
		if not en: self.ch.Disable()
		return True
		
		
		
		
	
	
