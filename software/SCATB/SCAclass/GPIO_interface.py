# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 


from ..constants import *

class GPIO_interface:
	def __init__(self, eport_handler, channel):
		self._eport_handler   = eport_handler
		self._send            = eport_handler.send
		self._Receive         = eport_handler.Receive
		self._SendAndGetReply = eport_handler.SendAndGetReply
		self._chID            = channel	

	def IsEnable(self): return IsEnable(self._chID, self._eport_handler)
	def Enable(self):   return Enable(  self._chID, self._eport_handler)
	def Disable(self):  return Disable( self._chID, self._eport_handler)
	
	def SetOutputEnable(self, vect):
		#Set pad direction
		pkt = self._SendAndGetReply(self._chID, GPIO_W_OE, vect)
		return False if pkt is False else True
	
	def SetOutputData(self, vect):
		#Set output value for pads configured as output
		pkt = self._SendAndGetReply(self._chID, GPIO_W_DAT_OUT, vect)
		return False if pkt is False else True

	def GetOutputData(self):
		#read input value on pads configured as input
		pkt = self._SendAndGetReply(self._chID, GPIO_R_DAT_OUT)
		return False if pkt is False else pkt.VAL
		
	def GetInputData(self):
		#read input value on pads configured as input
		pkt = self._SendAndGetReply(self._chID, GPIO_R_DAT_IN)
		return False if pkt is False else pkt.VAL
	
	def EnableInterrupt(self, vect=0xffffffff):
		#enable interrupt mask for each pad configured as input
		pkt1 = self._SendAndGetReply(self._chID, GPIO_W_INTE, vect) 
		#enable system interrupts 
		pkt2 = self._SendAndGetReply(self._chID, GPIO_W_CTRL, 1)
		return False if (pkt1 is False or pkt2 is False) else True
	
	def DisableInterrupt(self):
		#disable system interrupts 
		pkt2 = self._SendAndGetReply(self._chID, GPIO_W_CTRL, 0)
		return False if pkt is False else True
	
	def SetTriggerEdge(edge):
		#read input value on pads configured as input
		if edge is 1 or edge is 'negedge':   vect = 0xffffffff
		elif edge is 0 or edge is 'posedge': vect = 0
		else:
			print 'Invalid edge value'
			return False
		pkt = self._SendAndGetReply(self._chID, GPIO_W_PTRIG, vect)
		return False if pkt is False else True		
	
	def CheckInterrupt(self):
		pktr = self._Receive()
	
	
	
	
