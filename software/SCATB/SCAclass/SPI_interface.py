# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 


from ..constants import *
from re import findall
from ..constants import *

class SPI_interface:
	def __init__(self, eport_handler, ID):
		self._chID            = ID
		self._eport_handler   = eport_handler
		self._Send            = eport_handler.send
		self._Receive         = eport_handler.Receive
		self._SendAndGetReply = eport_handler.SendAndGetReply
		#self.SetControlRegister('IE', True)
		#self.SetControlRegister('ASS',True)

	def IsEnable(self): return IsEnable(self._chID, self._eport_handler)
	def Enable(self):   return Enable(  self._chID, self._eport_handler)
	def Disable(self):  return Disable( self._chID, self._eport_handler)

	def SetSlaveSelect(self, which):
		val = reord(1 << which)
		pktr = self._SendAndGetReply(self._chID,  SPI_W_SS, val)
		return True if pktr is not False else False

	def GetControlRegister(self, raw=False):
		val = {}
		pktr = self._SendAndGetReply(self._chID, SPI_R_ctrl)
		if pktr is False: return False
		rec  = reord(pktr.VAL)
		if raw: return rec
		val['TxNEG']   = True if rec & SPI_TxNEG_mask   else False
		val['RxNEG']   = True if rec & SPI_RxNEG_mask   else False
		val['ASS']     = True if rec & SPI_ASS_mask     else False
		val['LSB']     = True if rec & SPI_LSB_mask     else False
		val['INVSCLK'] = True if rec & SPI_INVSCLK_mask else False
		val['IE']      = True if rec & SPI_IE_mask      else False
		val['CharLen'] = rec & SPI_CharLen_mask
		val['GO']      = True if rec & SPI_GO_mask      else False
		return val
				
	def SetControlRegister(self, field, val=True):
		if type(field) is int: 
			pktr = self._SendAndGetReply(self._chID, SPI_W_ctrl, reord(field))
			if pktr is False: return False
		elif type(field) is str:
			if   field is 'TxNEG':   mask = SPI_TxNEG_mask
			elif field is 'RxNEG':   mask = SPI_RxNEG_mask
			elif field is 'ASS':     mask = SPI_ASS_mask	
			elif field is 'LSB':     mask = SPI_LSB_mask
			elif field is 'INVSCLK': mask = SPI_INVSCLK_mask		
			elif field is 'IE':      mask = JTG_IE_mask
			elif field is '_GO':     mask = JTG_GO_mask	
			else:                    return False
			pktr = self._SendAndGetReply(self._chID, SPI_R_ctrl)
			if pktr is False: return False 
			rec = reord( pktr.VAL )
			rec = (rec | mask) if val else (rec & ~mask)
			pktr = self._SendAndGetReply(self._chID, SPI_W_ctrl, reord(rec))
		return False if pktr is False else True
			
	def SetTrLen(self,val):
		pktr = self._SendAndGetReply(self._chID, SPI_R_ctrl)
		if pktr is False: return False
		rec  = unpack( pktr.VAL )
		reg  = pack(val, rec[2])
		pktr = self._SendAndGetReply(self._chID, SPI_W_ctrl, reg)
		return False if pktr is False else True
		
		
	def SetDivider(self, val): 
		pktr = self._SendAndGetReply(self._chID, SPI_W_DIV, reord(val))
		return False if pktr is False else True
		
	def GetDivider(self):
		pktr = self._SendAndGetReply(self._chID, SPI_R_DIV)
		return reord(pktr.VAL) if pktr is not False else False
	
	def StartTransmission(self):
		if self.SetControlRegister('IE', True) is False: return False
		return True if self._SendAndGetReply(self._chID, SPI_GO) is not False else False
		
	def Cmd(self, cmd, val=0):
		return self._SendAndGetReply(self._chID, cmd, val)
		
	def SetTransmit(self, MOSI, direction = 'msb'):
		nbyte = len(MOSI) if len(MOSI)<17 else 16
		for i in range(0,16-nbyte): MOSI.append(0)
		pktr = self._SendAndGetReply(self._chID,  SPI_W_MOSI_TX0, pack( MOSI[0],  MOSI[1],  MOSI[2],  MOSI[3]  ))
		if pktr is False: return False
		if nbyte>4:  
			pktr = self._SendAndGetReply(self._chID,  SPI_W_MOSI_TX1, pack( MOSI[4],  MOSI[5],  MOSI[6],  MOSI[7]  ))
		if nbyte>8:  
			pktr = self._SendAndGetReply(self._chID,  SPI_W_MOSI_TX2, pack( MOSI[8],  MOSI[9],  MOSI[10], MOSI[11] ))
		if nbyte>12: 
			pktr = self._SendAndGetReply(self._chID,  SPI_W_MOSI_TX3, pack( MOSI[12], MOSI[13], MOSI[14], MOSI[15] ))	
		return False if pktr is False else True
		
	def GetReceived(self, direction = 'msb', nbyte=16):
			MISO=[]
			pktr = self._SendAndGetReply(self._chID,  SPI_R_MISO_RX3)		
			if pktr is False: return False
			MISO.extend( unpack(pktr.VAL) )
			pktr = self._SendAndGetReply(self._chID,  SPI_R_MISO_RX2)		
			if pktr is False: return False
			MISO.extend( unpack(pktr.VAL) )
			pktr = self._SendAndGetReply(self._chID,  SPI_R_MISO_RX1)		
			if pktr is False: return False
			MISO.extend( unpack(pktr.VAL) )
			pktr = self._SendAndGetReply(self._chID,  SPI_R_MISO_RX0)		
			if pktr is False: return False
			MISO.extend( unpack(pktr.VAL) )
			return MISO[16-nbyte:16]
		
	def GO(self, slave, MOSI, direction='msb', mode=0):
		nbyte = len(MOSI)
		self.SetSlaveSelect(slave)
		if self.SetTrLen( nbyte*8 ) is False: return False
		if direction is 'msb': self.SetControlRegister('LSB',  True)
		else:                  self.SetControlRegister('LSB',  False)
		if mode & 1:           self.SetControlRegister('TxNEG',True)
		else:                  self.SetControlRegister('TxNEG',False)
		if mode & 2:           self.SetControlRegister('RxNEG',True)
		else:                  self.SetControlRegister('RxNEG',False)
		if mode & 3:           self.SetControlRegister('INVSCLK', True)
		else:                  self.SetControlRegister('INVSCLK', False)
		if self.SetTransmit(MOSI=MOSI, direction=direction) is False: return False
		if self.StartTransmission() is False: return False
		return self.GetReceived(direction, nbyte)
	
	

		
		
		
		
			
			
