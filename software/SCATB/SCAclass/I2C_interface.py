# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 


from re import findall
from ..constants import *
import time

class I2C_interface:
	def __init__(self, eport_handler, ID):
		self._chID            = ID
		self._eport_handler   = eport_handler
		self._Send            = eport_handler.send
		self._Receive         = eport_handler.Receive
		self._SendAndGetReply = eport_handler.SendAndGetReply
		  
	def IsEnable(self): return IsEnable(self._chID, self._eport_handler)
	def Enable(self):   return Enable(  self._chID, self._eport_handler)
	def Disable(self):  return Disable( self._chID, self._eport_handler)
	
	def pack(self, a, b=0, c=0, d=0, order=False):
		return int('%.2x'%a + '%.2x'%b + '%.2x'%c + '%.2x'%d) if order else int('%.2x'%d + '%.2x'%c + '%.2x'%b + '%.2x'%a)
	
	def WriteMultiByte7b(self, adr, data, speed = 0):
		nbyte = len(data) if len(data)<17 else 16
		pktr = self._SendAndGetReply(self._chID,  I2C_W_CTRL, ( ((nbyte << 2) & I2C_MASK_TRLEN) | (speed & 0b11) ))
		if pktr is False: return False
		for i in range(0,16-nbyte): data.append(0)
		if self._SendAndGetReply(self._chID,  I2C_W_DATA0, pack( data[0],  data[1],  data[2],  data[3],  'inv')) is False: return False
		if self._SendAndGetReply(self._chID,  I2C_W_DATA1, pack( data[4],  data[5],  data[6],  data[7],  'inv')) is False: return False
		if self._SendAndGetReply(self._chID,  I2C_W_DATA2, pack( data[8],  data[9],  data[10], data[11], 'inv')) is False: return False
		if self._SendAndGetReply(self._chID,  I2C_W_DATA3, pack( data[12], data[13], data[14], data[15], 'inv')) is False: return False
		return self._SendAndGetReply(self._chID,  I2C_MultiByte_7b_WRITE, adr & 0x7f)
	
	def WriteMultiByte10b(self, adr, data, speed = 0):
		nbyte = len(data) if len(data)<17 else 16
		pktr = self._SendAndGetReply(self._chID,  I2C_W_CTRL, ( ((nbyte << 2) & I2C_MASK_TRLEN) | (speed & 0b11) ))
		if pktr is False: return False
		# the address is actually up to 15 bits (use the first 11110 to follow the 10 bit standard)
		address = 0b111100000000000 | ( adr &0x3ff)  # I2C 10bit addressing standard
		for i in range(0,16-nbyte): data.append(0)
		if self._SendAndGetReply(self._chID,  I2C_W_DATA0, pack( data[0],  data[1],  data[2],  data[3],  'inv')) is False: return False
		if self._SendAndGetReply(self._chID,  I2C_W_DATA1, pack( data[4],  data[5],  data[6],  data[7],  'inv')) is False: return False
		if self._SendAndGetReply(self._chID,  I2C_W_DATA2, pack( data[8],  data[9],  data[10], data[11], 'inv')) is False: return False
		if self._SendAndGetReply(self._chID,  I2C_W_DATA3, pack( data[12], data[13], data[14], data[15], 'inv')) is False: return False
		return self._SendAndGetReply(self._chID,  I2C_MultiByte_10b_WRITE, reord(address)>>16 )
	
		
		
	def Read(self, adr, nbyte=16, adr_mode = '7bit', speed = 0):
		nbyte = 16 if nbyte>16 else nbyte
		#pktr = self._SendAndGetReply(self._chID,  I2C_W_CTRL, ( ((nbyte << 2) & I2C_MASK_TRLEN) | (speed & 0b11) ))
		pktr = self._SendAndGetReply(self._chID,  I2C_W_CTRL, ( ((nbyte << 2) & I2C_MASK_TRLEN) | 0 ))
		if pktr is False: return False
		pktr = self._SendAndGetReply(self._chID,  I2C_MultiByte_7b_READ, adr & 0x7f)
		if pktr is False: return False
		#time.sleep(0.1)
		pktr = self._SendAndGetReply(self._chID,  I2C_R_DATA3)
		if pktr is False: return False
		rec = unpack( pktr.VAL )
		pktr = self._SendAndGetReply(self._chID,  I2C_R_DATA2)
		if pktr is False: return False
		rec.extend( unpack( pktr.VAL ) )
		pktr = self._SendAndGetReply(self._chID,  I2C_R_DATA1)
		if pktr is False: return False
		rec.extend( unpack( pktr.VAL ) )
		pktr = self._SendAndGetReply(self._chID,  I2C_R_DATA0)
		if pktr is False: return False
		rec.extend( unpack( pktr.VAL ) )
		return rec
		
	
	
