# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 


from re import findall
from ..constants import *
import time

class ADC_interface:
	def __init__(self, eport_handler, channel):
		self._eport_handler   = eport_handler
		self._send            = eport_handler.send
		self._Receive         = eport_handler.Receive
		self._SendAndGetReply = eport_handler.SendAndGetReply
		self._chID            = channel
		self._CurLine         = None
	
	def IsEnable(self): return IsEnable(self._chID, self._eport_handler)
	def Enable(self):   return Enable(  self._chID, self._eport_handler)
	def Disable(self):  return Disable( self._chID, self._eport_handler)
	
	def GetCode(self, line = 0, samples=1, approx=True): 
		if line != self._CurLine: 
			pktr = self.SetInputLine(line)		
			if pktr is False: return False
		if samples == 1: return self.GetAnalogInput()
		else: return self.GetAnalogInputEverage(nmeasures=samples, approx=approx)
	
	def GetVoltage(self, line = 0, samples=1):
		code = self.GetCode(line=line, samples=samples, approx=False)
		if code is False: return False
		return float(code)/4096.0
	
	def _extract(self, val):
		a = findall('..', '%.8x' % val)
		return int(a[1]+a[0],16)
		
	def Cmd(self, cmd, val=0):
		return self._SendAndGetReply(self._chID, cmd, val)	

	def SetInputLine(self, line):
		pktr = self._SendAndGetReply(self._chID , ADC_W_InputLine, pack(line) )
		return False if pktr is False else True
	
	def SetCurrentSource(self, val):
		pktr = self._SendAndGetReply(self._chID , ADC_W_CurrentSource, reord(val) )
		return False if pktr is False else True
	
	def GetCurrentSource(self):
		pktr = self._SendAndGetReply(self._chID , ADC_R_CurrentSource)
		return False if pktr is False else self._extract(pktr.VAL)	
		
	def GetInputLine(self):
		pktr = self._SendAndGetReply(self._chID , ADC_R_InputLine)
		return False if pktr is False else self._extract(pktr.VAL)
	
	def GetAnalogInput(self, line = None):
		if line is not None: 
			pktr = self.SetInputLine(line)		
			if pktr is False: return False
		pktr = self._SendAndGetReply(self._chID , ADC_GO, None)
		return False if pktr is False else self._extract(pktr.VAL)
	
	def GetRawData(self):
		pktr = self._SendAndGetReply(self._chID , ADC_GO, None)		
		return False if pktr is False else self.GetData()
		
	
	def GetAnalogInputEverage(self, nmeasures=10, line=None, approx=True):
		if line is not None: 
			pktr = self.SetInputLine(line)		
			if pktr is False: return False
		res = []
		for i in range(0,nmeasures):
			pktr = self._SendAndGetReply(self._chID , ADC_GO)
			if pktr is not False: res.append(self._extract(pktr.VAL))
		dev = np.std(np.array(res))
		if approx: res = int( round( sum(res)/float(nmeasures) ) )
		else:      res = sum(res)/float(nmeasures)
	 	if res>7000: res = res-8192
		return res, dev
	
	def GetAnalogInputVoltage(self, nmeasures=10, line=None):
		if line is not None: 
			pktr = self.SetInputLine(line)		
			if pktr is False: return False
		val = self.GetAnalogInputEverage(nmeasures, None)
		return float(val)/4096

	def SetGainCalib(self, val):
		pktr = self._SendAndGetReply(self._chID , ADC_W_GainCalibrReg, reord(val))
		return False if pktr is False else True
	
	def GetGainCalib(self):
		pktr = self._SendAndGetReply(self._chID , ADC_R_GainCalibrReg, None)
		res = self._extract(pktr.VAL)
		return False if pktr is False else res
	
	def GetOffset(self):
		pktr = self._SendAndGetReply(self._chID , ADC_R_OFFSET, None)
		res = self._extract(pktr.VAL)
		return False if pktr is False else res	
	
	def GetData(self):
		pktr = self._SendAndGetReply(self._chID , ADC_R_DATA, None)
		res = self._extract(pktr.VAL)
		return False if pktr is False else res	
	
	def GetData_Ofs_Ga(self):
		pktr = self._SendAndGetReply(self._chID , ADC_R_DATA_Ofs_Ga)
		res = self._extract(pktr.VAL)
		return False if pktr is False else res
	
	def GetData_Ofs(self):
		pktr = self._SendAndGetReply(self._chID , ADC_R_DATA_Ofs, None)
		res = self._extract(pktr.VAL)
		return False if pktr is False else res
	
	def GO_SingleSlope(self, line = 0):
		pktr = self._SendAndGetReply(self._chID , ADC_GO_SingleSlope, 1)
		res = self._extract(pktr.VAL)
		return False if pktr is False else res
		
	def pack(self, a, b=0, c=0, d=0, order=False):
		return int('%.2x'%a + '%.2x'%b + '%.2x'%c + '%.2x'%d) if order else int('%.2x'%d + '%.2x'%c + '%.2x'%b + '%.2x'%a)
	
	def unpack(self, val):
		a = findall('..',hex(val))
		return int(a[1],16), int(a[2],16), int(a[3],16), int(a[4],16)
		

	
	
	
	
	
	
	
	
	
