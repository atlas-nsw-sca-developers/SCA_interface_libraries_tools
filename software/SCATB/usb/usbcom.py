# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

import ctypes
import sys
import time
import os
from ..constants import *
from multiprocessing import RLock

class HIF(ctypes.c_uint32):
	pass
class DTP(ctypes.c_uint32):
	pass
class DVC(ctypes.Structure):
	_pack_ = 16
	_fields_ = [('name', ctypes.c_char * 64),('connectionString', ctypes.c_char * 261),('dtp', DTP)]	
	
class usbcom:
	adrt = {'wbadr0':0x8, 'wbadr1':0x9, 'wbadr2':0xa, 'wbadr3':0xb,
	        'wbdat0':0xc, 'wbdat1':0xd, 'wbdat2':0xe, 'wbdat3':0xf,
	        'wbcmd' :0x0, 'wbintv':0x1, 'ctrl'  :0x3, 'status':0x2}
	         
	def __init__(self, DeviceName):
		if sys.platform.startswith("win"):
			self._dmgr = ctypes.cdll.dmgr
			self._depp = ctypes.cdll.depp
		else:
			self._dmgr = ctypes.cdll.LoadLibrary("libdmgr.so")
			self._depp = ctypes.cdll.LoadLibrary("libdepp.so")
		self._lock = RLock()
		self.connected = False
		self._hifInvalid = HIF(0)
		self._hif = HIF()	
		done = self._dmgr.DmgrOpen( ctypes.byref(self._hif) , DeviceName)
		if done is 0:
			print '->  Impossible to enstablish the communication: DMRG open problem.'
			os._exit(0) 
		else:
			done = self._depp.DeppEnable(self._hif)
			if done is False:
				print '->  Impossible to enstablish the communication: DEPP problem'
				return False
			else:
				if self._depp.DeppPutReg(self._hif, 0xff,0,False) is not 0:
					print '->  Communication established with the FPGA firmware'								
					self.connected = True
				else:
					print '->  Impossible to enstablish the communication with the FPGA firmware'
		self.curadr = None
		self.curdev = None
		self.curdat = None
		
	def _DeppWrite(self, adr, val):
		self._lock.acquire()
		tm = time.time()
		r = self._depp.DeppPutReg(self._hif, adr, val, False)
		self._lock.release()
		if r is 0:
			print '->  Impossible to send packetsto the FPGA firmware.'	
			return False
		else:
			return True
		#print 'DeppWrite'+str((time.time()-tm)*1000)
		
	def _DeppRead( self, adr):
		self._lock.acquire()
		val = ctypes.c_ubyte()
		r = self._depp.DeppGetReg(self._hif, adr, ctypes.pointer(val), False)
		self._lock.release()
		if r is 0:
			print '->  Impossible to receive packets from the FPGA firmware.'
			return False
		else:
			return val.value 
	
	def _DeppWriteBlock(self, adr, data):
		self._lock.acquire()
		art = ctypes.c_ubyte * (len(adr)*2)
		ar  = art(adr[0],data[0],adr[1],data[1],adr[2],data[2],adr[3],data[3],adr[4],data[4],)
		pt = ctypes.pointer(ar)
		r = self._depp.DeppPutRegSet(self._hif, pt, len(adr), False)
		self._lock.release()
		if r is 0:
			print '->  Impossible to send packetsto the FPGA firmware.'
			return False
		else:
			return True
		
	def _DmgrEnumDevices(self):
		self._lock.acquire()
		val = ctypes.c_int()
		self._DmgrEnumDevices(ctypes.pointer(val))
		self._lock.release()
		return val.value
		
	def _DmgrGetDvc(self,idvc):
		self._lock.acquire()
		val = DVC()
		self._DmgrGetDvc(idvc, ctypes.pointer(val))
		self._lock.release()
		return val
	
	def close(self):
		print ''
		self._lock.acquire()
		self._depp.DeppDisable(self._hif);
		self._dmgr.DmgrClose(self._hif)
		self._lock.release()
		os._exit(0)
	
	def wbwrite(self, device, wbadr, dat=None, nbyte=4):
		#tm = time.time()
		if self.curdev != device:
			self._DeppWrite(self.adrt['wbadr3'], device  & 0xff)
			self.curdev = device
		if self.curadr != wbadr:
			self._DeppWrite(self.adrt['wbadr0'],(wbadr      ) & 0xff)
			#self._DeppWrite(self.adrt['wbadr1'],(adr >>  8) & 0xff) #removed to speed up 
			#self._DeppWrite(self.adrt['wbadr2'],(adr >> 16) & 0xff) #removed to speed up 
			self.curadr = wbadr
		if dat != None and nbyte != None:
			self._DeppWrite(self.adrt['wbdat0'],(dat      ) & 0xff)
			if nbyte>1: self._DeppWrite(self.adrt['wbdat1'],(dat >>  8) & 0xff)
			if nbyte>2: self._DeppWrite(self.adrt['wbdat2'],(dat >> 16) & 0xff)
			if nbyte>3: self._DeppWrite(self.adrt['wbdat3'],(dat >> 24) & 0xff)
		self._DeppWrite(self.adrt['wbcmd'] , 0x1)	
		#print 'wbwriteslow\t'+str((time.time()-tm)*1000)
	
	def wbwritefast(self, device, wbadr, val=0, nbyte=4):
		#tm  = time.time()
		data = unpack(val)
		adr  = unpack(wbadr)
		art = ctypes.c_ubyte * 18
		ar  = art(self.adrt['wbadr3'], device, self.adrt['wbadr0'], adr[0], 
			    #self.adrt['wbdat2'], adr[2], self.adrt['wbadr1'], adr[1], 
		          self.adrt['wbdat3'], data[3],self.adrt['wbdat2'], data[2],
		          self.adrt['wbdat1'], data[1],self.adrt['wbdat0'], data[0], 
		          self.adrt['wbcmd'] , 0x1)
		pt = ctypes.pointer(ar)
		self._lock.acquire()
		r = self._depp.DeppPutRegSet(self._hif, pt, 7, False)
		self._lock.release()
		if r is 0: print '->  Impossible to send packets to the FPGA firmware.'
		self.curdev = device
		self.curadr = wbadr
		#print 'wbwritefast\t'+str((time.time()-tm)*1000)

	def repeat_wbwrite(self):
		self._DeppWrite(self.adrt['wbcmd'] , 0x1)	
		
	def wbread(self, device, wbadr, nbyte=4):
		if self.curdev != device:
			self._DeppWrite(self.adrt['wbadr3'], device  & 0xff)
			self.curdev = device
		if wbadr != self.curadr:
			self._DeppWrite(self.adrt['wbadr0'],(wbadr      ) & 0xff)
			self.curadr = wbadr
		self._DeppWrite(self.adrt['wbcmd'] , 0x2)	
		if nbyte is not None: #for example when cleaning interrupt
			dat = (self._DeppRead(self.adrt['wbdat0']))
			if nbyte>1: dat = (self._DeppRead(self.adrt['wbdat1']) <<  8) | dat
			if nbyte>2: dat = (self._DeppRead(self.adrt['wbdat2']) << 16) | dat
			if nbyte>3: dat = (self._DeppRead(self.adrt['wbdat3']) << 24) | dat
			return dat	
		return 0		
	
	def wbreadfast(self, device, wbadr, nbyte=4):	
		#tm = time.time()
		if self.curdev != device or wbadr != self.curadr:
			self.curdev = device
			self.curadr = wbadr
			adr  = unpack(wbadr)
			art = ctypes.c_ubyte * 9
			ar  = art(self.adrt['wbadr3'], device, self.adrt['wbadr0'], adr[0], 
			          #self.adrt['wbdat2'], adr[2], self.adrt['wbadr1'], adr[1], 
			          self.adrt['wbcmd'] , 0x2)
			pt = ctypes.pointer(ar)
			self._lock.acquire()
			r = self._depp.DeppPutRegSet(self._hif, pt, 3, False)
			self._lock.release()
			if r is 0: return False
		else: self._DeppWrite(self.adrt['wbcmd'] , 0x2)	
		if nbyte is not None: #for example when cleaning interrupt
			arta = ctypes.c_ubyte * 8
			artd = ctypes.c_ubyte * 8
			ara  = arta(self.adrt['wbdat0'],self.adrt['wbdat1'],self.adrt['wbdat2'],self.adrt['wbdat3'])
			ard  = artd()
			self._lock.acquire()
			self._depp.DeppGetRegSet(self._hif, ctypes.pointer(ara), ctypes.pointer(ard), 8)
			self._lock.release()
			return pack(ard[0],ard[1],ard[2],ard[3], 'inv')
		else:
			return 0

	def wbgetint(self):
		return self._DeppRead(self.adrt['wbintv'])
	
	def setctrl(self, n, bit=None, val=0):
		if bit is None:
			self._DeppWrite(n+3, val)
		else:
			dat = self._DeppRead(n+3)
			dat = (dat|(1<<bit)) if val else (dat&~(1<<bit))
			self._DeppWrite(n+3,dat)
						
	def getctrl(self, n):
		if n is 0:
			return self._DeppRead(0x2)
		elif n is 1:
			return self._DeppRead(0x4)
		elif n is 2:
			return self._DeppRead(0x5)
		elif n is 3:
			return self._DeppRead(0x6)
		
	def getstatus(self):
		return self._DeppRead(self.adrt['status'])


