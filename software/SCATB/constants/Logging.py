# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

import time 
import datetime
from re import findall
import numpy as np
from multiprocessing import Process, Pipe
import os
import atexit
from subprocess import Popen
import threading
import Constants

####### Error handler ###########################
class ErrHandle:
	def __init__(self, UpdateRate=0.1): 
		self._erlog  = 'log/ErLog'+str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H-%M-%S'))+'.log'
		self._seulog = 'log/SEUlog'+str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H-%M-%S'))+'.log'
		self._erplt  = 'software/tmp/erplt.log' 
		self._opplt  = 'software/tmp/opplt.log' 
		self._UpdateRate            = UpdateRate
		self._ErrorCounter          = [0]*23
		self._TxOperationCounter    = 0
		self._CurTxOperationCounter = 0
		self._RxOperationCounter    = 0
		self._CurRxOperationCounter = 0
		self._SeuCounter            = 0
		self._timedist              = 0
		self._prev_seuupdate        = 0
		self._SeuTotal              = 0
		self._oplist                = [[0,0,0]]*(int(1/self._UpdateRate)*60)
		self._lastupdate            = 0
		self.openwindow             = False 
		self._StartTime = time.time()
		fo = open(self._erlog, 'w')
		fs = open(self._seulog, 'w')
		fo.write('--------------------------- SCA test Log file ---------------------------')
		fs.write('--------------------------- SCA test Log file ---------------------------')
		fo.write('\nStart Time:\t'+datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d at %H:%M:%S') )
		fs.write('\nStart Time:\t'+datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d at %H:%M:%S') )		
		msg = raw_input("Insert the selected Ions: ")
		fo.write('\nIon:      \t'+msg)
		fs.write('\nIon:      \t'+msg)
		msg = raw_input("Insert the beam Flux: ")
		fo.write('\nFlux:     \t'+msg)
		fs.write('\nFlux:     \t'+msg)
		msg = raw_input("Insert the tilting angle: ")
		fo.write('\nAngle:    \t'+msg+'\n')
		fs.write('\nAngle:    \t'+msg+'\n')
		fo.write('-------------------------------------------------------------------------\n')
		fs.write('-------------------------------------------------------------------------\n')
		fo.close()
		fs.close()		
		print ''
		self.parent_conn, self.child_conn = Pipe()
		self.p = Process(target=self.PlotUpdate, args=(self.child_conn,))
		atexit.register(self.p.terminate)
		self.p.start()
		#self.PlotStatistics()
		self._pltlog()
	
	def PlotStatistics(self):
		self._plot1 = Popen(['gnuplot', 'software/SCATB/constants/opplt.cmd'],  shell=False)
		self._plot2 = Popen(['gnuplot', 'software/SCATB/constants/erplt.cmd'],  shell=False)
		self._plot2 = Popen(['gnuplot', 'software/SCATB/constants/seuplt.cmd'], shell=False)
	
	def CLosePlots(self):
		self._plot1.terminate()
		self._plot2.terminate()
		self._plot3.terminate()
		
	def close(self):
		self.parent_conn.send('exit')
		self._plot1.terminate()
		self._plot2.terminate()
		self.p.join()

	def PlotUpdate(self, conn):
		topprev   = 0
		trxrev    = 0
		tseuprev  = 0
		seuprev   = 0
		tx_oprate = 0
		seurate   = 0
		try:
			while True:
				time.sleep(self._UpdateRate)
				if conn.poll(): 
					dat = conn.recv()
					if dat == 'exit': break				
					if dat['id'] is 0:
						tx_oprate  = float(dat['op']) / (dat['time']-topprev)
						topprev = dat['time']
					elif dat['id'] is 1:
						rx_oprate  = float(dat['op']) / (dat['time']-trxrev)
						trxrev = dat['time']
					elif dat['id'] is 2:
						seurate =  float(dat['op'])
						#print dat['op']
						#print dat['time']
						tseuprev = dat['time']
						seuprev  = dat['op']
				else: 
					tx_oprate = 0
					rx_oprate = 0
				if self.DeltaTime(False)-tseuprev > 1: seurate = 0
				if self.DeltaTime(False)-topprev > 1: tx_oprate = 0
				if self.DeltaTime(False)-trxrev > 1: rx_oprate = 0
				self._oplist.pop(0)
				self._oplist.append([tx_oprate, rx_oprate, seurate])
				fo = open(self._opplt, 'w')
				for i in range(0,len(self._oplist)): 
					fo.write(str(i) + '\t' + str(self._oplist[i][0]) + '\t' + str(self._oplist[i][1])+ '\t' + str(self._oplist[i][2])+ '\n' )
				fo.close()
			conn.close()
    		except KeyboardInterrupt:
    			self.PlotUpdate(conn)
	
	def _logerrors(self, message): 
		fo = open(self._erlog, 'a')
		fo.write(message+'\n')
		fo.close()
		
	def _pltlog(self):
		fo = open(self._erplt, 'w')
		rate = np.array(self._ErrorCounter) / self.DeltaTime(formatted=False)
		for i in range(0,len(self._ErrorCounter)):
			fo.write(str(i) + '\t\t' + str(self._ErrorCounter[i]) + '\t\t' + str(rate[i]) +'\n')
		fo.close()
			
	def OperationCounter(self): return self._TxOperationCounter
	def ErrorCounter(self): return self._ErrorCounter
	def StartTime(self): return self._StartTime
	def CurrentTime(self): return time.time()
	
	def UpdateSeuCounter(self, val): 
		self._SeuCounter = val
		curtime = self.DeltaTime(False)
		freq = float(val)/(curtime-self._prev_seuupdate)
		self.parent_conn.send({'time':curtime, 'id':2, 'op':freq})
		fo = open(self._seulog, 'a')
		self._SeuTotal += val
		fo.write('\nSEUs: '+str(val)+'  \t\tfrom: %0.4f s\t\tto: %0.4f s\t\tfreq: %0.4f SEUs/s\t\ttotal: %f'%(self._prev_seuupdate, curtime, freq, self._SeuTotal))
		self._prev_seuupdate = curtime
		print 'Time: '+str(int(round(curtime)))+'  \tSEU counter: '+str(self._SeuCounter)+'  \tTotal: '+str(self._SeuTotal)
		fo.close()


	def DeltaTime(self, formatted=True):                
		if formatted: return str(datetime.timedelta(seconds=(time.time()-self._StartTime)))
		else: return  time.time()-self._StartTime
		
	def tx_operation(self): 
		self._TxOperationCounter += 1
		self._CurTxOperationCounter += 1
		if not self.child_conn.poll():
			self.parent_conn.send({'time':self.DeltaTime(False), 'id':0, 'op':self._CurTxOperationCounter})
			self._CurTxOperationCounter = 0
	
	def rx_operation(self): 
		self._RxOperationCounter += 1
		self._CurRxOperationCounter += 1
		if not self.child_conn.poll():
			self.parent_conn.send({'time':self.DeltaTime(False), 'id':1, 'op':self._CurRxOperationCounter})
			self._CurRxOperationCounter = 0
	
		
	def error(self, code, msg=''):
		self._ErCode = code
		self._ErrorCounter[code] += 1
		msg = '[X] time: '+self.DeltaTime(formatted=True)+', code: '+str(code)+', op: '+str(self._TxOperationCounter)+', err: '+str(self._ErrorCounter[code])+', \ttotal_err: '+str(sum(self._ErrorCounter))+', \t'+msg+'.'
		print msg
		self._logerrors(msg)	
		self._pltlog()

	def GetLasterror(self): return self._ErCode

