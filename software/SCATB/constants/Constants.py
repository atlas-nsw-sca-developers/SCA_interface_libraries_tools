# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

from re import findall
import numpy as np

####### TCP/IP components ###################
testsystem        = {'IP':"192.168.200.16", 'PORT':0,    'NAME':"GBT-SCA-TestSystem"} #if using Cactus-IPBUS
wave              = {'IP':"192.168.200.33", 'PORT':5025, 'NAME':"Agilent-33220A"}     #if using external waveform generator
Keitley           = {'IP':"192.168.200.32", 'PORT':5025, 'NAME':"Keitley-2450"}     #if using external SMU

######### Firmware Register Table ############
POWER_EN          = {'mode':'ctrl', 'REG':0, 'BIT':0, 'LEN':1} 
SCA_AUX_DIS       = {'mode':'ctrl', 'REG':1, 'BIT':0, 'LEN':1} 
SCA_TEST_EN       = {'mode':'ctrl', 'REG':1, 'BIT':1, 'LEN':1} 
SCA_RESETB        = {'mode':'ctrl', 'REG':1, 'BIT':2, 'LEN':1} 
SCA_FUSE_PROG     = {'mode':'ctrl', 'REG':3, 'BIT':0, 'LEN':1} 
PHSELSECTPRI      = {'mode':'ctrl', 'REG':1, 'BIT':3, 'LEN':1} 
PHSELSECTAUX      = {'mode':'ctrl', 'REG':1, 'BIT':4, 'LEN':1} 
EPORT_FREQ        = {'mode':'ctrl', 'REG':2, 'BIT':0, 'LEN':3}
	
PCB_CONTROL       = {'mode':'wb',   'BaseAdress': 0}
SCA_SERIAL_PORT   = {'mode':'wb',   'BaseAdress': 1}
SCA_EPORT_PRI     = {'mode':'wb',   'BaseAdress': 2}
SCA_EPORT_AUX     = {'mode':'wb',   'BaseAdress': 3}
SLV_GPIO          = {'mode':'wb',   'BaseAdress': 4}
SLV_SPI           = {'mode':'wb',   'BaseAdress': 5}
SLV_JTAG          = {'mode':'wb',   'BaseAdress': 6}
SCA_CTRL          = {'mode':'wb',   'BaseAdress': 7}
SLV_I2C0          = {'mode':'wb',   'BaseAdress': 8}
SLV_I2C1          = {'mode':'wb',   'BaseAdress': 9}
SLV_I2C2          = {'mode':'wb',   'BaseAdress': 10}
SLV_I2C3          = {'mode':'wb',   'BaseAdress': 11}
SLV_I2C4          = {'mode':'wb',   'BaseAdress': 12}
SLV_I2C5          = {'mode':'wb',   'BaseAdress': 13}
SLV_I2C6          = {'mode':'wb',   'BaseAdress': 14}
SLV_I2C7          = {'mode':'wb',   'BaseAdress': 15}
SLV_I2C8          = {'mode':'wb',   'BaseAdress': 16}
SLV_I2C9          = {'mode':'wb',   'BaseAdress': 17}
SLV_I2C10         = {'mode':'wb',   'BaseAdress': 18}
SLV_I2C11         = {'mode':'wb',   'BaseAdress': 19}
SLV_I2C12         = {'mode':'wb',   'BaseAdress': 20}
SLV_I2C13         = {'mode':'wb',   'BaseAdress': 21}
SLV_I2C14         = {'mode':'wb',   'BaseAdress': 22}
SLV_I2C15         = {'mode':'wb',   'BaseAdress': 23}

####### PCB control ##########################
DAC_REFERENCE = 1.2430 #V
DAC_OFFSET    = 0.002  #V
DAC_CODE_1V0  = int(round(65535/DAC_REFERENCE))

######### Board components address table ###
ADR_MEMORY                   = 0x50	            
ADR_AD5665R_SUPPLY           = 0x1f
ADR_MAX1239                  = 0b0110101
ADR_AD5665R_ADC0             = 0b0010000
ADR_AD5665R_ADC1             = 0b0010010
ADR_AD5665R_ADC2             = 0b0010100
ADR_AD5665R_ADC3             = 0b0011000
ADR_AD5665R_ADC4             = 0b0011010
ADR_AD5665R_ADC5             = 0b0011011
ADR_AD5665R_ADC6             = 0b0011100
ADR_AD5665R_ADC7             = 0b0011110

######### GBT-SCA Command Table ############
#CHANNEL CODE
CH_NODE 		       = 0x00 #0	   
CH_SPI  		       = 0x01 #1	   
CH_GPIO 		       = 0x02 #2	   
CH_I2C0 		       = 0x03 #3	   
CH_I2C1 		       = 0x04 #4	   
CH_I2C2 		       = 0x05 #5	   
CH_I2C3 		       = 0x06 #6	   
CH_I2C4 		       = 0x07 #7	   
CH_I2C5 		       = 0x08 #8	   
CH_I2C6 		       = 0x09 #9	   
CH_I2C7 		       = 0x0a #10	   
CH_I2C8 		       = 0x0b #11	   
CH_I2C9 		       = 0x0c #12	   
CH_I2C10		       = 0x0d #13	   
CH_I2C11		       = 0x0e #14	   
CH_I2C12		       = 0x0f #15	   
CH_I2C13		       = 0x10 #16	   
CH_I2C14		       = 0x11 #17	   
CH_I2C15		       = 0x12 #18	   
CH_JTAG 		       = 0x13 #19	   
CH_ADC  		       = 0x14 #20	   
CH_DAC  		       = 0x15 #21	   
#ERROR CODE
Err_bit                  = 0     
Inv_CH                   = 1     
Inv_Cmd                  = 2     
Inv_TR                   = 3     
Inv_LEN                  = 4     
CH_dis                   = 5     
CH_Busy                  = 6     
Num_Max_CH               = 0x15  	
#Node Ctrl
wb_nb_bit                = 31     
atlantic_nb_bit          = 15     
NC_Write_CRA             = 0x00     
NC_Read_CRA              = 0x01     
NC_Write_CRB             = 0x02     
NC_Read_CRB              = 0x03 
NC_Write_CRC             = 0x04     
NC_Read_CRC              = 0x05     
NC_Write_CRD             = 0x06     
NC_Read_CRD              = 0x07     
NC_Write_CRE             = 0x08     
NC_Read_CRE              = 0x09     
SEU_read                 = 0xb1 #on ch JTAG
SEU_clean                = 0xb0 #on ch JTAG
SCA_W_ChipID             = 0x90;#on ch ADC
SCA_R_ChipID             = 0x91;#on ch ADC
#SPI
SPI_GO                   = 0x72
SPI_W_MOSI_TX0                = 0x00     
SPI_R_MISO_RX0                = 0x01     
SPI_W_MOSI_TX1                = 0x10     
SPI_R_MISO_RX1                = 0x11     
SPI_W_MOSI_TX2                = 0x20     
SPI_R_MISO_RX2                = 0x21     
SPI_W_MOSI_TX3                = 0x30     
SPI_R_MISO_RX3                = 0x31     
SPI_W_ctrl               = 0x40     
SPI_R_ctrl               = 0x41     
SPI_go_ctrl              = 0x42     
SPI_W_DIV                = 0x50     
SPI_R_DIV                = 0x51     
SPI_W_SS                 = 0x60     
SPI_R_SS                 = 0x61     
SPI_CharLen_mask         = 0b0000000001111111
SPI_GO_mask              = 0b0000000100000000
SPI_RxNEG_mask           = 0b0000001000000000
SPI_TxNEG_mask           = 0b0000010000000000
SPI_LSB_mask             = 0b0000100000000000
SPI_IE_mask              = 0b0001000000000000
SPI_INVSCLK_mask         = 0b0000000010000000
SPI_ASS_mask             = 0b0010000000000000

#JTAG
JTAG_GO                   = 0xA2
JTAG_W_TDO_TX0            = 0x00     
JTAG_R_TDI_RX0            = 0x01     
JTAG_W_TDO_TX1            = 0x10     
JTAG_R_TDI_RX1            = 0x11     
JTAG_W_TDO_TX2            = 0x20     
JTAG_R_TDI_RX2            = 0x21     
JTAG_W_TDO_TX3            = 0x30     
JTAG_R_TDI_RX3            = 0x31     
JTAG_W_TMS_TX0            = 0x40     
JTAG_R_TMS_TX0            = 0x41     
JTAG_W_TMS_TX1            = 0x50     
JTAG_R_TMS_TX1            = 0x51     
JTAG_W_TMS_TX2            = 0x60     
JTAG_R_TMS_TX2            = 0x61     
JTAG_W_TMS_TX3            = 0x70     
JTAG_R_TMS_TX3            = 0x71     
JTAG_W_ctrl               = 0x80     
JTAG_R_ctrl               = 0x81     
JTAG_go_ctrl              = 0x82     
JTAG_W_DIV                = 0x90     
JTAG_R_DIV                = 0x91    
JTAG_CharLen_mask         = 0b0000000001111111
JTAG_GO_mask              = 0b0000000000000001
JTAG_RxNEG_mask           = 0b0000000000000010
JTAG_TxNEG_mask           = 0b0000000000000100
JTAG_LSB_mask             = 0b0000000000001000
JTAG_IE_mask              = 0b0000000000010000
JTAG_INVSCLK_mask         = 0b0000000001000000
JTAG_ASS_mask             = 0b0000000000100000  
JTAG_RES_mask             = 0b0000000100000000

JTG_CharLen_mask         = 0b00000000001111111
JTG_GO_mask              = 0b00000000100000000
JTG_RxNEG_mask           = 0b00000001000000000
JTG_TxNEG_mask           = 0b00000010000000000
JTG_LSB_mask             = 0b00000100000000000
JTG_IE_mask              = 0b00001000000000000
JTG_INVSCLK_mask         = 0b00100000000000000
JTG_ASS_mask             = 0b00010000000000000  
JTG_RES_mask             = 0b10000000000000000

#I2C
I2C_W_CTRL               = 0x30     
I2C_R_CTRL               = 0x31     
I2C_R_SRA                = 0x11     
I2C_W_MSK                = 0x20     
I2C_R_MSK                = 0x21
I2C_W_DATA0              = 0x40     
I2C_W_DATA1              = 0x50     
I2C_W_DATA2              = 0x60     
I2C_W_DATA3              = 0x70     
I2C_R_DATA0              = 0x41     
I2C_R_DATA1              = 0x51     
I2C_R_DATA2              = 0x61     
I2C_R_DATA3              = 0x71
I2C_SingleByte_7b_WRITE  = 0x82     
I2C_SingleByte_7b_READ   = 0x86     
I2C_SingleByte_10b_WRITE = 0x8A     
I2C_SingleByte_10b_READ  = 0x8E 
I2C_SingleByte_rmw_OR    = 0xC6     
I2C_SingleByte_rmw_XOR   = 0xCA
I2C_MultiByte_7b_WRITE   = 0xDA			     
I2C_MultiByte_7b_READ    = 0xDE	   
I2C_MultiByte_10b_WRITE	 = 0xE2	   
I2C_MultiByte_10b_READ	 = 0xE6
I2C_MASK_TRLEN           = 0b001111100
I2C_MASK_FREQ            = 0b000000011   
#GPIO
GPIO_R_DAT_IN		 = 0x01	   
GPIO_W_DAT_OUT  	       = 0x10	   
GPIO_R_DAT_OUT  	       = 0x11	   
GPIO_W_OE		       = 0x20	   
GPIO_R_OE		       = 0x21	   
GPIO_W_INTE		       = 0x30	   
GPIO_R_INTE		       = 0x31	   
GPIO_W_PTRIG		 = 0x40	   
GPIO_R_PTRIG		 = 0x41	   
GPIO_W_AUX		       = 0x50	   
GPIO_R_AUX		       = 0x51	   
GPIO_W_CTRL		       = 0x60	   
GPIO_R_CTRL		       = 0x61	   
GPIO_W_INTS		       = 0x70	   
GPIO_R_INTS		       = 0x71	   
GPIO_W_ECLK		       = 0x80	   
GPIO_R_ECLK		       = 0x81	   
GPIO_W_NEC		       = 0x90	   
GPIO_R_NEC		       = 0x91	   
#DAC
DAC_W_CTRL               = 0x00      
DAC_R_CTRL               = 0x01
DAC_W_OUT0               = 0x10      
DAC_R_OUT0               = 0x11
DAC_W_OUT1               = 0x20      
DAC_R_OUT1               = 0x21
DAC_W_OUT2               = 0x30      
DAC_R_OUT2               = 0x31
DAC_W_OUT3               = 0x40      
DAC_R_OUT3               = 0x41    
#ADC
ADC_GO                   = 0xB2 
ADC_W_InputLine          = 0x30 
ADC_R_InputLine          = 0x31 
ADC_W_CurrentSource      = 0x40 
ADC_R_CurrentSource      = 0x41
ADC_R_DATA_Ofs_Ga        = 0x21
ADC_R_DATA_Ofs           = 0xA1
ADC_R_DATA               = 0x51
ADC_R_OFFSET             = 0x61
ADC_W_CTRL               = 0x10
ADC_R_CTRL               = 0x11
ADC_GO_SingleSlope       = 0x02
ADC_W_GainCalibrReg      = 0x70
ADC_R_GainCalibrReg      = 0x71
ADC_W_ID                 = 0x90
ADC_R_ID                 = 0x91

######### SPI External Memory commands ############
SPIMEM_WREN              = 0x06 #set write enable latch
SPIMEM_WRDI              = 0x04 #reset write enable latch
SPIMEM_RDSR              = 0x05 #read status register
SPIMEM_WRSR              = 0x01 #write status register
SPIMEM_READ              = 0x03 #read data from memory array
SPIMEM_WRITE             = 0x02 #write data to memory array 


def unpack(val, order=''):
	a = findall('..','%.8x'%val)
	if order is 'inv':  return [int(a[0],16), int(a[1],16), int(a[2],16), int(a[3],16)]
	else:               return [int(a[3],16), int(a[2],16), int(a[1],16), int(a[0],16)]
	
def pack(a, b=0, c=0, d=0, order=''):
	if type(a) is list:
		if b is 'inv' or order is 'inv':  return int('%.2x'%a[3] + '%.2x'%a[2] + '%.2x'%a[1] + '%.2x'%a[0],16)  
		else: return int('%.2x'%a[0] + '%.2x'%a[1] + '%.2x'%a[2] + '%.2x'%a[3],16)	
	else:
		if order is 'inv': return int('%.2x'%d + '%.2x'%c + '%.2x'%b + '%.2x'%a,16)  
		else: return int('%.2x'%a + '%.2x'%b + '%.2x'%c + '%.2x'%d,16)	
	
def reord(val):
	reg = unpack(val)
	return pack(reg[0],reg[1],reg[2],reg[3])
	
	
def IsEnable(chid, eport):
	if chid < 8:
		pktr = eport.SendAndGetReply(CH_NODE , NC_Read_CRB)
		if pktr is False: return False
		elif ((pktr.VAL >> chid )  & 0x1) : return True
		else: return False
	elif chid < 16:
		pktr = eport.SendAndGetReply(CH_NODE , NC_Read_CRC)
		if pktr is False: return False
		elif ((pktr.VAL >> chid-8)  & 0x1): return True
		else: return False
	else:
		pktr = eport.SendAndGetReply(CH_NODE , NC_Read_CRD)
		if pktr is False: return False
		elif ((pktr.VAL >> chid-16) & 0x1): return True
		else: return False


def Enable(chid, eport):
	if chid < 8:
		pktr = eport.SendAndGetReply(CH_NODE , NC_Read_CRB)
		if pktr is False: return False
		else: 
			eport.SendAndGetReply(CH_NODE , NC_Write_CRB, (pktr.VAL&0xff) | (0x01 << chid))
			return True
	elif chid < 16:
		pktr = eport.SendAndGetReply(CH_NODE , NC_Read_CRC)
		if pktr is False: return False
		else: 
			eport.SendAndGetReply(CH_NODE , NC_Write_CRC, (pktr.VAL&0xff) | (0x01 << (chid-8)))
			return True
	else:
		pktr = eport.SendAndGetReply(CH_NODE , NC_Read_CRD)
		if pktr is False: return False
		else: 
			eport.SendAndGetReply(CH_NODE , NC_Write_CRD, (pktr.VAL&0xff) | (0x01 << (chid-16)))
			return True
			
def Disable(chid, eport):
	if chid < 8:
		pktr = eport.SendAndGetReply(CH_NODE , NC_Read_CRB)
		if pktr is False: return False
		else: 
			eport.SendAndGetReply(CH_NODE , NC_Write_CRB, (pktr.VAL&0xff) & (~(0x01 << chid)))
			return True
	elif chid < 16:
		pktr = eport.SendAndGetReply(CH_NODE , NC_Read_CRC)
		if pktr is False: return False
		else: 
			eport.SendAndGetReply(CH_NODE , NC_Write_CRC, (pktr.VAL&0xff) & (~(0x01 << (chid-8))))
			return True
	else:
		pktr = eport.SendAndGetReply(CH_NODE , NC_Read_CRD)
		if pktr is False: return False
		else: 
			eport.SendAndGetReply(CH_NODE , NC_Write_CRD, (pktr.VAL&0xff) & (~(0x01 << (chid-16))))
			return True








