
set style histogram rows
set xrange [-1:23]
set yrange [0:200]
set autoscale ymax   

set style fill solid
set xtic ("EPORT" 22, "COM" 0, "SPI"   1,"GPIO"  2,"I2C0"  3,"I2C1" 4, "I2C2"5, "I2C3"  6,"I2C4"  7, "I2C5"  8,"I2C6"  9,"I2C7" 10,"I2C8" 11,"I2C9"12,"I2C10"13,"I2C11"14, "I2C12"15,"I2C13"16,"I2C14"17,"I2C15"18,"JTAG"19,"ADC"  20,"DAC"  21)	
set xtic rotate by -75 scale 0
set boxwidth 0.9

plot "software/tmp/erplt.log" using 1:2 with boxes

pause 1
reread
   
