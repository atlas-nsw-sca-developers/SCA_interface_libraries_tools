# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

from   ..constants import *
import random 
import numpy as np
import matplotlib.pyplot as plt
import time
import datetime
import os


class test_routines:
	def __init__(self, COM, PCB, SCA, SLV, WVF, SMU, EH):
		self._debug    = False
		self._COM      = COM
		self._PCB      = PCB
		self._SCA      = SCA
		self._SLV      = SLV
		self._EH       = EH

	#include here test routines
