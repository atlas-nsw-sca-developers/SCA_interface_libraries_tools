set terminal png size 1000,1000
set output 'results/dnl_inl_fit.png'
set grid
set yr [0:1024*5]
set ylabel 'ADC Code'
set ytic 512
set multiplot
set xr [0.0:1.0]

f(x)=a*x+b
set format x ""
fit f(x) 'results/out.dat' u 1:2 via a,b
set size 1,0.3
set origin 0,0.66
set format y "%4.0f"
set key left 
set bmar 0
set tmar 1
plot 'results/out.dat' u 1:2  t 'data',  f(x) t 'fit' 

set tmar 1
set size 1,0.3
set origin 0,0.33
#set autoscale
set yr [-5:6]
set ytic 1
set ylabe "distance [LSB]"
set format y "%4.1f"
plot 'results/out.dat' u 1:(-$2+f($1)) pt 7 ps 1 lc 3  

set size 1,0.3
set xlabel 'Vin [V]'
set format x "%.1f"
set origin 0,0
#set autoscale
set yr [0:10.0]
set ytic 0.5
set tmar 1
set bmar 3
set ylabel 'RMS [LSB]'
plot 'results/out.dat' u 1:3 t '' lc 4

unset multiplot












