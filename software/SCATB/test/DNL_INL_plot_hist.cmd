set terminal png size 1000,1000
set output 'results/dnl_inl.png'
set grid
set multiplot
set xr [0:1024*4]
set xtic 512
set xtics font ",10" 

set format x "%.0f"
set size 1,0.3
set origin 0,0.33
set yr [-1:1]
set bmar 0 
set ytic 0.2 
set ylabel 'DNL [LSB]'
plot 'results/dnlinl.dat' u 1:2 lc 1  t '' 

set format x "%.0f"
set size 1,0.3
set origin 0,0.0
set yr [-1.0:4.0]
set ylabel 'INL [LSB]'
set xlabel 'ADC Code'
set bmar 3
set tmar 1
set ytic 0.5  
plot 'results/dnlinl.dat' u 1:3 lc 4 t '' 

set format x "%.0f"
set size 1,0.3
set origin 0,0.65
set yr [1000:3500]
#set autoscale
set ylabel 'HIST [LSB]'
set xlabel 'ADC Code'
set bmar 3
set tmar 1
set ytic 1000 
set style fill solid
set boxwidth 1
plot 'results/dnlinl.dat' u 1:4 with boxes lc 3 t '' 

unset multiplot
###!display adc.png


