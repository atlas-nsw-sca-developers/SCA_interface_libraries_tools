# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

from constants import *
from usb       import *
from com       import *
from board     import *
from SCAclass  import *
from test      import *
from waveform  import *
import         time
from multiprocessing import Process, Pipe

def close(arg=0):
	#PCB.SupplyDisable()
	print '->  Closing SCA test system ...'
	try: 
		EH.close()
	except:
		pass
	#SEUproc.terminate()
	COM.close()
	exit(arg)
print ''
EH    = ErrHandle()	
COM = usbcom('Genesys')
try: COM
except NameError: exit(1)
if not COM.connected: close(1)
PCB   = boardctrl(COM)
#if not PCB.isEnable: close()	
SCA   = SCActrl(COM, EH)
SLV   = interfaces(COM, PCB)
WVF   = waveformgenerator( wave['IP'], wave['PORT'], wave['NAME'] )
SMU   = Keitley2410(Keitley['IP'], Keitley['PORT'], Keitley['NAME'] )
test  = test_routines(COM, PCB, SCA, SLV, WVF, SMU, EH)
PCB.SupplyEnable()
SCA.Connect('pri')
print ''



