# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 
import socket
import time

class Keitley2410:

	def __init__(self, IP, PORT=5025, name='', limitation=1.5):
		self.present = False
		self._limit = limitation
		self.Connect(IP, PORT, name)
		if self.present: 
			self.SetCurLimit(0.020)
			self.SetVoltage(0)
			self.OutputEnable()
	
	def Connect(self, IP, PORT=5025, name=''):
		self.s = socket.socket()
		self.s.settimeout(1)
		if self.s.connect_ex((IP, PORT)):
			print '-x  Impossible to establish the connection with '+str(name)+'on adress '+str(IP)
			self.present = False
		else: 
			print '->  Communication established with '+str(name)+' on adress '+str(IP)
			self.present = True
			#self.s.send("*CLS;*RST\n")
	
	def _isonline(self): 
		if not self.present: print  '-x  Impossible to communicate with Keitley-2410, check the local network.'
		return self.present 	
		
	def Send(self, cmd):     
		if not self._isonline(): return False 
		self.s.send(cmd+"\n")	
	
	def Receive(self):   
	  	if not self._isonline(): return False   
		return self.s.recv(256)
		
	def SetVoltage(self, val): 
		if val > self._limit: 
			print '->  SMU output voltage value required exceed limitation\n    use SMU.SetVoltLimit to change it'
		else:return self.Send('SOUR:VOLT '+str(val))
			
	def GetVoltage(self) :
		self.Send('SOUR:VOLT?')
		return self.Receive()
	
	def SetCurLimit(self, val):
		return self.Send('SOUR:VOLT:ILIM '+str(val))
		
	def SetVoltLimit(self, val):
		self._limit = val
	
	def OutputEnable(self): self.Send('OUTPUT ON')
	
	def OutputDisable(self): self.Send('OUTPUT OFF')
	
	
	
	
