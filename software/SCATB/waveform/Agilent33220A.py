# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

import socket
import time

class waveformgenerator:
	def __init__(self, IP, PORT=5025, name='', limitation=1):
		self.present = False
		self._limit = limitation
		self.Connect(IP, PORT, name)
		self.dcconf = False
		if self.present:
			self.SetLoadImpedence(50)
			self._SetMODE('DC')
			self.SetDC(0) 
			self.SetOutput(True)
	
	def _isonline(self): 
		if not self.present: 
			print  '-x  Impossible to communicate with a waveform generator, check the local network.'
		return self.present 
		
	def Connect(self, IP, PORT=5025, name=''):
		self.s = socket.socket()
		self.s.settimeout(1)
		if self.s.connect_ex((IP, PORT)):
			print '-x  Impossible to establish the connection with '+str(name)+' waveform generator on adress '+str(IP)
			self.present = False
		else: 
			print '->  Communication established with '+str(name)+' waveform generator on adress '+str(IP)
			self.present = True
			self.s.send("*CLS;*RST\n")
		
	def Reset(self):        
		if not self._isonline(): return False 
		self.dcconf = False
		self.s.send("*CLS;*RST\n")
		self.SetOutput()
		
	def _Send(self, cmd):     
		self.dcconf = False
		if not self._isonline(): return False 
		self.s.send(cmd+"\n")
		
	def _Receive(self):   
	  	if not self._isonline(): return False   
		return self.s.recv(256)
		
	def _SetMODE(self, mode): 
		self.dcconf = False
		if not self._isonline(): return False 
		self.s.send("FUNCtion "+mode+"\n")
	
	def SetLoadImpedence(self, val='INFinity'):
		if not self._isonline(): return False 
		if   type(val) is int: self.s.send("OUTPut:LOAD "+str(val)+"\n")
		elif type(val) is str: self.s.send("OUTPut:LOAD "+val+"\n")
	
	def SetOutput(self, EN=True):
		if not self._isonline(): return False
		if EN: self.s.send("OUTPut ON\n")
		else:  self.s.send("OUTPut OFF\n")
		self.OutEN = EN
	
	def SetDC(self, val): 
		self.dcconf = False
		if not self._isonline(): return False
		if val>self._limit:
			print '->  Waveform Generator DC value required exceede the limitation'
			val = self._limit
		self.s.send("VOLTage:OFFSet "+str(val)+"\n")
		
		
	def SetDAC(self, val):
		if not self._isonline(): return False
		if not self.dcconf:
			self.s.send("FUNCtion USER\n")
			self.s.send("FUNCtion:USER VOLATILE\n")
			self.s.send("VOLTage 1\n")
			self.s.send("VOLTage:OFFSet 0.5\n")
			self.dcconf = True
		#self.s.send("OUTPut OFF\n")
		self.s.send("DATA:DAC VOLATILE, "+str(val-8191)+"\n")
		#self.s.send("OUTPut ON\n")
		#time.sleep(0.4)
	
	def GetDC(self):  
		if not self._isonline(): return False
		self.s.send("VOLTage:OFFSet?\n")
		return self.s.recv(256)
	
	def SetWaveform(self, func, freq=1, offset=0.5, vpp=1.0):
		self.dcconf = False
		if not self._isonline(): return False
		self.s.send("OUTPut OFF\n")
		if func is 'ramp':     	
			self.s.send("VOLTage:OFFSet 0\n")
			self.s.send('FUNCtion RAMP\n')
			self.s.send('VOLTage:LOW '  +str(float(offset))+'\n')
			self.s.send('VOLTage:HIGH ' +str(float(vpp)-offset)+'\n')
		else: 
			if   func is 'triangle': cmd='TRIangle'
			elif func is 'sin':      cmd='SINusoid'
			elif func is 'square':   cmd='SQUare'
			elif func is 'pulse':    cmd='PULSe'
			elif func is 'noise':    cmd='NOISe'
			elif func is 'dc':       cmd='DC'
			self.s.send('FUNCtion '       +cmd         +'\n')
			self.s.send("VOLTage:OFFSet " +str(offset) +"\n")
			self.s.send("VOLTage "        +str(vpp)    +"\n")
		self.s.send("FREQuency "      +str(freq)   +"\n")
		self.s.send("OUTPut ON\n")
		
		
		
