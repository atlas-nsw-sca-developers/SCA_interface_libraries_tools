# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

from GPIOslave   import *
from I2Cslave    import * 
from JTAGslave   import * 
from SPIslave    import *
from ..constants import *

class interfaces:
	def __init__(self,COM, PCB):
		self.GPIO    = GPIOslave(COM, SLV_GPIO['BaseAdress'])
		self.JTAG    = JTAGslave(COM, SLV_JTAG['BaseAdress'])
		self.SPI     = SPIslave( COM, SLV_SPI['BaseAdress'])
		self.ADC     = PCB.DACarray
		self.DAC     = PCB.ADC
		self.I2C     = [None]*16
		self.I2C[0]  = I2Cslave(COM, 8)
		self.I2C[1]  = I2Cslave(COM, 9)
		self.I2C[2]  = I2Cslave(COM, 10)
		self.I2C[3]  = I2Cslave(COM, 11)
		self.I2C[4]  = I2Cslave(COM, 12)
		self.I2C[5]  = I2Cslave(COM, 13)
		self.I2C[6]  = I2Cslave(COM, 14)
		self.I2C[7]  = I2Cslave(COM, 15)
		self.I2C[8]  = I2Cslave(COM, 16)
		self.I2C[9]  = I2Cslave(COM, 17)
		self.I2C[10] = I2Cslave(COM, 18)
		self.I2C[11] = I2Cslave(COM, 19)
		self.I2C[12] = I2Cslave(COM, 20)
		self.I2C[13] = I2Cslave(COM, 21)
		self.I2C[14] = I2Cslave(COM, 22)
		self.I2C[15] = I2Cslave(COM, 23)
	
