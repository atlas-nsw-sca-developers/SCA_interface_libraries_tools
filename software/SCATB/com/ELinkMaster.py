# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

import os
import time
from re import findall
from ..constants import *

class scapkt:
	def __init__(self):
		self.TID = 0
		self.LEN = 0
		self.CHN = 0
		self.CMD = 0
		self.ERR = 0
		self.VAL = 0
		
class ELinkMaster:
	
	_mmtab    = {'control':0x00, 'status':0x01, 'sendcmd':0x02, 'senddata':0x03, 'reccmd':0x02, 'recdata':0x03, 'enable':0x6}
	_command  = {'connect':0x1a, 'reset' :0x1b, 'loopback':0x1c, 'send':0x10}
	
	def __init__(self, com, deviceid, EH, debugmode=False):
		self._com    = com;
		self._wrt    = self._com.wbwrite
		self._wrtf   = self._com.wbwritefast
		self._rea    = self._com.wbread
		self._reaf   = self._com.wbreadfast
		self._flag   = self._com.wbgetint 
		self._len    = 4
		self._EH     = EH
		self.ID      = deviceid
		self.trid    = 0
		self.debug   = debugmode
		self.timeout = 50
		self._makeerror = 0

	def _inctrid(self):
		self.trid = 1 if self.trid>253 else self.trid+1
		return self.trid
	
	def _getflag(self):
		flag = (self._flag() >> self.ID) & 0b1
		if flag is 1:
			self._reaf(self.ID, self._mmtab['status'], None) #clean interrupt
			return True
		else:
			return False
	
	def pack(self, a, b=0, c=0, d=0, order=False):
		return int('%.2x'%a + '%.2x'%b + '%.2x'%c + '%.2x'%d) if order else int('%.2x'%d + '%.2x'%c + '%.2x'%b + '%.2x'%a)
	
	def unpack(self, val):
		a = findall('..',hex(val))
		return int(a[1],16), int(a[2],16), int(a[3],16), int(a[4],16)
		
	
	def DebugMode(self, debugmode=True):
		self.debug = debugmode 
	
	def connect(self):
		self._wrt(self.ID, self._mmtab['control'], self._command['connect'])
		ack = self._rea(self.ID, self._mmtab['control'])
		return True if ack is 0x01 else False
		
	def reset(self):
		self._wrt(self.ID, self._mmtab['control'], self._command['reset'])
		ack = self._rea(self.ID, self._mmtab['control'])
		return True if ack is 0x01 else False
	
	def enable(self):    self._wrt(self.ID, self._mmtab['enable'], 1)
	def disable(self):   self._wrt(self.ID, self._mmtab['enable'], 0)
	def isenable(self):  self._rea(self.ID, self._mmtab['enable'])

	def send(self, channel, command, data=0x0):
		self._makeerror += 1
		pkt = scapkt()
		self._inctrid()
		word =  ((self._len)<<24) | ((command&0xff)<<16) | ((self.trid)<<8) | (channel&0xff)
		self._wrtf(self.ID, self._mmtab['sendcmd'],  word)
		if data is not None: 
			self._wrtf(self.ID, self._mmtab['senddata'], data)

#		if self._makeerror == 100:
#			self._makeerror =0
#		else:		
#			self._wrtf(self.ID, self._mmtab['control'],  self._command['send'],1)

		self._wrtf(self.ID, self._mmtab['control'],  self._command['send'],1)

		#ack = self._rea(self.ID, self._mmtab['control'])
		#while ack is not 0x1:
		#	ack = self._rea(self.ID, self._mmtab['control'])
		#to speed up we suppose that is acknoledged alwways by the FPGA 2015-03-14
		if self.debug is True:
			print 'Sent command:\n-channel:\t'+hex(channel)+'\n-ID:     \t'+hex(self.trid)+'\n-command:\t'+hex(command)+'\n-data:   \t'+hex(data)
		pkt.VAL = data			
		pkt.LEN = self._len 
		pkt.TID = self.trid 
		pkt.CHN = channel  &0xff
		pkt.CMD = command  &0xff
		return pkt
		
		
	def receive(self, mode=0):
		tm=time.time()
		cnt  = 0
		flag = self._getflag()
		while ((mode==1 or mode==2) and flag is False and cnt < self.timeout):
			flag = self._getflag()
			cnt += 1

		if flag is False:
			#if mode is 1: print '[X] timeout exceded in receiving paket from SCA'
			#self._EH.error(22, 'COM - timeout exceded in receiving paket from SCA')
			return False
		else:
			pkt = scapkt()
			word =  self._reaf(self.ID, self._mmtab['reccmd'])
			pkt.VAL = self._reaf(self.ID, self._mmtab['recdata'])
			pkt.ERR = (word>>24) & 0xff
			pkt.TID = (word>>8)  & 0xff
			pkt.CHN = word       & 0xff
			pkt.LEN = (word>>16) & 0xff
			#if self._makeerror in range(100,110):
			#	pkt.ERR = 1<<CH_Busy |1
			#	print 'caiooooo'
				
			return pkt
		
		
		
		
		
		
		
		
		
		
		
		
			
			
		
