# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

from ..constants  import *

class I2Cslave:
	def __init__(self, com, deviceid):
		self._com      = com;
		self._regwrite = self._com.wbwritefast
		self._regread  = self._com.wbreadfast
		self.ID        = deviceid
		self.adr       = None
		self._regtable = {'ctrl':0,'status':1, 'dout':2, 'din':18, 'adr':35}
	
	def GetStatus(self): return self._regread(self.ID, self._regtable['status'])
	
	def SetAdr(self, adr): 
		if adr != self.adr:
			self._regwrite(self.ID, self._regtable['adr'], adr)

	def GetAdr(self): return self._regread(self.ID, self._regtable['adr'])
	
	def CheckReceived(self):
		rec = self._regread(self.ID, self._regtable['ctrl'])
		if rec:
			self._regwrite(self.ID, self._regtable['ctrl'], 0)
			return True
		else:
			return False
	
	def GetReceived(self, nbyte=16):
		rec = []
		for i in range(0,nbyte):
			rec.append(self._regread(self.ID, self._regtable['din'] + i ) )
		return rec
	
	def SetSend(self, data):
		nbyte = 16 if len(data)>16 else len(data)
		for i in range(0,nbyte):
			self._regwrite( self.ID, self._regtable['dout']+(15-i), data[i] )
					
	def GetSend(self):
		rec = []
		for i in range(0,16):
			rec.append(self._regread(self.ID, self._regtable['dout'] + i ) )
		return rec

