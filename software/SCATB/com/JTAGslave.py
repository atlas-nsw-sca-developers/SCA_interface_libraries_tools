# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

from ..constants import *

class JTAGslave:
	def __init__(self, com, deviceid):
		self._com      = com;
		self._regwrite = self._com.wbwritefast
		self._regread  = self._com.wbreadfast
		self.ID        = deviceid
		self._regtable = {'ctrl':0,'status':1, 'TDO':2, 'TDI':10, 'TMS':6, 'EN':14 }
		self.Reset()
	
	def GetStatus(self):
		return self._regread(self.ID, self._regtable['status'])
	
	def GetReceived(self, nbyte=16, direction='lsb'):
		TDI = []
		TMS = []
		TDI.extend( unpack( self._regread(self.ID, self._regtable['TDI']) ) )
		TMS.extend( unpack( self._regread(self.ID, self._regtable['TMS']) ) )
		TDI.extend( unpack( self._regread(self.ID, self._regtable['TDI']+1) ) )
		TMS.extend( unpack( self._regread(self.ID, self._regtable['TMS']+1) ) )
		TDI.extend( unpack( self._regread(self.ID, self._regtable['TDI']+2) ) )
		TMS.extend( unpack( self._regread(self.ID, self._regtable['TMS']+2) ) ) 
		TDI.extend( unpack( self._regread(self.ID, self._regtable['TDI']+3) ) )
		TMS.extend( unpack( self._regread(self.ID, self._regtable['TMS']+3) ) )		
		if   direction is 'lsb': return TDI[0:nbyte],     TMS[0:nbyte]		
		elif direction is 'msb': return TDI[16-nbyte:16], TMS[16-nbyte:16]
	
	def SetSend(self, TDO, direction):
		nbyte = 16 if len(TDO)>16 else len(TDO)
		if direction is 'lsb':
			for i in range(0, 16-nbyte): TDO.append(0x0)
			self._regwrite( self.ID, self._regtable['TDO']+3, pack(TDO[0:4]) )
			self._regwrite( self.ID, self._regtable['TDO']+2, pack(TDO[4:8]))
			self._regwrite( self.ID, self._regtable['TDO']+1, pack(TDO[8:12]))
			self._regwrite( self.ID, self._regtable['TDO']+0, pack(TDO[12:16]))
		elif direction is 'msb':
			TDO = TDO[::-1]
			for i in range(0, 16-nbyte): TDO.append(0x0)			
			self._regwrite( self.ID, self._regtable['TDO']+0, pack(TDO[0:4], 'inv') )
			self._regwrite( self.ID, self._regtable['TDO']+1, pack(TDO[4:8], 'inv'))
			self._regwrite( self.ID, self._regtable['TDO']+2, pack(TDO[8:12], 'inv'))
			self._regwrite( self.ID, self._regtable['TDO']+3, pack(TDO[12:16], 'inv'))
	
	def GetSend(self):
		TDO = []
		for i in range(0, 4): 
			TDO.extend( unpack(self._regread( self.ID, self._regtable['TDO']+i )) )
		return TDO

			
	def SetCtrl(self, MsbLsb, TxEdge, RxEdge):
		self._regwrite( self.ID, self._regtable['ctrl'], (MsbLsb<<2)|(RxEdge<<1)|(TxEdge))  
	
	def GetCtrl(self):
		return self._regread( self.ID, self._regtable['ctrl'])
	
	def Reset(self):
		self._regwrite( self.ID, self._regtable['EN'], 1)
		self._regwrite( self.ID, self._regtable['EN'], 0)
		
