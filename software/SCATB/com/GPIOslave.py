# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

from ..constants  import *

class GPIOslave:
	def __init__(self, com, deviceid):
		self._com      = com;
		self._regwrite = self._com.wbwritefast
		self._regread  = self._com.wbreadfast
		self.ID        = deviceid
		self._regtable = {'ctrl':0x0,'status':0x1,'dataout':0x2,'datain':0x3}
		
	def SetDirection(self, direction):
		#val = 1 if direction is 1 or direction is 'out' else 0x0
		self._regwrite(self.ID, self._regtable['ctrl'], direction)
		
	def GetDirection(self):
		return self._regread(self.ID, self._regtable['ctrl'])
		
	def SetOutput(self, val):
		self._regwrite(self.ID, self._regtable['dataout'], val)
	
	def GetOutput(self):
		return self._regread(self.ID, self._regtable['dataout'])
		
	def GetInput(self):
		return self._regread(self.ID, self._regtable['datain'])
		
