# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

from ..constants import *

class SPIslave:
	def __init__(self, com, deviceid):
		self._com      = com;
		self._regwrite = self._com.wbwritefast
		self._regread  = self._com.wbreadfast
		self.ID        = deviceid
		self._regtable = {'ctrl':0,'status':1, 'MISO':2, 'MOSI':6, 'EN':10 }
		self.Reset()
	
	def GetStatus(self):
		return self._regread(self.ID, self._regtable['status'])
	
	def GetReceived(self, nbyte=16, direction='lsb'):
		MOSI = []
		MOSI.extend( unpack( self._regread(self.ID, self._regtable['MOSI']) ) )
		MOSI.extend( unpack( self._regread(self.ID, self._regtable['MOSI']+1) ) )
		MOSI.extend( unpack( self._regread(self.ID, self._regtable['MOSI']+2) ) )
		MOSI.extend( unpack( self._regread(self.ID, self._regtable['MOSI']+3) ) )	
		if   direction is 'lsb': return MOSI[0:nbyte]
		elif direction is 'msb': return MOSI[16-nbyte:16]
	
	def SetSend(self, MISO, direction):
		nbyte = 16 if len(MISO)>16 else len(MISO)
		if direction is 'lsb':
			for i in range(0, 16-nbyte): MISO.append(0x0)
			self._regwrite( self.ID, self._regtable['MISO']+3, pack(MISO[0:4]) )
			self._regwrite( self.ID, self._regtable['MISO']+2, pack(MISO[4:8]))
			self._regwrite( self.ID, self._regtable['MISO']+1, pack(MISO[8:12]))
			self._regwrite( self.ID, self._regtable['MISO']+0, pack(MISO[12:16]))
		elif direction is 'msb':
			MISO = MISO[::-1]
			for i in range(0, 16-nbyte): MISO.append(0x0)			
			self._regwrite( self.ID, self._regtable['MISO']+0, pack(MISO[0:4], 'inv') )
			self._regwrite( self.ID, self._regtable['MISO']+1, pack(MISO[4:8], 'inv'))
			self._regwrite( self.ID, self._regtable['MISO']+2, pack(MISO[8:12], 'inv'))
			self._regwrite( self.ID, self._regtable['MISO']+3, pack(MISO[12:16], 'inv'))
	
	def GetSend(self):
		MISO = []
		for i in range(0, 4): 
			MISO.extend( unpack(self._regread( self.ID, self._regtable['MISO']+i )) )
		return MISO

			
	def SetCtrl(self, MsbLsb, mode):
		self._regwrite( self.ID, self._regtable['ctrl'], (MsbLsb<<2)|((mode&1)<<1) | ((mode&2)>>1) )  
	
	def GetCtrl(self):
		return self._regread( self.ID, self._regtable['ctrl'])
	
	def Reset(self):
		self._regwrite( self.ID, self._regtable['EN'], 1)
		self._regwrite( self.ID, self._regtable['EN'], 0)
		
