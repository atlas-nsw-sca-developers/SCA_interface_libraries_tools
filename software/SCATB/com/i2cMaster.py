# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

from ..constants  import *

class i2cMaster:
	def __init__(self, com, deviceid):
		self._com  = com;
		self._regwrite = self._com.wbwrite
		self._regread  = self._com.wbread
		self.deviceid  = deviceid
		self.timeout   = 10
		
	_freqcode = {'fast':0b11,'midfast':0b10,'midslow':0b01,'slow':0b00}
	_regtable = {'control':0xc,'gener':0x0,'status':0x4,'data0':0x10,'data1':0x14,'data2':0x18,'data3':0x1c}
	
	def _clr_regs(self):
		self._regwrite(self.deviceid, self._regtable['data0'], 0)
		self._regwrite(self.deviceid, self._regtable['data1'], 0)
		self._regwrite(self.deviceid, self._regtable['data2'], 0)
		self._regwrite(self.deviceid, self._regtable['data3'], 0)
		 
	def _wait_ack(self):
		i = self._com.wbgetint() & (1<<self.deviceid)
		cnt = self.timeout
		while i is 0:
			cnt = cnt-1
			i = self._com.wbgetint() & (1<<self.deviceid)
			if cnt is 0:
				print '->  PCB control - I2C write error' 
				return False
		return ((self._regread(self.deviceid, self._regtable['status'])>>24) is 0x04)
		
	def write(self, adr, data, speed = 'slow'):
		if len(data)<16:
			nbyte    = len(data)
		else:
			nbyte    = 15
		ack = self._regwrite(self.deviceid,  self._regtable['control'], ( ((nbyte & 0b1111) << 2) | (self._freqcode[speed] & 0b11) ) << 24 )
		if ack is False: return False
		for i in range(0,16-nbyte):
			data.append(0)
		self._regwrite(self.deviceid, self._regtable['data0'],  data[3]  | (data[2] <<8) | (data[1] <<16) | (data[0] <<24))
		self._regwrite(self.deviceid, self._regtable['data1'],  data[7]  | (data[6] <<8) | (data[5] <<16) | (data[4] <<24))
		self._regwrite(self.deviceid, self._regtable['data2'],  data[11] | (data[10]<<8) | (data[9] <<16) | (data[8] <<24))
		self._regwrite(self.deviceid, self._regtable['data3'],  data[15] | (data[14]<<8) | (data[13]<<16) | (data[12]<<24))
		self._regwrite(self.deviceid, self._regtable['gener'],  0x16000000 | (adr & 0x7f) << 16 )
		ack = self._wait_ack()
		if not ack: print '-x  PCB control - Component not found on the bus'
		return ack
		
	
	def read(self, adr, nbyte=1, speed = 'slow'):
		self._clr_regs()
		self._regwrite(self.deviceid,  self._regtable['control'], ( ((nbyte & 0b1111) << 2) | (self._freqcode[speed] & 0b11) ) << 24 )
		self._regwrite(self.deviceid, self._regtable['gener'],  0x17000000 | (adr & 0x7f) << 16 )
		if self._wait_ack() is False:
			return False
		val = []
		data = self._regread(self.deviceid, self._regtable['data0'])
		for i in range(1,min(4,nbyte+1)):
			val.append((data>>(8*i))&0xff)
		if nbyte>4:
			data = self._regread(self.deviceid, self._regtable['data1'])
			for i in range(0,min(4,nbyte+1)):
				val.append((data>>(8*i))&0xff)
		if nbyte>4:
			data = self._regread(self.deviceid, self._regtable['data2'])
			for i in range(0,min(4,nbyte+1)):
				val.append((data>>(8*i))&0xff)		
		if nbyte>4:
			data = self._regread(self.deviceid, self._regtable['data3'])
			for i in range(0,min(4,nbyte+1)):
				val.append((data>>(8*i))&0xff)	
		if len(val) is 1:
			return val[0]
		else:
			return val


