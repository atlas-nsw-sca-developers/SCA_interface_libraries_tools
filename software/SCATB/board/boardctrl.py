# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 

import sys 
import os
import getopt
import unittest
import time
import logging
from   ..constants import *
from   ..          import *
from   devices     import *
from   random      import randint

class boardctrl:
	Vmax = 3.291	
	Vmin = 1.169
	Xmax = 0xffff
	address = { 'dacpwr'   : 0x1f}
	def __init__(self, ComInterface):
		self.isEnable  = False
		self._com      = ComInterface
		self.I2C       = i2cMaster(ComInterface,0)
		self._memory   = MEM24LC256(ADR_MEMORY, self.I2C)
		self._dacpower = AD5665R(ADR_AD5665R_SUPPLY, self.I2C)
		self.ADC       = MAX1239(ADR_MAX1239, self.I2C)
		self.INA_VDD   = INA226(0x40, self.I2C)
		self.INA_AVDD  = INA226(0x41, self.I2C)
		self.INA_DVDD  = INA226(0x44, self.I2C)
		self.DACarray  = AD5665R_array([ADR_AD5665R_ADC0, ADR_AD5665R_ADC1, 
		                                ADR_AD5665R_ADC2, ADR_AD5665R_ADC3, 
		                                ADR_AD5665R_ADC4, ADR_AD5665R_ADC5, 
		                                ADR_AD5665R_ADC6, ADR_AD5665R_ADC7], 
		                                self.I2C,DAC_OFFSET,DAC_REFERENCE)
		                                
		if self.CheckBoardCommunication() is True:
			print '->  Communication established with GBT-SCA interface board'
			self.isEnable = True
		else: print '[X] Impossible to communicate with the interface board'
		
	def CheckBoardCommunication(self):
		return self._memory.CheckCommunication()
		
	def MemoryWrite(self, adr, val):
		return self._memory.write(adr, val)
	
	def MemoryRead(self, adr, nbytes=1):
		return self._memory.read(adr, nbytes)
		
	def SupplyEnable(self, V=58368): #64890 for 1.2 volt
		#Vmax = 3.291	
		#Vmin = 1.169
		#Xmax = 0xffff
		#K   = Xmax * (Vmax * Vmin) / (Vmax - Vmin)
		#val = K * (1/V - 1/Vmax)
		done = self._dacpower.SetValue(0,V)          #Core VDD
		done = self._dacpower.SetValue(1,V) and done #Analog VDD
		done = self._dacpower.SetValue(2,V) and done #Periphery VDD
		if not done:
			print '[X] Error in enabling the power supplyes'
			return False
		else:
			self._com.setctrl(POWER_EN['REG'], POWER_EN['BIT'], True)
			print '->  Power supplies enabled'
			return True
		
	def SupplyDisable(self, on=True):
		self._com.setctrl(POWER_EN['REG'], POWER_EN['BIT'], False)
		print '->  Power supplies disabled'
	
		
		
		
	    
	    
	    
	    
	    
	    
	    
	    
