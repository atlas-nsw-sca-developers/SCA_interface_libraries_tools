# Alessandro Caratelli <alessandro.caratelli@cern.ch> CERN.
# This project is distributed
# WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY,
# SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. 
from random import randint
import numpy as np

class MEM24LC256:
	def __init__(self, i2c_adr, MasterI2C):
		self._i2c = MasterI2C
		self._adr = i2c_adr
		
	def write(self, adr, val):
		return self._i2c.write( self._adr, [(adr>>8)&0xff, adr&0xff, val])
		
	def read(self, adr, nbytes=1):
		self._i2c.write(self._adr, [(adr>>8)&0xff, adr&0xff])
		return self._i2c.read(self._adr, nbytes)
		
	def CheckCommunication(self):
		adr = randint(2,127)
		val = randint(1,256)
		flag = self.write( adr, val)
		if flag is True:
			flag = self.read(adr)
			return True if flag is val else False
		else:
			return False
		
class AD5665R:
	def __init__(self, i2c_adr, MasterI2C, OFFSET=0.0025, FULLSCALE=1.248):
		self._i2c      = MasterI2C
		self._adr      = i2c_adr
		self.OFFSET    = OFFSET
		self.FULLSCALE = FULLSCALE	

	def SetValue(self, which, code):
		cmd = 0b00011000 | (which & 0b11)
		return self._i2c.write(self._adr, [cmd, (code>>8)&0xff, code&0xff]) #set dac value
	
	def SetVoltage(self, which, val, unit='V'):
		if   unit is 'uV': V = float(val)/1000000
		elif unit is 'mV': V = float(val)/1000
		elif unit is 'V':  V = float(val)
		else: return False
		if V <= self.OFFSET:    
			V = self.OFFSET
			print '->  Required output voltage for on-board DAC is lower then the offset value computed as %.5f mV'%(self.OFFSET*1000)
		elif V >= self.FULLSCALE: V = self.FULLSCALE
		#code = int( round( (V - self.OFFSET) * 0xffff / (self.FULLSCALE) ) )
		code = int( round( V/((self.FULLSCALE - self.OFFSET)/0xffff)))
		return self.SetValue(which, code)
		
class MAX1239:
	def __init__(self, i2c_adr, MasterI2C):
		self._i2c      = MasterI2C
		self._adr      = i2c_adr
	
	def GetValue(self, which):
		'''
		Reference = 0b010 -> external reference
		clk       = 0b1   -> internal clock
		BIP       = 0b0   -> unipolar
		RST       = 0b0   -> reset config register
		SCAN      = 0b11
		CS        = which
		SGL       = 0b1   -> single ended
		'''
		SetupReg  = 0b10101000
		ConfigReg = 0b01100001 |((which<<1)&0b00011110)
		if self._i2c.write(self._adr, [SetupReg,  ConfigReg]) is False: return False
		VAL = self._i2c.read(self._adr, 3)	
		return (VAL[0]&0x0f)<<8 | (VAL[1]&0xff)
	
	def GetVoltage(self, which, nsamples=1):
		if nsamples == 1:
			return float(self.GetValue(which))/float(0xfff)*1.2
		else: 
			dat = []
			for i in range(0, nsamples): dat.append(float(self.GetValue(which)))
			std = np.std(np.array(dat))
			dat = (float(sum(dat))/len(dat))/float(0xfff)*1.2
			return dat, std
			
			
class AD5665R_array:
	def __init__(self, i2c_adr_list, MasterI2C, OFFSET, FULLSCALE):
		self._i2c   = MasterI2C
		self.DAC = []
		for i in i2c_adr_list:
			self.DAC.append( AD5665R( i, self._i2c, OFFSET, FULLSCALE) )
	
	def SetValue(self, which, code):
		return self.DAC[ which>>2 ].SetValue( which & 3, code)
		
	def SetVoltage(self, which, val, unit='V'):
		return self.DAC[ which>>2 ].SetVoltage( which & 3, val, unit)

class INA226:
	def __init__(self, i2c_adr, MasterI2C):
		self._i2c = MasterI2C
		self._adr = i2c_adr
		self._max_cur = 0.020
		self._config = 0x41b7
		self._res = 10.0
		self._CurLSB = self._max_cur / 32768.0
		self._CAL = int(0.00512 / (self._CurLSB * self._res))
		self._i2c.write(self._adr, [0x00, (self._config>>8)&0xff,  self._config&0xff])
		self._i2c.write(self._adr, [0x05, (self._CAL>>8)&0xff,  self._CAL&0xff])
		self._i2c.write(self._adr, [0x04] )
		self._CurRegister = 0x04
	
	def Initiate(self):
		self._i2c.write(self._adr, [0x00, (self._config>>8)&0xff,  self._config&0xff])
		self._i2c.write(self._adr, [0x05, (self._CAL>>8)&0xff,  self._CAL&0xff])
		self._i2c.write(self._adr, [0x04] )
		self._CurRegister = 0x04
		
	def Read(self):
		if self._CurRegister != 0x04:
			self._i2c.write(self._adr, [0x04] )
		rep = self._i2c.read(self._adr, 2)
		return ((rep[0]<<8 | rep[1]) * self._CurLSB * 1000) 
		
	def ReadShunt(self):
		self._i2c.write(self._adr, [0x01] )
		rep = self._i2c.read(self._adr, 2)
		return (rep[0]<<8 | rep[1])
		
		
		
		
		
		
		
		
		
		
		
		
		
			
		
		
		
		
